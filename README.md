# Cash Flow

Programa para controlar fluxo de caixa visando pequenos comércios, criando automaticamente planilhas e gerando relatórios mensais.

## Gerar Executável
```
pip install pyinstaller
pyinstaller.exe --onefile --windowed --icon=cash_flow_icon.ico app.py
```