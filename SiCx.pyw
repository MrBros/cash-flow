import os
import io
import sys
# import json
# import sqlite3
import calendar
import subprocess
import collections
import tkinter as tk
from tkinter import tix
import tkinter.font as font
from platform import system
from openpyxl import load_workbook
from datetime import date, datetime
from tkinter import ttk, filedialog, messagebox

def to_br_currency(value: float): return 'R$ ' + ('%.2f' % value).replace('.',',')

def to_us_currency(value: float): return '$ ' + ('%.2f' % value)

def hex_to_int(hex_number): return int(hex_number, 16)

def is_number(value):
    try:
        float(value)
    except ValueError:
        return False
    return True

def to_us_float(string: str):
    string = string.replace(',','.') 
    if is_number(string):
        return float(string)
    return False 

def to_br_float(number):
    number = str(number)
    return number.replace('.',',')  

def callback_digit(input):
    if input.isdigit():
        return True
    elif input == '':
        return True
    else:
        return False

def callback_float(input):
    input = input.replace('.','').replace(',','.') 
    if input == '.': input = '0.0'
    elif input == '-': input = '-0.0'
    elif input == '+': input = '+0.0'
    if is_number(input):
        return True
    elif input == '':
        return True
    return False  

def focus_next(previous_widget=None, widget_deque=None):
    while widget_deque[0][0] != previous_widget:
        widget_deque.rotate(-1)
    while not widget_deque[1][1]:
        widget_deque.rotate(-1)
    
    widget_deque[1][0].focus()
    if isinstance(widget_deque[1][0], ttk.Entry):
        widget_deque[1][0].select_range(0, 'end')
    elif isinstance(widget_deque[1][0], tk.Listbox):
        widget_deque[1][0].selection_set(0)
    
def format(tp=None, string=None, *args):
        length = len(string)
        
        if string == '':
            return string

        if tp == 'id':
            cod = string
            for i in range(0, 8-length):
                cod = '0' + cod
            return cod

        elif tp == 'cpf':
            if length != 11:
                return 'INVÁLIDO'
            else:
                return f'{string[:3]}.{string[3:6]}.{string[6:9]}-{string[9:]}'

        elif tp == 'rg':
            if length not in (8,9):
                return 'INVÁLIDO'
            elif length == 8:
                return f'{string[:2]}.{string[2:5]}.{string[5:]}'
            elif length == 9:
                return f'{string[:2]}.{string[2:5]}.{string[5:8]}-{string[8:]}'

        elif tp == 'phone':
            if length not in (8,9,10,11,12,13):
                return 'INVÁLIDO'
            else:
                phone = string[length-8:]
                ddd = '37'
                if length in (10,11):
                    ddd = string[:2]
                elif length in (12,13):
                    ddd = string[2:4]
                digit = '9 '
                if phone[0] == '3':
                    digit = ''
                return f'+55 {ddd} {digit}{phone[:4]}-{phone[4:]}'

def update_button_state(table=None, button=None, *args):
    button.configure(state='disabled')
    if table.selection():
        button.configure(state='normal')

def update_table_dimensions(master=None, table=None, colname=None, mincolwidth=None, colwidth=None, anch=[tk.W, tk.CENTER, tk.E]):
        master.update()
        width = table.winfo_width()
        
        if mincolwidth is None:
            mincolwidth = colwidth

        for i in range(0,len(colname)):
            table.column(f'column{i+1}', width=int(colwidth[i]*width), anchor=anch[i], minwidth=int(mincolwidth[i]*width), stretch=tk.NO)
            table.heading(f'#{i+1}', text=colname[i])
# OK
class Counter():
    def __init__(self, initial_amount=0, initial_quantity=0):
        self.__amount = initial_amount
        self.__counter = initial_quantity

    # This function will update the amount and quantity variables
    def increase(self, new_amount: float):
        if is_number(new_amount):
            self.__amount += new_amount
            if self.__amount > 0:
                if new_amount > 0:
                    self.__counter += 1
                elif new_amount < 0:
                    self.__counter -= 1
            else: 
                self.__amount = 0
                self.__counter = 0
            return True
        return False

    # This function is just to simplify the previous one for amount variable 
    def get(self):
        return [self.__counter, self.__amount]  
    
    # This function will be a bridge between the graphic interface and the project core 
    def insert(self, command: str):
        try: 
            command = to_us_float(command)
            self.increase(command)
            return command
        except:
            pass
# OK
class Sheet():
    def __init__(self, sheet_file_path=r'./', month=datetime.now().month, day=datetime.now().day):
        self.__sheet_file_path = sheet_file_path
        self.__sheet_dir_path = self.__sheet_file_path.replace(fr'/{month}.xlsx', '')
        self.__today = str(day)
        self.__workbook = load_workbook(self.__sheet_file_path)
        self._sheet = self.__workbook[self.__today]
        
        self.__loc_map = {
            'cash_in': ['A', 'B', 'C'],
            'cash_out': ['D', 'E', 'F'],
            'sales': ['B40', None, 'C40'],
            'cashier': [None, None, 'C38'],
            'credit_card_sales': [None, None, 'F39'],
            'debit_card_sales': [None, None, 'F40']
        }

    def insert(self, loc: str, description='', payment_method='DINHEIRO', value=0.0):
        desc_coord, payment_coord, value_coord = self.__loc_map[loc]
        log_string = ''
        if loc == 'sales':
            self._sheet[desc_coord] = int(description)
            self._sheet[value_coord] = float(value)
        
        elif loc in ('cash_in', 'cash_out'):
            for i in range(4,33):
                if self._sheet[f'{desc_coord}{i}'].value in (None, ''):
                    self._sheet[f'{desc_coord}{i}'] = str(description).upper() 
                    self._sheet[f'{payment_coord}{i}'] = str(payment_method).upper()
                    self._sheet[f'{value_coord}{i}'] = float(value)
                    break
        else:
            tmp_value = self._sheet[value_coord].value
            if tmp_value in (None, ''):
                self._sheet[value_coord] = f'= {float(value)}'
            else:
                self._sheet[value_coord] = f'{tmp_value} + {float(value)}'
            
        self.__workbook.save(self.__sheet_file_path)
        return log_string
    
    def update(self, loc: str, coordinates=[None, None, None], description='', payment_method='DINHEIRO', value=0.0):
        old_values = [None, None, None]
        if loc in ('cash_in', 'cash_out'):
            if None not in coordinates:
                old_values = [self._sheet[coordinates[0]].value, self._sheet[coordinates[1]].value, self._sheet[coordinates[2]].value]
                self._sheet[coordinates[0]] = str(description).upper()
                self._sheet[coordinates[1]] = str(payment_method).upper()
                self._sheet[coordinates[2]] = float(value)
        
        self.__workbook.save(self.__sheet_file_path)
        
        return old_values

    def delete(self, loc, coordinates=[None, None, None], description='', payment_method='DINHEIRO', value=0.0):
        old_values = [None, None, None]
        if loc in ('cash_in', 'cash_out'):
            old_values = [self._sheet[coordinates[0]].value, self._sheet[coordinates[1]].value, self._sheet[coordinates[2]].value]   
            self._sheet[coordinates[0]] = ''
            self._sheet[coordinates[1]] = 'DINHEIRO'
            self._sheet[coordinates[2]] = 0.0
        
        elif loc == 'sales':
            old_values = [self._sheet[coordinates[0]].value, None, self._sheet[coordinates[2]].value]
            coordinates = self.__loc_map[loc]
            self._sheet[coordinates[0]] = 0
            self._sheet[coordinates[2]] = 0.0
        
        else:
            old_values = [None, None, self._sheet[coordinates[2]].value]
            coordinates = self.__loc_map[loc]
            tmp_value = self._sheet[coordinates[2]].value
            
            if tmp_value not in (None, '') and tmp_value != 0.0:
                self._sheet[coordinates[2]] = f'{tmp_value} - {float(value)}'
            
            else:
                self._sheet[coordinates[2]] = f'= {0.0}'

        self.__workbook.save(self.__sheet_file_path)

        return old_values

    def OnOpen(self, witch='current', *args):
        file_path = None
        
        if witch == 'current':
            file_path = self.__sheet_file_path
        
        elif witch == 'other':
            file_path = filedialog.askopenfilename(
                initialdir=self.__sheet_dir_path,
                title = "Abrir Planilha",
                filetypes = [("Microsoft Excel","*.xlsx")]
            )
            if file_path == '':
                return False
        
        if system() == 'Windows':
            os.startfile(file_path)
        elif system() == 'Linux':
            subprocess.run(['xdg-open', file_path])
# OK
class Home(tk.Frame):
    def __init__(self, container, log_file_path=None, tmp_file_path=None, yesterday_file_path=None, clear_tables_selection=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.__container = container
        self.__tmp_file_path = tmp_file_path
        self.__log_file_path = log_file_path
        self.__reg_float = self.register(callback_float)

        try:
            tmp_file = open(self.__tmp_file_path, 'r')
            arr = tmp_file.read().split()
            self.__sales_counter = Counter(initial_amount=float(arr[0]), initial_quantity=int(arr[1]))
            tmp_file.close()
        except:
            self.__sales_counter = Counter()
        yesterday = []
        try:
            yesterday_tmp_file = open(yesterday_file_path, 'r')
            yesterday = yesterday_tmp_file.read().split()
            yesterday_tmp_file.close()
            yesterday = [to_br_currency(float(yesterday[0])), int(yesterday[1])]
        except:
            yesterday = ['NÃO ENCONTRADO','NÃO ENCONTRADO']
        log_string = ''
        try:
            log_file = open(self.__log_file_path, 'r')
            for line in log_file:
                log_string += str(line)
            log_file.close()
        except:
            log_string = f"BOM DIA!\nDIA {date.today().strftime('%d/%m/%Y')}\nRESUMO DO DIA ANTERIOR:\n>> VENDAS REALIZADAS: {yesterday[1]}\n>> VALOR TOTAL: {yesterday[0]}\n\n"
            log_file = open(self.__log_file_path, 'w')
            log_file.write(log_string)

        # command frame
        self.__command_frame = ttk.Frame(self)
        ## command label
        self.__command_label = ttk.Label(
            self.__command_frame, 
            justify=tk.CENTER, 
            text='Digite um valor:'
        )
        ## command entry
        self.__command_string = tk.StringVar()
        self.__command_entry = ttk.Entry(
            self.__command_frame, 
            textvariable=self.__command_string,
            validate="key", 
            validatecommand=(self.__reg_float, '%P'),
            justify=tk.CENTER
        )
        self.__command_entry.focus()

        # amount frame
        self.__amount_frame = ttk.Frame(self)
        ## amount text
        self.__amount_text = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            text='Montante atual:'
        )
        ## amount value
        self.__amount_string = tk.StringVar()
        self.__amount_string.set(to_br_currency(self.__sales_counter.get()[1]))
        self.__amount_value = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            textvariable=self.__amount_string
        )
        ## quantity text
        self.__quantity_text = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            text='Vendas realizadas:'
        )
        ## quantity value
        self.__quantity_string = tk.StringVar()
        self.__quantity_string.set(self.__sales_counter.get()[0])
        self.__quantity_value = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            textvariable=self.__quantity_string
        )

        # buttons_frame
        self.__buttons_frame = ttk.Frame(self)
        ## buttons
        buttons_map = {
            'Entrada [F2]': lambda: self.__container.call_window('insert', 'cash_in'),
            'Saída [F3]': lambda: self.__container.call_window('insert', 'cash_out'),
            'Caixa II [F4]': lambda: self.__container.call_window('insert', 'cashier'),
            'Cartão [F5]': lambda: self.__container.call_window('insert', 'card_sales')
            # 'A prazo [F6]': lambda: Search(self, operation='new_credit_sale', history_update=lambda arg: self.history_update(arg)),
        }
        self.__buttons = []
        buttons_padding = [20,5,5,5,20]
        index = 0
        for key in buttons_map.keys():
            value = buttons_map[key]
            tmp_button = ttk.Button(
                self.__buttons_frame, 
                text=key, 
                command=value,
                cursor='hand2'
            )
            tmp_button.grid(row=0, column=index, padx=(buttons_padding[index], buttons_padding[index+1]), pady=20, sticky='NSEW')
            tmp_button.bind("<Return>", value)
            tmp_button.bind("<KP_Enter>", value)
            self.__buttons.append(tmp_button)
            index += 1
        
        # history frame
        self.__history_frame = ttk.Frame(self)
        ## history label
        self.__history_label = ttk.Label(
            self.__history_frame, 
            justify=tk.CENTER, 
            text='Vendas:'
        )
        ## history text
        self.__history_string = tk.StringVar()
        self._history_box = tk.Text(
            self.__history_frame,
            state='disabled',
            highlightthickness=0,
            wrap=tk.WORD,
            width=55,
            height=15
        )
        self._history_box.tag_configure("right", justify='right')
        self._history_box.tag_add("right", 1.0, "end")
        self.__history_scrollbar = ttk.Scrollbar(
            self.__history_frame, 
            orient="vertical", 
            command=self._history_box.yview, 
            cursor="hand2"
        )
        self._history_box.configure(yscrollcommand=self.__history_scrollbar.set)
        self._history_box.yview_moveto(1)

        # close frame
        self.__close_frame = ttk.Frame(self)
        ## close button
        self.__close_button = ttk.Button(
            self.__close_frame, 
            text='Finalizar expediente', 
            command=self.__container.close,
            cursor='hand2'
        )
        self._history_box.configure(state='normal')
        self._history_box.insert('1.0', log_string)
        self._history_box.configure(state='disabled')
        self._history_box.yview('end')

        # Grid
        ## Self
        self.rowconfigure(3, weight=1)
        self.columnconfigure(0, weight=1)
        ## Command
        self.__command_frame.rowconfigure(0, weight=1)
        self.__command_frame.columnconfigure(1, weight=1)
        self.__command_frame.grid(row=0, column=0, sticky='NSEW')
        self.__command_label.grid(row=0, column=0, padx=(20,20), pady=(20, 5), sticky='NSW')
        self.__command_entry.grid(row=0, column=1, padx=(10,20), pady=(20, 5), sticky='EW')
        ## Amount
        self.__amount_frame.rowconfigure((0,1), weight=1)
        self.__amount_frame.columnconfigure(1, weight=1)
        self.__amount_frame.grid(row=1, column=0, sticky='NSEW')
        self.__amount_text.grid(row=0, column=0, padx=(20,0), pady=5, sticky='NSW')
        self.__amount_value.grid(row=0, column=1, padx=(11,20), pady=5, sticky='NSE')
        self.__quantity_text.grid(row=1, column=0, padx=(20,0), pady=5, sticky='NSW')
        self.__quantity_value.grid(row=1, column=1, padx=(10,20), pady=5, sticky='NSE')
        ## Buttons
        self.__buttons_frame.rowconfigure(0, weight=1)
        self.__buttons_frame.columnconfigure((0,1,2,3,4), weight=1)
        self.__buttons_frame.grid(row=2, column=0, sticky='NSEW')
        ## History
        self.__history_frame.rowconfigure(1, weight=1)
        self.__history_frame.columnconfigure(0, weight=1)
        self.__history_frame.grid(row=3, column=0, sticky='NSEW')
        self.__history_label.grid(row=0, column=0, padx=20, pady=5, sticky='W')
        self._history_box.grid(row=1, column=0, padx=(20,0), pady=(0,5), ipadx=5, ipady=5, sticky='NSEW')
        self.__history_scrollbar.grid(row=1, column=1, sticky="WNS", padx=(0,20), pady=(0,5))
        ## Close
        self.__close_frame.rowconfigure(0, weight=1)
        self.__close_frame.columnconfigure(0, weight=1)
        self.__close_frame.grid(row=4, column=0, sticky='NSEW')
        self.__close_button.grid(row=0, column=0, padx=20, pady=20, sticky='EW')

        # Binds
        ## Self
        self.bind('<FocusIn>', clear_tables_selection)
        ## Command
        self.__command_entry.bind("<Return>", self.submit)
        self.__command_entry.bind("<KP_Enter>", self.submit)
        ## Close
        self.__close_button.bind("<Return>", self.__container.close)
        self.__close_button.bind("<KP_Enter>", self.__container.close)

    def submit(self, *args):
        string = self.__sales_counter.insert(self.__command_string.get())
        
        self.__command_entry.delete(0,'end')
        
        counter, amount = self.__sales_counter.get()
        
        self.__amount_string.set(to_br_currency(amount))
        self.__quantity_string.set(counter)

        log_string = None
        if is_number(string) and string != 0.0:
            log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {to_br_currency(float(string))}"
            
            self.history_update(log_string)

            tmp_file = open(self.__tmp_file_path, 'w')
            tmp_file.write(f'{amount} {counter}')    
            tmp_file.close()

        self.__command_entry.focus()

    def get_sales_var(self): return self.__sales_counter.get()

    def focus_on_command_entry(self): self.__command_entry.focus()
    
    def history_update(self, log_string, *args):
        self._history_box.configure(state='normal')
        self._history_box.insert('end', log_string)
        self._history_box.configure(state='disabled')
        self._history_box.yview('end')

        log_file = open(self.__log_file_path, 'a')
        log_file.write(log_string)
        log_file.close()
# OK
class Preview(tk.Frame): 
    def __init__(self, container, sheet_object=None, history_update=None, cbox_preset=['1','1','2020'], root_path=r'/output', *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.configure(width=50)
        self.__container = container
        self.__sheet = sheet_object
        self.__inputs_coordinates = []
        self.__outputs_coordinates = []
        self.__history_update = history_update
        self.__root_path = root_path
        self.__cbox_value = cbox_preset
        self.__selected_date = datetime.strptime('/'.join(self.__cbox_value),"%d/%m/%Y").strftime("%d/%m/%Y")
        self.__month_dict = {
            '1' : 'Janeiro',
            '2' : 'Fevereiro',
            '3' : 'Março',
            '4' : 'Abril',
            '5' : 'Maio',
            '6' : 'Junho',
            '7' : 'Julho',
            '8' : 'Agosto',
            '9' : 'Setembro',
            '10': 'Outubro',
            '11': 'Novembro',
            '12': 'Dezembro'
        }
        self.__date_frame = ttk.Frame(self)
        self.__day_label = ttk.Label(
            self.__date_frame,
            text='de'
        )
        monthrange = calendar.monthrange(date.today().year, date.today().month)
        day_arr = [str(day) for day in range(1,monthrange[1]+1)]
        self.__day_cbox = ttk.Combobox(
            self.__date_frame,
            values=day_arr,
            width=4,
            state='readonly',
            justify='center'
        )
        self.__day_cbox.set(cbox_preset[0])
        self.__month_label = ttk.Label(
            self.__date_frame,
            text='de'
        )
        month_arr = [self.__month_dict[key] for key in self.__month_dict.keys()]
        self.__month_cbox = ttk.Combobox(
            self.__date_frame,
            values=month_arr,
            width=10,
            state='readonly',
            justify='center'
        )
        self.__month_cbox.set(self.__month_dict[cbox_preset[1]])
        year_arr = [year for year in range(2020,date.today().year+1)]
        self.__year_cbox = ttk.Combobox(
            self.__date_frame,
            values=year_arr,
            width=6,
            state='readonly',
            justify='center'
        )
        self.__year_cbox.set(cbox_preset[2])
        self.__set_button = ttk.Button(
            self.__date_frame,
            text='Atualizar',
            command=self.alter_sheet
        )

        self.__table_in_frame = ttk.Frame(self)
        self.__table_in_label = ttk.Label(
            self.__table_in_frame,
            text='Entradas'
        )
        self.__table_in = ttk.Treeview(
            self.__table_in_frame,
            selectmode='browse',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.__table_in_scrollbar = ttk.Scrollbar(
            self.__table_in_frame, 
            orient="vertical", 
            command=self.__table_in.yview, 
            cursor="hand2"
        )
        self.__table_in.configure(yscrollcommand=self.__table_in_scrollbar.set)

        self.__table_out_frame = ttk.Frame(self)
        self.__table_out_label = ttk.Label(
            self.__table_out_frame,
            text='Saídas'
        )
        self.__table_out = ttk.Treeview(
            self.__table_out_frame,
            selectmode='browse',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.__table_out_scrollbar = ttk.Scrollbar(
            self.__table_out_frame, 
            orient="vertical", 
            command=self.__table_out.yview, 
            cursor="hand2"
        )
        self.__table_out.configure(yscrollcommand=self.__table_out_scrollbar.set)

        self.__coordinates = {
            'table_in': [],
            'table_out': []
        }

        self.__button_frame = ttk.Frame(self)
        self.__update_button = ttk.Button(
            self.__button_frame,
            cursor='hand2',
            text='Alterar',
            command=self.alter_table_data,
            state='disabled'
        )
        self.__delete_button = ttk.Button(
            self.__button_frame,
            cursor='hand2',
            text='Excluir',
            command=self.delete_table_data,
            state='disabled'
        )
        
        # grid
        ## Preview
        self.rowconfigure(1, weight=1)
        self.columnconfigure((0,1), weight=1)
        self.grid(row=0, column=1, sticky='NSEW')
        ## Date
        self.__date_frame.rowconfigure(0, weight=1)
        self.__date_frame.grid(row=0, column=0, sticky='NSEW', columnspan=2)
        self.__day_cbox.grid(row=0, column=0, sticky='W', padx=(20,10), pady=(20,0))
        self.__day_label.grid(row=0, column=1, padx=0, pady=(20,0))     
        self.__month_cbox.grid(row=0, column=2, sticky='W', padx=(10,10), pady=(20,0))
        self.__month_label.grid(row=0, column=3, padx=0, pady=(20,0))        
        self.__year_cbox.grid(row=0, column=4, sticky='W', padx=(10,10), pady=(20,0))
        self.__set_button.grid(row=0, column=5,sticky='EW', padx=(0,20), pady=(20,0))
        ## Table IN
        self.__table_in_frame.grid(row=1, column=0, sticky='NSEW')
        self.__table_in_frame.rowconfigure(1, weight=1)
        self.__table_in_frame.columnconfigure(0, weight=1)
        self.__table_in_label.grid(row=0, column=0, padx=20, pady=(10,0), columnspan=2)
        self.__table_in.grid(row=1, column=0, padx=(20,0), pady=(10,20), sticky='NSEW')
        self.__table_in_scrollbar.grid(row=1, column=1, padx=0, pady=(10,20), sticky="WNS")
        ## Table OUT
        self.__table_out_frame.grid(row=1, column=1, sticky='NSEW')
        self.__table_out_frame.rowconfigure(1, weight=1)
        self.__table_out_frame.columnconfigure(0, weight=1)
        self.__table_out_label.grid(row=0, column=0, padx=20, pady=(10,0), columnspan=2)
        self.__table_out.grid(row=1, column=0, padx=(20,0), pady=(10,20), sticky='NSEW')
        self.__table_out_scrollbar.grid(row=1, column=1, padx=(0,20), pady=(10,20), sticky="WNS")
        ## Buttons
        self.__button_frame.grid(row=2, column=0, sticky='NSEW', columnspan=2)
        self.__button_frame.rowconfigure(0, weight=1)
        self.__button_frame.columnconfigure((0,1), weight=1)
        self.__update_button.grid(row=0, column=0, padx=(20,10), pady=(0,20), sticky='EW')
        self.__delete_button.grid(row=0, column=1, padx=(10,20), pady=(0,20), sticky='EW')
            
        # Binds
        self.__day_cbox.bind('<KP_Enter>', lambda arg: self.__month_cbox.focus())
        self.__day_cbox.bind('<Return>', lambda arg: self.__month_cbox.focus())

        self.__month_cbox.bind('<KP_Enter>', lambda arg: self.__year_cbox.focus())
        self.__month_cbox.bind('<Return>', lambda arg: self.__year_cbox.focus())

        self.__year_cbox.bind('<KP_Enter>', lambda arg: self.__set_button.focus())
        self.__year_cbox.bind('<Return>', lambda arg: self.__set_button.focus())

        self.__set_button.bind('<KP_Enter>', lambda arg: self.alter_sheet())
        self.__set_button.bind('<Return>', lambda arg: self.alter_sheet())

        self.__table_in.bind('<FocusIn>', lambda arg: self.switch_table_selection(self.__table_out))
        self.__table_out.bind('<FocusIn>', lambda arg: self.switch_table_selection(self.__table_in))
        
        self.__table_in.bind('<ButtonRelease-1>', lambda arg: self.onFocus(self.__table_in))
        self.__table_out.bind('<ButtonRelease-1>', lambda arg: self.onFocus(self.__table_out))
        
        self.__table_in.bind('<KP_Enter>', lambda arg: self.__update_button.focus())
        self.__table_in.bind('<Return>', lambda arg: self.__update_button.focus())
        
        self.__table_out.bind('<KP_Enter>', lambda arg: self.__update_button.focus())
        self.__table_out.bind('<Return>', lambda arg: self.__update_button.focus())
        
        self.__update_button.bind('<KP_Enter>', self.alter_table_data)
        self.__update_button.bind('<Return>', self.alter_table_data)
        
        self.__delete_button.bind('<KP_Enter>', self.delete_table_data)
        self.__delete_button.bind('<Return>', self.delete_table_data)

    def update_table_dimensions(self):
        for table in (self.__table_in, self.__table_out):
            update_table_dimensions(
                master=self,
                table=table, 
                colname=['Descrição', 'Pagamento', 'Valor'],
                mincolwidth=[0.335,0.250,0.190],
                colwidth=[0.335,0.330,0.330]
            )
        self.set_all_table_data()

    def clear_tables_selection(self, *args):
        for table in (self.__table_in, self.__table_out):
            if len(table.selection()) > 0:
                table.selection_remove(table.selection()[0])
                
        self.__update_button['state'] = 'disabled'
        self.__delete_button['state'] = 'disabled'

    def switch_table_selection(self, other_table, *args):
        if len(other_table.selection()) > 0:
            other_table.selection_remove(other_table.selection()[0])
    
    def onFocus(self, this_table, *args):
        if len(this_table.selection()) > 0:
            self.__update_button['state'] = 'normal'
            self.__delete_button['state'] = 'normal'
        else:
            self.__update_button['state'] = 'disabled'
            self.__delete_button['state'] = 'disabled'

    def set_all_table_data(self):
        col = 1
        for table in (self.__table_in, self.__table_out):
            self.set_table_data(table, min_col=col, max_col=col+2)
            col += 3

    def set_table_data(self, table, min_col=1, max_col=3, *args):
        for row in table.get_children():
            table.delete(row)

        key = 'table_in'
        coords = ['A', 'B', 'C']
        if table == self.__table_out: 
            key = 'table_out'
            coords = ['D', 'E', 'F']
        
        index = 4
        for row in self.__sheet._sheet.iter_rows(min_row=4, max_row=32, min_col=min_col, max_col=max_col, values_only=True):
            if row[0] not in ('', None):
                table.insert("", "end", values=[row[0], row[1], to_br_currency(float(row[2]))])
                self.__coordinates[key].append([coords[0]+str(index), coords[1]+str(index), coords[2]+str(index)])
            index += 1    
        
    def alter_sheet(self, *args):
        year = str(self.__year_cbox.get())
        month=''
        for key in self.__month_dict.keys():
            if self.__month_dict[key] == self.__month_cbox.get():    
                month = str(key)
                break
        
        print(rf'{self.__root_path}/{year}/{month}.xlsx')
        self.__sheet = Sheet(
            sheet_file_path=rf'{self.__root_path}/{year}/{month}.xlsx',
            day=self.__day_cbox.get()
        )
        self.__cbox_value = [str(self.__day_cbox.get()),str(month),str(year)]
        self.__selected_date = datetime.strptime('/'.join(self.__cbox_value),"%d/%m/%Y").strftime("%d/%m/%Y")
        
        self.define_selectmode()
        self.set_all_table_data()

    def define_selectmode(self):
        if self.__selected_date != datetime.now().strftime("%d/%m/%Y"): 
            self.__table_in.configure(selectmode='none')
            self.__table_out.configure(selectmode='none')
        else: 
            self.__table_in.configure(selectmode='browse')
            self.__table_out.configure(selectmode='browse')

    def alter_table_data(self, *args):
        table = self.__table_in
        selection = table.selection()
        coord_array = self.__coordinates['table_in']
        win = 'cash_in'
        if len(selection) == 0: 
            table = self.__table_out
            selection = table.selection()
            coord_array = self.__coordinates['table_out']
            win = 'cash_out'

        for row in selection:
            values = table.item(row,"values")
            value = str(values[2]).replace('R$ ','')
            index = hex_to_int(str(row).replace('I',''))-1
            coordinates = coord_array[index]
            
            self.__container.call_window(
                operation='update', 
                wintype=win, 
                values=[values[0], values[1], value],
                coordinates=coordinates,
                dt=self.__selected_date
            )
        
        self.__update_button['state'] = 'disabled'
        self.__delete_button['state'] = 'disabled'
    
    def delete_table_data(self, *args):  
        table = self.__table_in
        selection = table.selection()
        coord_array = self.__coordinates['table_in']
        win = 'cash_in'
        if len(selection) == 0: 
            table = self.__table_out
            selection = table.selection()
            coord_array = self.__coordinates['table_out']
            win = 'cash_out'

        for row in selection:
            values = table.item(row,"values")
            value = str(values[2]).replace('R$ ','')
            index = hex_to_int(str(row).replace('I',''))-1
            coordinates = coord_array[index]
            
            self.__container.call_window(
                operation='delete', 
                wintype=win, 
                values=[values[0], values[1], value],
                coordinates=coordinates,
                dt=self.__selected_date
            )
        
        self.__update_button['state'] = 'disabled'
        self.__delete_button['state'] = 'disabled'
# OK
class App(tix.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Window Settings
        self.title('Controlador de Fluxo de Caixa')
        self.__fullscreen = True
        self.attributes("-fullscreen", self.__fullscreen)
        self.iconbitmap(rf'{os.getcwd()}/assets/cashier.ico')

        # Menu
        self.__menubar = tk.Menu(self)
        self.config(menu=self.__menubar)
        # Submenu
        self.__archive_menu = tk.Menu(self.__menubar, tearoff=0)
        self.__credit_sales_menu = tk.Menu(self.__menubar, tearoff=0)
        self.__client_menu = tk.Menu(self.__menubar, tearoff=0)
        # self.__help_menu = tk.Menu(self.__menubar, tearoff=0)
        # Sub-submenu
        self.__open_submenu = tk.Menu(self.__archive_menu, tearoff=0)
        ## Cascades
        self.__menubar.add_cascade(label='Arquivo', menu=self.__archive_menu)
        # self.__menubar.add_cascade(label='Cliente', menu=self.__client_menu)
        # self.__menubar.add_cascade(label='Notinhas', menu=self.__credit_sales_menu)
        # self.__menubar.add_cascade(label='Ajuda', menu=self.__help_menu)
        self.__archive_menu.add_cascade(label='Abir', menu=self.__open_submenu)
        ### Commands
        ### Archive Menu
        self.__open_submenu.add_command(label='Planilha atual', command=lambda: self.__sheet.OnOpen(witch='current'))
        self.__open_submenu.add_command(label='Outra planilha', command=lambda: self.__sheet.OnOpen(witch='other'))
        # self.__archive_menu.add_command(label='Relatório', command=self.generate_relatory)
        self.__archive_menu.add_command(label='Mudar visualização', command=self.switch_view)
        self.__archive_menu.add_command(label='Sair', command=self.on_closing)
        ### Client Menu
        # self.__client_menu.add_command(label='Cadastrar', command=lambda: ClientRegister(self, operation='insert', history_update=lambda arg: self.__home.history_update(arg)))
        # self.__client_menu.add_command(label='Atualizar ', command=lambda: Search(self, operation='update', history_update=lambda arg: self.__home.history_update(arg)))
        # self.__client_menu.add_command(label='Remover', command=lambda: Search(self, operation='remove', history_update=lambda arg: self.__home.history_update(arg)))
        # ### Credit Sales Menu
        # self.__credit_sales_menu.add_command(label='Ver notinha', command=lambda: Search(self, operation='show_client', history_update=lambda arg: self.__home.history_update(arg)))
        ### Help Menu
        # self.__help_menu.add_command(label='Ajuda', command=self.help)

        # Variables
        self.__today = date.today().strftime("%Y%m%d")
        self.__yesterday = date.fromordinal(date.today().toordinal() - 1).strftime("%Y%m%d")
        self.__wintype_dict = {
            'cash_in': 'Entrada',
            'cash_out': 'Saída',
            'sales': 'Vendas',
            'cashier': 'Caixa II',
            'card_sales': 'Venda no Cartão',
        }
        self.__operation_dict = {
            'insert': 'Inserir',
            'update': 'Atualizar',
            'delete': 'Remover'
        }

        # Paths
        log_dir_path = os.getcwd()+r'/logs'
        tmp_dir_path = log_dir_path+r'/tmp'
        
        try:
            os.makedirs(log_dir_path)
        except OSError:
            pass
        try:
            os.makedirs(tmp_dir_path)
        except OSError:
            pass

        self.__tmp_file_path = fr'{tmp_dir_path}/{self.__today}.txt'
        self.__log_file_path = fr'{log_dir_path}/{self.__today}.txt'
        self.__output_dir_path = fr'{os.getcwd()}/output'
        self.__sheet_dir_path = fr'{os.getcwd()}/output/{datetime.now().year}/'
        self.__sheet_file_path = fr'{os.getcwd()}/output/{datetime.now().year}/{datetime.now().month}.xlsx'

        # Classes
        self.__sheet = Sheet(sheet_file_path=self.__sheet_file_path)

        # Home Frame
        self.__home = Home(
            self,
            log_file_path=self.__log_file_path,
            tmp_file_path=self.__tmp_file_path,
            yesterday_file_path=fr'{tmp_dir_path}/{self.__yesterday}.txt',
            clear_tables_selection=self.clear_tables_selection
        )
        # Preview Frame
        self.__preview = Preview(
            self,
            sheet_object=self.__sheet,
            root_path=self.__output_dir_path,
            cbox_preset=[str(date.today().day),str(date.today().month),str(date.today().year)],
            history_update=lambda arg: self.__home.history_update(arg)
        )
        
        # Protocols
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Grid
        self.rowconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.__home.grid(row=0, column=0, sticky='NSEW')
        self.__preview.grid(row=0, column=1, sticky='NSEW')
        self.__preview.update_table_dimensions()
        
        # Binds
        ## App
        self.bind('<Control-o>', lambda arg: self.__sheet.OnOpen(witch='current'))
        self.bind('<Control-k>', lambda arg: self.__sheet.OnOpen(witch='other'))
        self.bind('<F2>', lambda arg: self.call_window(operation='insert', wintype='cash_in'))
        self.bind('<F3>', lambda arg: self.call_window('insert', 'cash_out'))
        self.bind('<F4>', lambda arg: self.call_window('insert', 'cashier'))
        self.bind('<F5>', lambda arg: self.call_window('insert', 'card_sales'))
        # self.bind('<F6>', lambda arg: Search(self, operation='new_credit_sale', history_update=lambda arg: self.__home.history_update(arg)))
        # self.bind('<F10>', lambda arg: Search(self, operation='show_client'))
        self.bind('<F11>', self.switch_view)

        # self.__creditsales.close_month_sales()

    def on_closing(self):
        if messagebox.askokcancel("Atenção", "Você realmente deseja sair?"):
            counter, amount = self.__home.get_sales_var()
            self.__sheet.insert('sales', description=counter, value=amount)
            self.destroy()
    
    def call_window(self, operation=None, wintype=None, values=['', 'DINHEIRO', ''], coordinates=None, dt=datetime.now().strftime("%d/%m/%Y"), *args):
        description_flag, payment_flag, value_flag = (True, True, True)
        log_string = ''

        if wintype == 'card_sales':
            description_flag = False
        
        elif wintype == 'cashier':
            description_flag = False
            payment_flag = False

        old_values = values
        new_values = old_values

        if operation != 'delete':
            window = Window(
                container=self, 
                operation=operation,
                printable_operation=self.__operation_dict[operation],
                wintype=wintype,
                printable_wintype=self.__wintype_dict[wintype],
                description=description_flag, 
                payment_method=payment_flag, 
                value=value_flag, 
                values=values,
            )
            self.wait_window(window)
            new_values = window._values
        
        if new_values != [None, None, None]:
            if operation == 'insert':
                try:
                    self.__sheet.insert(
                        loc=wintype, 
                        description=new_values[0],
                        payment_method=new_values[1],
                        value=new_values[2]
                    )
                    log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {self.__operation_dict[operation].upper()} {self.__wintype_dict[wintype].upper()}\
                                   \n>> DESCRIÇÃO: {str(new_values[0]).upper()}\
                                   \n>> PAGAMENTO: {str(new_values[1]).upper()}\
                                   \n>> VALOR: {to_br_currency(float(new_values[2]))}"
                except:
                    messagebox.showerror(title='Erro', message='Não foi possível realizar essa operação!')
            
            elif operation == 'update':
                try:
                    self.__sheet.update(
                        loc=wintype, 
                        coordinates=coordinates,
                        description=new_values[0],
                        payment_method=new_values[1],
                        value=new_values[2],
                    )
                    log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {self.__operation_dict[operation].upper()} {self.__wintype_dict[wintype].upper()}\
                                   \n>> DESCRIÇÃO: {str(old_values[0].upper())} -> {str(new_values[0]).upper()}\
                                   \n>> PAGAMENTO: {str(old_values[1].upper())} -> {str(new_values[1]).upper()}\
                                   \n>> VALOR: {str(old_values[2].upper())} -> {to_br_currency(float(new_values[2]))}"
                except:
                    messagebox.showerror(title='Erro', message='Não foi possível realizar essa operação!')
            
            elif operation == 'delete':
                try:
                    self.__sheet.delete(
                        loc=wintype, 
                        coordinates=coordinates
                    )
                    log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {self.__operation_dict[operation].upper()} {self.__wintype_dict[wintype].upper()}\
                                   \n>> DESCRIÇÃO: {str(old_values[0].upper())}\
                                   \n>> PAGAMENTO: {str(old_values[1].upper())}\
                                   \n>> VALOR: {str(old_values[2].upper())}"
                except:
                    messagebox.showerror(title='Erro', message='Não foi possível realizar essa operação!')

        if log_string != '':
            self.__home.history_update(log_string)
            self.__preview.set_all_table_data()

        self.__home.focus_on_command_entry()

    def switch_view(self, *args):
        if not self.__fullscreen:
            self.__preview.grid(row=0, column=1, sticky='NSEW')
        else:
            self.__preview.grid_forget()
            self.resizable(False, False)
            self.geometry("510x600")
        
        self.__fullscreen = not self.__fullscreen
        self.attributes('-fullscreen', self.__fullscreen)
        self.__home._history_box.yview('end')
        self.__preview.update_table_dimensions()
    
    def clear_tables_selection(self, *args): self.__preview.clear_tables_selection()

    def generate_relatory(self, *args):
        messagebox.showinfo("Ops","Essa funcionalidade ainda não foi implementada!")

    def help(self, *args):
        messagebox.showinfo("Ops","Essa funcionalidade ainda não foi implementada!")

    def close(self, *args):
        counter, amount = self.__home.get_sales_var()
        self.__sheet.insert('sales', description=counter, value=amount)
        log_string = f"""
        EXPEDIENTE FINALIZADO COM SUCESSO!\n
        DIA: {date.today().strftime("%d/%m/%Y")}\n
        VENDAS REALIZADAS: {counter}\n
        VALOR TOTAL: {to_br_currency(amount)}\n
        """
        messagebox.showinfo("Informação", log_string)

        self.__home.history_update(f"{datetime.now().strftime('%H:%M:%S')} >\n"+log_string)          
        
        self.destroy()
# OK
class Window(tk.Toplevel):
    def __init__(self, container=None, operation=None, printable_operation=None, wintype=None, printable_wintype=None, description=True, payment_method=True, value=True, values=['','DINHEIRO',''], *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.resizable(False, False)
        self.title(f'{printable_operation} {printable_wintype}')
        self.rowconfigure(4, weight=1)
        self.columnconfigure(0, weight=1)
        self.grab_set()
        self.focus()

        self._values = values
        self.__description = description
        self.__payment = payment_method
        self.__value = value
        self.__reg_float = self.register(callback_float)
        self.__operation = operation
        self.__wintype = wintype

        # Error
        self.__error_frame = ttk.Frame(self)
        self.__error_string = tk.StringVar()
        self.__error_label = ttk.Label(
            self.__error_frame,
            textvar=self.__error_string,
            foreground='red'
        )
        
        # Description
        self.__description_frame = ttk.Frame(self)
        self.__description_label = ttk.Label(
            self.__description_frame, 
            text='* Descrição'
        )
        self.__description_string = tk.StringVar()
        self.__description_string.set(self._values[0])
        self.__description_entry = ttk.Entry(
            self.__description_frame,
            textvar=self.__description_string,
            width=50
        )
        
        # Payment
        self.__payment_frame = ttk.Frame(self)
        self.__payment_label = ttk.Label(
            self.__payment_frame, 
            text='* Forma de pagamento'
        )
        self.__payment_array = [
            'DINHEIRO', 
            'CRÉDITO', 
            'DÉBITO', 
            'CHEQUE', 
            'VALE ALIMENTAÇÃO', 
            'SICOOB', 
            'IB CAIXA', 
            'OUTROS'
        ]
        if wintype == 'card_sales':
            if self._values[1] in ('','DINHEIRO'):
                self._values[1] = 'DÉBITO'
            self.__payment_array = [
                'DÉBITO',
                'CRÉDITO'
            ]
        self.__payment_cbox = ttk.Combobox(
            self.__payment_frame,
            values=self.__payment_array,
            state='readonly'
        )
        self.__payment_cbox.set(self._values[1])
        
        # Value   
        self.__value_frame = ttk.Frame(self)         
        self.__value_label = ttk.Label(
            self.__value_frame, 
            text='* Valor'
        )
        self.__value_string = tk.StringVar()
        self.__value_string.set(self._values[2])
        self.__value_entry = ttk.Entry(
            self.__value_frame,
            textvariable=self.__value_string,
            validate="key", 
            validatecommand=(self.__reg_float, '%P'),
            width=50
        )

        # Buttons
        self.__buttons_frame = ttk.Frame(self)
        self.__confirm_button = ttk.Button(
            self.__buttons_frame,
            text='OK',
            cursor='hand2',
            command=self.confirm
        )
        self.__cancel_button = ttk.Button(
            self.__buttons_frame,
            text='Cancelar',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.__description_entry, self.__description],
            [self.__payment_cbox, self.__payment],
            [self.__value_entry, self.__value],
            [self.__confirm_button, True],
            [self.__cancel_button, True]
        ]
        self.__widget_deque = collections.deque(widget_sequence)

        # Grid 
        if self.__description:
            self.__description_frame.grid(row=1, column=0, sticky='NSEW')
            self.__description_frame.rowconfigure((0,1), weight=1)
            self.__description_frame.columnconfigure(0, weight=1)
            self.__description_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,0))
            self.__description_entry.grid(row=1, column=0, sticky='EW', padx=20, pady=(0,5))
        
        if self.__payment:
            self.__payment_frame.grid(row=2, column=0, sticky='NSEW')
            self.__payment_frame.rowconfigure(0, weight=1)
            self.__payment_frame.columnconfigure(1, weight=1)
            self.__payment_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,0), ipady=5)
            self.__payment_cbox.grid(row=0, column=1, sticky='EW', padx=(0,20), pady=(20,0))
        
        if self.__value:
            self.__value_frame.grid(row=3, column=0, sticky='NSEW')
            self.__value_frame.rowconfigure(0, weight=1)
            self.__value_frame.columnconfigure(1, weight=1)
            self.__value_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,0))
            self.__value_entry.grid(row=1, column=0, sticky='EW', padx=20, pady=(0,5))
        
        self.__buttons_frame.grid(row=4, column=0, sticky='NSEW')
        self.__buttons_frame.rowconfigure(0, weight=1)
        self.__buttons_frame.columnconfigure((0,1), weight=1)
        self.__confirm_button.grid(row=0, column=0, sticky='NSEW', padx=(20,10), pady=20)
        self.__cancel_button.grid(row=0, column=1, sticky='NSEW', padx=(10,20), pady=20)

        focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
        
        # Binds
        self.bind('<Escape>', self.cancel)
        self.__description_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.__description_entry, widget_deque=self.__widget_deque))
        self.__description_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__description_entry, widget_deque=self.__widget_deque))
        
        self.__payment_cbox.bind('<Return>', lambda arg: focus_next(previous_widget=self.__payment_cbox, widget_deque=self.__widget_deque))
        self.__payment_cbox.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__payment_cbox, widget_deque=self.__widget_deque))
        
        self.__value_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.__value_entry, widget_deque=self.__widget_deque))
        self.__value_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__value_entry, widget_deque=self.__widget_deque))
        
        self.__confirm_button.bind('<Return>', self.confirm)
        self.__confirm_button.bind('<KP_Enter>', self.confirm)
        
        self.__cancel_button.bind('<Return>', self.cancel)
        self.__cancel_button.bind('<KP_Enter>', self.cancel)

        self.focus()

    def error_check(self, *args):
        widget_dict = {
            'description': [self.__description_entry, self.__description_string, self.__description],
            'payment': [self.__payment_cbox, self.__payment_cbox, self.__payment],
            'value': [self.__value_entry, self.__value_string, self.__value]
        }

        for key in widget_dict.keys():
            if widget_dict[key][1].get() in (None, '') and widget_dict[key][2]:
                self.__error_string.set('Preencha todos os campos obrigatórios (*)')
                self.__error_frame.grid(row=0, column=0, sticky='NSEW')
                self.__error_frame.rowconfigure(0, weight=1)
                self.__error_frame.columnconfigure(0, weight=1)
                self.__error_label.grid(row=0, column=0, sticky='NS', padx=20, pady=(20,0))

                widget_dict[key][0].focus()
                return True
        
        self.__error_string.set('')
        self.__error_frame.grid_remove()
        return False    
        
    def confirm(self, *args):
        if not self.error_check():
            self._values = [self.__description_entry.get().upper(), self.__payment_cbox.get().upper(), to_us_float(self.__value_entry.get())]
            self.destroy()

    def cancel(self, *args):
        self._values = [None, None, None]
        self.destroy()
# OK
def main():
    root = App()

    style = ttk.Style(root)
    style.theme_use('clam')
    helv12 = font.Font(family="Helvetica", size=123)
    root.mainloop()
 
if __name__ == '__main__':
    main()