from platform import system
from datetime import datetime
from os import startfile, close
from openpyxl import load_workbook
from tkinter.filedialog import askopenfilename
from functions import to_br_currency, is_number, to_us_float


class Sheet():
    def __init__(self, sheet_file_path=r'./'):
        self.__sheet_file_path = sheet_file_path
        self.__sheet_dir_path = self.__sheet_file_path.replace(fr'/{datetime.now().month}.xlsx', '')
        self.__today = f'{datetime.now().day}'
        self.__workbook = load_workbook(self.__sheet_file_path)
        self.__sheet = self.__workbook[self.__today]
        
        self.__loc_map = {
            'cash_in': ['A', 'B', 'C'],
            'cash_out': ['D', 'E', 'F'],
            'sales': ['B40', None, 'C40'],
            'cashier': [None, None, 'C38'],
            'credit_card_sales': [None, None, 'F39'],
            'debit_card_sales': [None, None, 'F40']
        }

    def insert(self, loc: str, description='', payment_method='DINHEIRO', value=0.0):
        desc_coord, payment_coord, value_coord = self.__loc_map[loc]
        log_string = ''
        if loc == 'sales':
            self.__sheet[desc_coord] = int(description)
            self.__sheet[value_coord] = float(value)
        
        elif loc in ('cash_in', 'cash_out'):
            for i in range(4,33):
                if self.__sheet[f'{desc_coord}{i}'].value in (None, ''):
                    self.__sheet[f'{desc_coord}{i}'] = str(description).upper() 
                    self.__sheet[f'{payment_coord}{i}'] = str(payment_method).upper()
                    self.__sheet[f'{value_coord}{i}'] = float(value)
                    break
        else:
            tmp_value = self.__sheet[value_coord].value
            if tmp_value in (None, ''):
                self.__sheet[value_coord] = f'= {float(value)}'
            else:
                self.__sheet[value_coord] = f'{tmp_value} + {float(value)}'
            
        self.__workbook.save(self.__sheet_file_path)
        return log_string
    
    def update(self, loc: str, coordinates=[None, None, None], description='', payment_method='DINHEIRO', value=0.0):
        old_values = [None, None, None]
        if loc in ('cash_in', 'cash_out'):
            if None not in coordinates:
                old_values = [self.__sheet[coordinates[0]].value, self.__sheet[coordinates[1]].value, self.__sheet[coordinates[2]].value]
                self.__sheet[coordinates[0]] = str(description).upper()
                self.__sheet[coordinates[1]] = str(payment_method).upper()
                self.__sheet[coordinates[2]] = float(value)
        
        self.__workbook.save(self.__sheet_file_path)
        
        return old_values

    def delete(self, loc, coordinates=[None, None, None], description='', payment_method='DINHEIRO', value=0.0):
        old_values = [None, None, None]
        if loc in ('cash_in', 'cash_out'):
            old_values = [self.__sheet[coordinates[0]].value, self.__sheet[coordinates[1]].value, self.__sheet[coordinates[2]].value]   
            self.__sheet[coordinates[0]] = ''
            self.__sheet[coordinates[1]] = 'DINHEIRO'
            self.__sheet[coordinates[2]] = 0.0
        
        elif loc == 'sales':
            old_values = [self.__sheet[coordinates[0]].value, None, self.__sheet[coordinates[2]].value]
            coordinates = self.__loc_map[loc]
            self.__sheet[coordinates[0]] = 0
            self.__sheet[coordinates[2]] = 0.0
        
        else:
            old_values = [None, None, self.__sheet[coordinates[2]].value]
            coordinates = self.__loc_map[loc]
            tmp_value = self.__sheet[coordinates[2]].value
            
            if tmp_value not in (None, '') and tmp_value != 0.0:
                self.__sheet[coordinates[2]] = f'{tmp_value} - {float(value)}'
            
            else:
                self.__sheet[coordinates[2]] = f'= {0.0}'

        self.__workbook.save(self.__sheet_file_path)

        return old_values

    def OnOpen(self, witch='current', *args):
        file_path = None
        
        if witch == 'current':
            file_path = self.__sheet_file_path
        
        elif witch == 'other':
            file_path = askopenfilename(
                initialdir=self.__sheet_dir_path,
                title = "Abrir Planilha",
                filetypes = [("Microsoft Excel","*.xlsx")]
            )
            if file_path == '':
                return False
        
        if system() == 'Windows':
            startfile(file_path)