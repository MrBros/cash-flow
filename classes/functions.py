from tkinter import ttk

def to_br_currency(value: float):
    return 'R$ ' + ('%.2f' % value).replace('.',',')

def to_us_currency(value: float):
    return '$ ' + ('%.2f' % value)

def is_number(value):
    try:
        float(value)
    except ValueError:
        return False
    return True

def to_us_float(string: str): # The string must be in BRL floating format
    string = string.replace(',','.') 
    if is_number(string):
        return float(string)
    return False 

def to_br_float(number):
    number = str(number)
    return number.replace('.',',')  

def hex_to_int(hex_number):
    return int(hex_number, 16)

def callback_digit(input):
    if input.isdigit():
        return True
    elif input == '':
        return True
    else:
        return False

def callback_float(input): # The string must be in BRL floating format
    input = input.replace('.','').replace(',','.') 
    if input == '.': input = '0.0'
    elif input == '-': input = '-0.0'
    elif input == '+': input = '+0.0'
    if is_number(input):
        return True
    elif input == '':
        return True
    return False  

def focus_next(previous_widget=None, widget_deque=None):
    while widget_deque[0][0] != previous_widget:
        widget_deque.rotate(-1)
    while not widget_deque[1][1]:
        widget_deque.rotate(-1)
    
    widget_deque[1][0].focus()
    if isinstance(widget_deque[1][0], ttk.Entry):
        widget_deque[1][0].select_range(0, 'end')
    