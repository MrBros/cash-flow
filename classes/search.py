import sqlite3
import tkinter as tk
from tkinter import ttk
from sheet import Sheet
import io
import collections
from tkinter import messagebox
import json
from datetime import date, datetime
from clientwindow import ClientWindow
from clientregister import ClientRegister
from authwindow import AuthWindow
from saleregister import SaleRegister

class Search(tk.Toplevel):
    def __init__(self, container, operation='update', history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.container = container
        self.title('Buscar Cliente')
        self.resizable(False, False)
        self.rowconfigure((0,1), weight=1)
        self.columnconfigure(0, weight=1)
        self.grab_set()

        self.history_update = history_update
        self.operation = operation
        self.cod = 0
        self.conn = sqlite3.connect('credit_sales.db')
        self.cursor = self.conn.cursor()

        self.search_frame = ttk.Frame(self)
        self.search_label = ttk.Label(
            self.search_frame,
            text='Digite o nome do cliente',
        ) 
        self.search_string = tk.StringVar()
        self.search_entry = ttk.Entry(
            self.search_frame,
            textvariable=self.search_string
        )
        self.search_lbox = tk.Listbox(
            self.search_frame,
            selectmode='browse',
            width=70,
        )
        self.search()

        # buttons
        self.buttons_frame = ttk.Frame(self)
        self.confirm_button = ttk.Button(
            self.buttons_frame,
            text='OK [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.cancel_button = ttk.Button(
            self.buttons_frame,
            text='Cancelar [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.search_entry, True],
            [self.search_lbox, True],
            [self.confirm_button, True],
            [self.cancel_button, True]
        ]
        self.widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.search_frame.grid(row=0, column=0, sticky='NSEW')
        self.search_frame.rowconfigure(2, weight=1)
        self.search_label.grid(row=0, column=0, padx=20, pady=(20, 0), sticky='W')
        self.search_entry.grid(row=1, column=0, padx=20, pady=5, sticky='NSEW')
        self.search_lbox.grid(row=2, column=0, padx=20, pady=(0, 20), sticky='EW')

        self.buttons_frame.grid(row=1, column=0, columnspan=2, sticky='NSEW')
        self.buttons_frame.rowconfigure(1, weight=1)
        self.buttons_frame.columnconfigure((0,1), weight=1)
        self.confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.search_entry.bind('<Return>', lambda arg: self.focus_next(self.search_entry))
        self.search_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.search_entry))
        self.search_entry.bind('<KeyRelease>', lambda arg: self.search())

        self.search_lbox.bind('<Return>', lambda arg: self.focus_next(self.search_lbox))
        self.search_lbox.bind('<KP_Enter>', lambda arg: self.focus_next(self.search_lbox))

        self.confirm_button.bind('<Return>', self.confirm)
        self.confirm_button.bind('<KP_Enter>', self.confirm)

        self.cancel_button.bind('<Return>', self.cancel)
        self.cancel_button.bind('<KP_Enter>', self.cancel)

        self.focus_next(self.cancel_button)
        self.focus_next(self.cancel_button)
    
    def search(self):
        try:
            word = int(self.search_string.get().upper())
            self.cursor.execute("""
            select cod, name from client where cod == ? order by name
            """, (word,))
        except:
            word = self.search_string.get().upper()+'%'
            self.cursor.execute("""
            select cod, name from client where name like ? order by name
            """, (word,))

        self.search_lbox.delete(0,'end')
        try:
            index=0
            for person in self.cursor.fetchall():
                cod = self.format(tp='cod', string=str(person[0]))
                name = str(person[1])
                self.search_lbox.insert(index, f'{cod} {name}')
                index+=1
        except:
            pass
        
    def callback_digit(self, input):
        if input.isdigit():
            return True
        elif input == '':
            return True
        else:
            return False

    def focus_next(self, previous_widget):
        while self.widget_deque[0][0] != previous_widget:
            self.widget_deque.rotate(-1)
        while not self.widget_deque[1][1]:
            self.widget_deque.rotate(-1)
       
        self.widget_deque[1][0].focus()
        if isinstance(self.widget_deque[1][0], ttk.Entry):
            self.widget_deque[1][0].select_range(0, 'end')
        elif isinstance(self.widget_deque[1][0], tk.Listbox):
            self.widget_deque[1][0].selection_set(0)
    
    def format(self, tp=None, string=None, *args):
        length = len(string)
        
        if string == '':
            return string

        if tp == 'cod':
            cod = string
            for i in range(0, 8-length):
                cod = '0' + cod
            return cod

        elif tp == 'cpf':
            if length != 11:
                return 'INVÁLIDO'
            else:
                return f'{string[:3]}.{string[3:6]}.{string[6:9]}-{string[9:]}'

        elif tp == 'rg':
            if length not in (8,9):
                return 'INVÁLIDO'
            elif length == 8:
                return f'{string[:2]}.{string[2:5]}.{string[5:]}'
            elif length == 9:
                return f'{string[:2]}.{string[2:5]}.{string[5:8]}-{string[8:]}'

        elif tp == 'phone':
            if length not in (8,9,10,11,12,13):
                return 'INVÁLIDO'
            else:
                phone = string[length-8:]
                ddd = '37'
                if length in (10,11):
                    ddd = string[:2]
                elif length in (12,13):
                    ddd = string[2:4]
                digit = '9 '
                if phone[0] == '3':
                    digit = ''
                return f'+55 {ddd} {digit}{phone[:4]}-{phone[4:]}'

    def confirm(self, *args):
        self.client_cod = self.search_lbox.get(self.search_lbox.curselection())[:8]
        name = self.search_lbox.get(self.search_lbox.curselection())[9:]
        
        if self.operation == 'update':
            try:
                self.client_cod = int(self.client_cod)
                window = ClientRegister(
                    self.container, 
                    operation=self.operation, 
                    cod=self.client_cod, 
                    history_update=self.history_update
                )
                window.focus()
                self.conn.close()
                self.destroy()
            except:
                pass

        elif self.operation == 'new_credit_sale':
            self.conn.close()
            window = SaleRegister(
                self.container,
                cod=self.client_cod,
                name=name,
                history_update=self.history_update
            )
            window.focus()
            self.destroy()

        elif self.operation == 'show_client':
            self.conn.close()
            window = ClientWindow(
                self.container,
                cod=self.client_cod,
                history_update=self.history_update
            )
            window.focus()
            self.destroy()

        elif self.operation == 'remove':
            # try:
            self.cursor.execute("""
            delete from client where cod == ?
            """,(int(self.client_cod),))
            self.conn.commit()
            self.conn.close()

            cod = f'\n>> CÓDIGO DO CLIENTE:\t{self.format(tp="cod", string=str(self.client_cod))}'
            name = f'\n>> NOME:\t{name.upper()}'
            log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > REMOVER CLIENTE {cod}{name}\n"
            self.history_update(log_string)

            # except: messagebox.showerror('ERRO', 'NÃO FOI POSSÍVEL REMOVER O CLIENTE!')

            self.destroy()

    def cancel(self, *args): 
        self.conn.close()
        self.destroy()