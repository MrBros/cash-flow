import tkinter as tk
from tkinter import ttk
from datetime import datetime
from functions import to_br_currency, to_us_float, is_number, hex_to_int

class Preview(tk.Frame): 
    def __init__(self, container, sheet_object=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.__container = container
        self.__sheet = sheet_object
        self.__inputs_coordinates = []
        self.__outputs_coordinates = []
        self.__history_update = history_update
         
        self.__table_in = ttk.Treeview(
            self,
            selectmode='browse',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.__table_in_scrollbar = ttk.Scrollbar(
            self, 
            orient="vertical", 
            command=self.__table_in.yview, 
            cursor="hand2"
        )
        self.__table_in.configure(yscrollcommand=self.__table_in_scrollbar.set)
        self.__table_out = ttk.Treeview(
            self,
            selectmode='browse',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.__table_out_scrollbar = ttk.Scrollbar(
            self, 
            orient="vertical", 
            command=self.__table_out.yview, 
            cursor="hand2"
        )
        self.__table_out.configure(yscrollcommand=self.__table_out_scrollbar.set)
    
        self.__table_in_label = ttk.Label(
            self,
            text='Entradas'
        )
        self.__table_out_label = ttk.Label(
            self,
            text='Saídas'
        )

        self.__update_button = ttk.Button(
            self,
            cursor='hand2',
            text='Atualizar [F8]',
            command=self.alter_table_data,
            state='disabled'
        )
        self.__delete_button = ttk.Button(
            self,
            cursor='hand2',
            text='Excluir [F9]',
            command=self.delete_table_data,
            state='disabled'
        )
        
        # grid
        ## Preview
        self.rowconfigure(1, weight=1)
        self.columnconfigure((0,2), weight=1)
        self.grid(row=0, column=1, sticky='NSEW')
        ## Table IN
        self.__table_in_label.grid(row=0, column=0, padx=20, pady=(20,0))
        self.__table_in.grid(row=1, column=0, padx=(10,0), pady=(10,20), sticky='NSEW')
        self.__table_in_scrollbar.grid(row=1, column=1, padx=0, pady=(10,20), sticky="WNS")
        ## Table OUT
        self.__table_out_label.grid(row=0, column=2, padx=20, pady=(20,0))
        self.__table_out.grid(row=1, column=2, padx=(10,0), pady=(10,20), sticky='NSEW')
        self.__table_out_scrollbar.grid(row=1, column=3, padx=(0,20), pady=(10,20), sticky="WNS")
        ## Buttons
        self.__update_button.grid(row=2, column=0, padx=(10,0), pady=(0,20), sticky='EW')
        self.__delete_button.grid(row=2, column=2, padx=(10,0), pady=(0,20), sticky='EW')
            
           # self.get_table_data(table)
        
        # Binds
        self.__table_in.bind('<FocusIn>', lambda arg: self.switch_table_selection(self.__table_out))
        self.__table_out.bind('<FocusIn>', lambda arg: self.switch_table_selection(self.__table_in))
        self.__table_in.bind('<KP_Enter>', lambda arg: self.__update_button.focus())
        self.__table_in.bind('<Return>', lambda arg: self.__update_button.focus())
        self.__table_out.bind('<KP_Enter>', lambda arg: self.__update_button.focus())
        self.__table_out.bind('<Return>', lambda arg: self.__update_button.focus())
        self.__update_button.bind('<KP_Enter>', self.alter_table_data)
        self.__update_button.bind('<Return>', self.alter_table_data)
        self.__delete_button.bind('<KP_Enter>', self.delete_table_data)
        self.__delete_button.bind('<Return>', self.delete_table_data)

    def clear_tables_selection(self, *args):
        for table in (self.__table_in, self.__table_out):
            if len(table.selection()) > 0:
                table.selection_remove(table.selection()[0])
                
        self.__update_button['state'] = 'disabled'
        self.__delete_button['state'] = 'disabled'

    def switch_table_selection(self, other_table, *args):
        if len(other_table.selection()) > 0:
            other_table.selection_remove(other_table.selection()[0])

        self.__update_button['state'] = 'normal'
        self.__delete_button['state'] = 'normal'   
    
    def set_table_data(self, table, min_col=1, max_col=3, *args):
        for row in table.get_children():
            table.delete(row)

        for row in self.__sheet.iter_rows(min_row=4, max_row=32, min_col=min_col, max_col=max_col, values_only=True):
            table.insert("", "end", values=row)

    def alter_table_data(self, *args):
        table = self.__table_in
        selection = table.selection()
        coord_array = self.__inputs_coordinates
        win = 'cash_in'
        if len(selection) == 0: 
            table = self.__table_out
            selection = table.selection()
            coord_array = self.__outputs_coordinates
            win = 'cash_out'

        for row in selection:
            values = table.item(row,"values")
            value = str(values[2]).replace('R$ ','')
            index = hex_to_int(str(row).replace('I',''))-1
            coordinates = coord_array[index]
            
            self.__container.call_window(
                operation='update', 
                wintype=win, 
                values=[values[0], values[1], value]
            )
        
        self.__update_button['state'] = 'disabled'
        self.__delete_button['state'] = 'disabled'
    
    def delete_table_data(self, *args):  
        table = self.__table_in
        selection = table.selection()
        coord_array = self.__inputs_coordinates
        win = 'cash_in'
        if len(selection) == 0: 
            table = self.__table_out
            selection = table.selection()
            coord_array = self.__outputs_coordinates
            win = 'cash_out'

        for row in selection:
            index = hex_to_int(str(row).replace('I',''))-1
            coordinates = coord_array[index]

            desc, pay, val, op, tp, log_string = ['','','','','','']
            old_values = self.__sheet.delete(loc=win, coordinates=coordinates)
            if old_values[0] is not None:
                op = 'REMOVER '
                tp = 'ENTRADA'
                log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > "
                if table == self.__table_out: tp = 'SAÍDA'
                desc = '\n>> DESCRIÇÃO:\t'+old_values[0].upper()
            if old_values[1] is not None:
                pay = '\n>> MÉTODO DE PAGAMENTO:\t'+old_values[1].upper()
            if old_values[2] is not None:
                val = '\n>> VALOR:\t'+to_br_currency(to_us_float(str(old_values[2])))
            
            log_string = f"{log_string}{op}{tp}{desc}{pay}{val}\n"
            self.__history_update(log_string) 
        
        self.set_table_data(table)
            
        self.__update_button['state'] = 'disabled'
        self.__delete_button['state'] = 'disabled'

    def tables_resize(self, *args):
        self.update()
        self.table_dimensions = (self.__table_in.winfo_width(), self.__table_in.winfo_height())
        
        for table in (self.__table_in, self.__table_out):
            table.yview_moveto(1)
            table.column('column1', minwidth=int(0.335*self.table_dimensions[0]), width=int(0.335*self.table_dimensions[0]), anchor=tk.W)
            table.heading('#1', text='Descrição')
            table.column('column2', minwidth=int(0.250*self.table_dimensions[0]), width=int(0.330*self.table_dimensions[0]), anchor=tk.CENTER)
            table.heading('#2', text='Pagamento')
            table.column('column3', minwidth=int(0.190*self.table_dimensions[0]), width=int(0.330*self.table_dimensions[0]), anchor=tk.E)
            table.heading('#3', text='Valor')
