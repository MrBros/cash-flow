import os
import sys
import tkinter as tk
from home import Home
from sheet import Sheet
from window import Window
from search import Search
from preview import Preview
import tkinter.font as font
from tkinter import messagebox
from tkinter import ttk, filedialog
from datetime import date, datetime
from creditsales import CreditSales
from menutooltip import MenuTooltip
from clientregister import ClientRegister
from functions import to_br_currency, to_us_float, is_number, hex_to_int

class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Window Settings
        self.resizable(False, False)
        self.title('Controlador de Fluxo de Caixa')
        self.__fullscreen = True
        self.attributes('-fullscreen', self.__fullscreen)

        # Menu
        self.__menubar = MenuTooltip(parent=self)
        self.config(menu=self.__menubar)
        # Submenu
        self.__archive_menu = MenuTooltip(parent=self.__menubar)
        self.__credit_sales_menu = MenuTooltip(parent=self.__menubar)
        self.__client_menu = MenuTooltip(parent=self.__menubar)
        self.__help_menu = MenuTooltip(parent=self.__menubar)
        # Sub-submenu
        self.__open_submenu = MenuTooltip(parent=self.__archive_menu)
        ## Cascades
        self.__menubar.add_cascade(label='Arquivo', menu=self.__archive_menu)
        self.__menubar.add_cascade(label='Cliente', menu=self.__client_menu)
        self.__menubar.add_cascade(label='Notinhas', menu=self.__credit_sales_menu)
        self.__menubar.add_cascade(label='Ajuda', menu=self.__help_menu)
        self.__archive_menu.add_cascade(label='Abir', menu=self.__open_submenu)
        ### Commands
        ### Archive Menu
        self.__open_submenu.add_command(label='Planilha atual', command=lambda: self.__sheet.OnOpen(witch='current'))
        self.__open_submenu.add_command(label='Outra planilha', command=lambda: self.__sheet.OnOpen(witch='other'))
        self.__archive_menu.add_command(label='Relatório', command=self.generate_relatory)
        self.__archive_menu.add_command(label='Mudar visualização', command=self.switch_view)
        self.__archive_menu.add_command(label='Sair', command=self.on_closing)
        ### Client Menu
        self.__client_menu.add_command(label='Cadastrar', command=lambda: ClientRegister(self, operation='insert', history_update=lambda arg: self.__home.history_update(arg)))
        self.__client_menu.add_command(label='Atualizar ', command=lambda: Search(self, operation='update', history_update=lambda arg: self.__home.history_update(arg)))
        self.__client_menu.add_command(label='Remover', command=lambda: Search(self, operation='remove', history_update=lambda arg: self.__home.history_update(arg)))
        ### Credit Sales Menu
        self.__credit_sales_menu.add_command(label='Ver notinha', command=lambda: Search(self, operation='show_client', history_update=lambda arg: self.__home.history_update(arg)))
        ### Help Menu
        self.__help_menu.add_command(label='Ajuda', command=self.help)

        # Variables
        self.__today = date.today().strftime("%Y%m%d")
        self.__yesterday = date.fromordinal(date.today().toordinal() - 1).strftime("%Y%m%d")
        self.__wintype_dict = {
            'cash_in': 'Entrada',
            'cash_out': 'Saída',
            'sales': 'Vendas',
            'cashier': 'Caixa II',
            'card_sales': 'Venda no Cartão',
        }
        self.__operation_dict = {
            'insert': 'Inserir',
            'update': 'Atualizar',
            'delet': 'Remover'
        }

        # Paths
        log_dir_path = os.getcwd()+r'/logs'
        tmp_dir_path = log_dir_path+r'/tmp'
        
        try:
            os.makedirs(log_dir_path)
        except OSError:
            pass
        try:
            os.makedirs(tmp_dir_path)
        except OSError:
            pass

        self.__tmp_file_path = fr'{tmp_dir_path}/{self.__today}.txt'
        self.__log_file_path = fr'{log_dir_path}/{self.__today}.txt'
        self.__sheet_dir_path = fr'{os.getcwd()}/output/{datetime.now().year}/'
        self.__sheet_file_path = fr'{os.getcwd()}/output/{datetime.now().year}/{datetime.now().month}.xlsx'

        # Classes
        self.__sheet = Sheet(sheet_file_path=self.__sheet_file_path)
        self.__creditsales = CreditSales()

        # Home Frame
        self.__home = Home(
            self,
            log_file_path=self.__log_file_path,
            tmp_file_path=self.__tmp_file_path,
            yesterday_file_path=fr'{tmp_dir_path}/{self.__yesterday}.txt',
            clear_tables_selection=self.clear_tables_selection
        )
        # Preview Frame
        self.__preview = Preview(
            self,
            sheet_object=self.__sheet,
            history_update=lambda arg: self.__home.history_update(arg)
        )
        
        # Protocols
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Grid
        self.rowconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.__home.grid(row=0, column=0, sticky='NSEW')
        self.__preview.grid(row=0, column=1, sticky='NSEW')
        self.__preview.tables_resize()
        
        # Binds
        ## App
        self.bind('<Control-o>', lambda arg: self.__sheet.OnOpen(witch='current'))
        self.bind('<Control-k>', lambda arg: self.__sheet.OnOpen(witch='other'))
        self.bind('<F2>', lambda arg: self.call_window(operation='insert', wintype='cash_in'))
        self.bind('<F3>', lambda arg: self.call_window('insert', 'cash_out'))
        self.bind('<F4>', lambda arg: self.call_window('insert', 'cashier'))
        self.bind('<F5>', lambda arg: self.call_window('insert', 'card_sales'))
        self.bind('<F6>', lambda arg: Search(self, operation='new_credit_sale'))
        self.bind('<F10>', lambda arg: Search(self, operation='show_client'))
        self.bind('<F11>', self.switch_view)

    def on_closing(self):
        if messagebox.askokcancel("Atenção", "Você realmente deseja sair?"):
            counter, amount = self.__home.get_sales_var()
            self.__sheet.insert('sales', description=counter, value=amount)
            self.destroy()
    
    def call_window(self, operation=None, wintype=None, values=['', 'DINHEIRO', ''], *args):
        description_flag, payment_flag, value_flag = (True, True, True)

        if wintype == 'card_sales':
            description_flag = False
        
        elif wintype == 'cashier':
            description_flag = False
            payment_flag = False

        old_values = values
        window = Window(
            container=self, 
            operation=operation,
            printable_operation=self.__operation_dict[operation],
            wintype=wintype,
            printable_wintype=self.__wintype_dict[wintype],
            description=description_flag, 
            payment_method=payment_flag, 
            value=value_flag, 
            values=values,
        )
        new_values = window.values
        
        if new_values != [None, None, None]:
            try:
                self.__sheet.insert(
                    loc=wintype, 
                    description=new_values[0],
                    payment_method=new_values[1],
                    value=new_values[2]
                )
                log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {self.__operation_dict[operation].upper()} {self.__wintype_dict[wintype].upper()}\
                \n>> DESCRIÇÃO: {str(new_values[0]).upper()}\
                \n>> PAGAMENTO: {str(new_values[1]).upper()}\
                \n>> VALOR: {to_br_currency(float(new_values[2]))}"
                self.__home.history_update(log_string)

            except:
                messagebox.showerror(title='Erro', message='Não foi possível realizar essa operação!')
       
        self.__home.focus_on_command_entry()
        
    def switch_view(self, *args):
        if not self.__fullscreen:
            self.__preview.grid(row=0, column=1, sticky='NSEW')
        else:
            self.__preview.grid_forget()
        
        self.__fullscreen = not self.__fullscreen
        self.attributes('-fullscreen', self.__fullscreen)
    
    def clear_tables_selection(self, *args): self.__preview.clear_tables_selection()

    def generate_relatory(self, *args):
        messagebox.showinfo("Ops","Essa funcionalidade ainda não foi implementada!")

    def help(self, *args):
        messagebox.showinfo("Ops","Essa funcionalidade ainda não foi implementada!")

    def close(self, *args):
        counter, amount = self.__home.get_sales_var()
        self.__sheet.insert('sales', description=counter, value=amount)
        log_string = f"""
        EXPEDIENTE FINALIZADO COM SUCESSO!\n
        DIA: {date.today().strftime("%d/%m/%Y")}\n
        VENDAS REALIZADAS: {counter}\n
        VALOR TOTAL: {to_br_currency(amount)}\n
        """
        messagebox.showinfo("Informação", log_string)

        self.__home.history_update(f"{datetime.now().strftime('%H:%M:%S')} >\n"+log_string)          
        
        self.destroy()

def main():
    root = App()

    style = ttk.Style(root)
    style.theme_use('xpnative')

    root.mainloop()
 
if __name__ == '__main__':
    main()


