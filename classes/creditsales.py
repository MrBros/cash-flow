import sqlite3
from sheet import Sheet
import io
import collections
from tkinter import messagebox
import json
from datetime import date, datetime

class CreditSales():
    def __init__(self, *args, **kwargs):
        self.connection = None
        self.cursor = None

    def backup(self):
        self.connection = sqlite3.connect('credit_sales.db')
        self.cursor = self.connection.cursor()

        with io.open('credit_sales_dump.sql', 'w') as f:
            for linha in self.connection.iterdump():
                f.write('%s\n' % linha)

        self.connection.commit()
        self.connection.close()
        
    
    def restore_backup(self):
        self.connection = sqlite3.connect('credit_sales.db')
        self.cursor = self.connection.cursor()

        f = io.open('clientes_dump.sql', 'r')
        sql = f.read()
        self.cursor.executescript(sql)

        self.connection.commit()
        self.connection.close()

    def create_database(self):
        self.connection = sqlite3.connect('credit_sales.db')
        self.cursor = self.connection.cursor()

        self.cursor.execute("""
        create table client(
            cod integer not null primary key autoincrement,
            name varchar(50) not null,
            cpf varchar(11),
            rg varchar(9),
            contact json not null,
            address json not null,
            c_limit float,
            c_status varchar(10) not null,
            comment varchar(200)
        );
        """)

        self.cursor.execute("""
        create table sale(
            cod integer not null primary key autoincrement,
            client_cod serial not null,
            items json not null,
            value float not null,
            s_date date not null,
            authentication varchar(100),

            foreign key (client_cod) references client(cod)
        );
        """)

        self.cursor.execute("""
        create table expired_sale(
            cod integer not null primary key autoincrement,
            client_cod serial not null,
            items json not null,
            value float not null,
            expiration_date date not null,
            paid boolean not null,

            foreign key (client_cod) references client(cod)
        );
        """)

        self.connection.commit()
        self.connection.close()
    
    def initialize_database(self):
        try:
            self.create_database()
        except:
            self.restore_backup()
    
    def close_client_sales(self, client_cod):
        self.connection = sqlite3.connect('credit_sales.db')
        self.cursor = self.connection.cursor()
        
        items = dict()
        value = 0.0
        expiration_date = datetime.now()
        paid = False

        self.cursor.execute("""
        select * from sale
        where client_cod == ?
        order by client_cod
        """,(client_cod,))
        fetched = self.cursor.fetchall()

        if len(fetched) > 0:
            for line in self.cursor.fetchall():
                items.update({
                    'cod': line[0],
                    'client_cod': line[1],
                    'items': line[2],
                    'value': line[3],
                    's_date': line[4],
                    'authentication': line[5]
                })
                value += line[3]
            
            self.cursor.execute("""
            insert into expired_sale(client_cod, items, value, expiration_date, paid) 
            values (?,?,?,?,?)
            """,(client_cod, json.dumps(items), value, expiration_date, paid))
        
        self.cursor.execute("""
        delete from sale where client_cod == ?
        """,(client_cod,))
        
        self.connection.commit()
        self.connection.close()

    def close_month_sales(self):
        if int(datetime.now().strftime('%d')) == 1:
            self.connection = sqlite3.connect('credit_sales.db')
            self.cursor = self.connection.cursor()
            
            self.cursor.execute("""
            select client_cod from sales
            group by client_cod
            """)
            clients = self.cursor.fetchall()

            self.connection.commit()
            self.connection.close()

            for client in clients:
                self.close_client_sales(client)

    
    # def pay(self, month:int, value:float):
    #     if month == int(datetime.now().strftime('%m')):
        
    #     else:

        
