from cx_Freeze import setup, Executable
 
setup(name='Caixa',
    version='3.0',
    description='Controlador de fluxo de caixa',
    options={
        'build_exe': {
            'packages': [
                'os',
                'datetime',
                'openpyxl',
                'tkinter'
            ]
        }
    },
    executables = [
        Executable(script='app.py',base=None,icon='icon.png')
    ]
)
