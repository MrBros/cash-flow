
import sqlite3
import tkinter as tk
from tkinter import ttk
from sheet import Sheet
import io
import collections
from tkinter import messagebox
import json
from datetime import date, datetime

class ClientRegister(tk.Toplevel):
    def __init__(self, container, operation='insert', cod=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.operation_dict = {
            'insert': 'Inserir',
            'delete': 'Remover',
            'update': 'Alterar'
        }

        
        self.title(f'{self.operation_dict[operation].upper()} CLIENTE')
        self.resizable(False, False)
        self.rowconfigure((1,2,3,4,5,6), weight=1)
        self.columnconfigure(0, weight=1)

        self.values = None
        self.operation = operation
        self.client_cod = cod
        self.history_update = history_update
        self.reg_digit = self.register(self.callback_digit)
        self.reg_float = self.register(self.callback_float)

        try:
            self.client = self.get_client_info()
        except:
            self.client = {
            'cod':      self.client_cod,
            'name':     '',
            'cpf':      '',
            'rg':       '',
            'phone_1':  '',
            'phone_2':  '',
            'street':   '',
            'number':   '',
            'district': 'AEROPORTO 1',
            'city':     'BOM DESPACHO',
            'limit':    '100,00',
            'status':   'ABERTO',
            'comment':  ''
        }

        self.error_string = tk.StringVar()
        self.error_label = ttk.Label(
            self,
            textvariable=self.error_string,
            foreground='red'
        )

        # name 
        self.name_frame = ttk.Frame(self)
        self.name_label = ttk.Label(
            self.name_frame,
            text='* NOME',
        ) 
        self.name_string = tk.StringVar()
        self.name_string.set(self.client["name"])
        self.name_entry = ttk.Entry(
            self.name_frame,
            textvariable=self.name_string,
            width=50,
        ) 

        # documents
        self.inf_frame = ttk.Frame(self)
        self.cpf_label = ttk.Label(
            self.inf_frame,
            text='CPF'
        ) 
        self.cpf_string = tk.StringVar()
        self.cpf_string.set(self.client["cpf"])
        self.cpf_entry = ttk.Entry(
            self.inf_frame,
            textvariable=self.cpf_string,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        ) 
        self.rg_label = ttk.Label(
            self.inf_frame,
            text='RG'
        ) 
        self.rg_string = tk.StringVar()
        self.rg_string.set(self.client["rg"])
        self.rg_entry = ttk.Entry(
            self.inf_frame,
            textvariable=self.rg_string,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        ) 
        
        # Contact 
        self.contact_frame = ttk.Frame(self)
        self.contact_title = ttk.Label(
            self.contact_frame,
            text='CONTATO',
        )
        self.phone_1_label = ttk.Label(
            self.contact_frame,
            text='* TELEFONE 1'
        ) 
        self.phone_1_string = tk.StringVar()
        self.phone_1_string.set(self.client["phone_1"])
        self.phone_1_entry = ttk.Entry(
            self.contact_frame,
            textvariable=self.phone_1_string,
            width=20,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        )  
        self.phone_2_label = ttk.Label(
            self.contact_frame,
            text='TELEFONE 2',
        ) 
        self.phone_2_string = tk.StringVar()
        self.phone_2_string.set(self.client["phone_2"])
        self.phone_2_entry = ttk.Entry(
            self.contact_frame,
            textvariable=self.phone_2_string,
            width=20,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        )

        # address
        self.address_frame = ttk.Frame(self)
        self.address_title = ttk.Label(
            self.address_frame,
            text='ENDEREÇO',
        )
        self.street_label = ttk.Label(
            self.address_frame,
            text='* RUA/AVENIDA'
        ) 
        self.street_string = tk.StringVar()
        self.street_string.set(self.client["street"])
        self.street_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.street_string,
            width=53
        )  
        self.number_label = ttk.Label(
            self.address_frame,
            text='* NÚMERO'
        ) 
        self.number_string = tk.StringVar()
        self.number_string.set(self.client["number"])
        self.number_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.number_string,
            width=9,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        )  
        self.district_label = ttk.Label(
            self.address_frame,
            text='* BAIRRO',
        ) 
        self.district_string = tk.StringVar()
        self.district_string.set(self.client["district"])
        self.district_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.district_string,
            width=25
        )
        self.city_label = ttk.Label(
            self.address_frame,
            text='* CIDADE',
        ) 
        self.city_string = tk.StringVar()
        self.city_string.set(self.client["city"])
        self.city_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.city_string,
            width=25
        )

        # limit
        self.limit_frame = ttk.Frame(self)
        self.limit_label = ttk.Label(
            self.limit_frame,
            text='* LIMITE MENSAL',
        ) 
        self.limit_string = tk.StringVar()
        self.limit_string.set(self.client["limit"])
        self.limit_entry = ttk.Entry(
            self.limit_frame,
            textvariable=self.limit_string,
            validate="key", 
            validatecommand=(self.reg_float, '%P')
        )
        self.status_label = ttk.Label(
            self.limit_frame,
            text='* STATUS',
        ) 
        self.status_cbox = ttk.Combobox(
            self.limit_frame,
            values=['ABERTO', 'SUSPENSO', 'FECHADO'],
            state='readonly'
        )
        self.status_cbox.set(self.client["status"])
        self.comment_label = ttk.Label(
            self.limit_frame,
            text='COMENTÁRIOS ADICIONAIS',
        ) 
        self.comment_box = tk.Text(
            self.limit_frame,
            height=5,
            width=50
        )
        self.comment_box.insert(1.0, self.client["comment"])

        # buttons
        self.buttons_frame = ttk.Frame(self)
        self.confirm_button = ttk.Button(
            self.buttons_frame,
            text='CONFIRMAR [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.cancel_button = ttk.Button(
            self.buttons_frame,
            text='CANCELAR [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.name_entry, True],
            [self.cpf_entry, True],
            [self.rg_entry, True],
            [self.phone_1_entry, True],
            [self.phone_2_entry, True],
            [self.street_entry, True],
            [self.number_entry, True],
            [self.district_entry, True],
            [self.city_entry, True],
            [self.limit_entry, True],
            [self.status_cbox, True],
            [self.comment_box, True],
            [self.confirm_button, True],
            [self.cancel_button, True]
        ]
        self.widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.error_label.grid(row=0, column=0, padx=20, pady=10)

        self.name_frame.grid(row=1, column=0, sticky='NSEW')
        self.name_frame.rowconfigure(2, weight=1)
        self.name_frame.columnconfigure(0, weight=1)
        self.name_label.grid(row=0, column=0, padx=20, pady=0, sticky='W')
        self.name_entry.grid(row=1, column=0, padx=20, pady=(0,10), sticky='EW')

        self.inf_frame.grid(row=2, column=0, sticky='NSEW')
        self.inf_frame.rowconfigure(1, weight=1)
        self.inf_frame.columnconfigure((0,1), weight=1)
        self.cpf_label.grid(row=0, column=0, padx=(20, 10), pady=0, sticky='W')
        self.cpf_entry.grid(row=1, column=0, padx=(20, 10), pady=(0,10), sticky='EW')
        self.rg_label.grid(row=0, column=1, padx=(10, 20), pady=0, sticky='W')
        self.rg_entry.grid(row=1, column=1, padx=(10, 20), pady=(0,10), sticky='EW')

        self.contact_frame.grid(row=3, column=0, sticky='NSEW')
        self.contact_frame.rowconfigure(1, weight=1)
        self.contact_frame.columnconfigure((0,1), weight=1)
        self.phone_1_label.grid(row=0, column=0, padx=(20, 10), pady=(10,0), sticky='W')
        self.phone_1_entry.grid(row=1, column=0, padx=(20, 10), pady=(0,10), sticky='EW')
        self.phone_2_label.grid(row=0, column=1, padx=(10, 20), pady=(10,0), sticky='W')
        self.phone_2_entry.grid(row=1, column=1, padx=(10, 20), pady=(0,10), sticky='EW')

        self.address_frame.grid(row=4, column=0, sticky='NSEW')
        self.address_frame.rowconfigure((1,3), weight=1)
        self.address_frame.columnconfigure((0,1), weight=1)
        self.street_label.grid(row=0, column=0, padx=(20, 10), pady=(10,0), sticky='W')
        self.street_entry.grid(row=1, column=0, padx=(20, 10), pady=(0,10), columnspan=2, sticky='W')
        self.number_label.grid(row=0, column=1, padx=(10, 20), pady=(10,0), sticky='E')
        self.number_entry.grid(row=1, column=1, padx=(10, 20), pady=(0,10), sticky='E')
        self.district_label.grid(row=2, column=0, padx=(20, 10), pady=0, sticky='W')
        self.district_entry.grid(row=3, column=0, padx=(20, 10), pady=(0,10) ,sticky='EW')
        self.city_label.grid(row=2, column=1, padx=(10, 20), pady=0, sticky='W')
        self.city_entry.grid(row=3, column=1, padx=(10, 20), pady=(0,10), sticky='EW')

        self.limit_frame.grid(row=5, column=0, sticky='NSEW')
        self.limit_frame.rowconfigure(1, weight=1)
        self.limit_frame.columnconfigure((0, 1), weight=1)
        self.limit_label.grid(row=0, column=0, padx=(20, 10), pady=(10, 0), sticky='W')
        self.limit_entry.grid(row=1, column=0, padx=(20, 10), pady=(0, 10), sticky='EW')
        self.status_label.grid(row=0, column=1, padx=(10, 20), pady=(10, 0), sticky='W')
        self.status_cbox.grid(row=1, column=1, padx=(10, 20), pady=(0, 10), sticky='EW')
        self.comment_label.grid(row=2, column=0, padx=20, pady=0, sticky='W')
        self.comment_box.grid(row=3, column=0, padx=20, pady=(0, 20), sticky='EW', columnspan=2)

        self.buttons_frame.grid(row=6, column=0, columnspan=2, sticky='NSEW')
        self.buttons_frame.rowconfigure(1, weight=1)
        self.buttons_frame.columnconfigure((0,1), weight=1)
        self.confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.name_entry.bind('<Return>', lambda arg: self.focus_next(self.name_entry))
        self.name_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.name_entry))

        self.cpf_entry.bind('<Return>', lambda arg: self.focus_next(self.cpf_entry))
        self.cpf_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.cpf_entry))

        self.rg_entry.bind('<Return>', lambda arg: self.focus_next(self.rg_entry))
        self.rg_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.rg_entry))

        self.phone_1_entry.bind('<Return>', lambda arg: self.focus_next(self.phone_1_entry))
        self.phone_1_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.phone_1_entry))

        self.phone_2_entry.bind('<Return>', lambda arg: self.focus_next(self.phone_2_entry))
        self.phone_2_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.phone_2_entry))

        self.street_entry.bind('<Return>', lambda arg: self.focus_next(self.street_entry))
        self.street_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.street_entry))

        self.number_entry.bind('<Return>', lambda arg: self.focus_next(self.number_entry))
        self.number_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.number_entry))

        self.district_entry.bind('<Return>', lambda arg: self.focus_next(self.district_entry))
        self.district_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.district_entry))

        self.city_entry.bind('<Return>', lambda arg: self.focus_next(self.city_entry))
        self.city_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.city_entry))

        self.limit_entry.bind('<Return>', lambda arg: self.focus_next(self.limit_entry))
        self.limit_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.limit_entry))

        self.status_cbox.bind('<Return>', lambda arg: self.focus_next(self.status_cbox))
        self.status_cbox.bind('<KP_Enter>', lambda arg: self.focus_next(self.status_cbox))

        self.confirm_button.bind('<Return>', self.confirm)
        self.confirm_button.bind('<KP_Enter>', self.confirm)

        self.cancel_button.bind('<Return>', self.cancel)
        self.cancel_button.bind('<KP_Enter>', self.cancel)

        self.focus_next(self.cancel_button)

    def callback_digit(self, input):
        if input.isdigit():
            return True
        elif input == '':
            return True
        else:
            return False
  
    def error_check(self, *args):
        for widget in (self.name_string, self.street_string, self.number_string, self.district_string, self.city_string, self.limit_string, self.status_cbox, self.phone_1_string):
            if widget.get() is None or widget.get() == '':
                self.error_string.set('PREENCHA TODOS OS CAMPOS OBRIGATÓRIOS (*)')
                self.focus_next(self.cancel_button)
                return True
        
        self.error_string.set('')
        return False
        
    def isnumber(self, value):
        try:
            float(value)
        except ValueError:
            return False
        return True
   
    def callback_float(self, input): # The string must be in BRL floating format
        input = input.replace('.','').replace(',','.') 
        if input == '.': input = '0.0'
        elif input == '-': input = '-0.0'
        elif input == '+': input = '+0.0'
        if self.isnumber(input):
            return True
        elif input == '':
            return True
        return False  
    
    def to_us_float(self, string: str): # The string must be in BRL floating format
        string = string.replace(',','.') 
        if self.isnumber(string):
            return float(string)
        return False  

    def focus_next(self, previous_widget):
        while self.widget_deque[0][0] != previous_widget:
            self.widget_deque.rotate(-1)
        while not self.widget_deque[1][1]:
            self.widget_deque.rotate(-1)
       
        self.widget_deque[1][0].focus()
        if isinstance(self.widget_deque[1][0], ttk.Entry):
            self.widget_deque[1][0].select_range(0, 'end')

    def get_client_info(self, *args):
        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
        cursor.execute("""
        select * from client where cod == ? limit 1
        """,(self.client_cod,))
        
        queue = cursor.fetchall()[0]
        contact = json.loads(queue[4])
        address = json.loads(queue[5])
        client = {
            'cod':      int(queue[0]),
            'name':     str(queue[1]),
            'cpf':      str(queue[2]),
            'rg':       str(queue[3]),
            'phone_1':  str(contact["phone_1"]),
            'phone_2':  str(contact["phone_2"]),
            'street':   str(address["street"]),
            'number':   str(address["number"]),
            'district': str(address["district"]),
            'city':     str(address["city"]),
            'limit':    str(queue[6]).replace('.',','),
            'status':   str(queue[7]),
            'comment':  str(queue[8])
        }
        conn.commit()
        conn.close()

        return client

    def confirm(self, *args):
        if not self.error_check():
            name = self.name_string.get().upper()
            cpf = self.cpf_string.get()
            rg = self.rg_string.get()
            contact = { 
                "phone_1": self.phone_1_string.get(), 
                "phone_2": self.phone_2_string.get() 
            }
            address = { 
                "street": self.street_string.get().upper(), 
                "number": int(self.number_string.get()), 
                "district": self.district_string.get().upper(), 
                "city": self.city_string.get().upper()
            }
            limit = self.to_us_float(self.limit_string.get())
            status = self.status_cbox.get().upper()
            comment = self.comment_box.get(1.0,'end').upper()
            self.values = (name, cpf, rg, json.dumps(contact), json.dumps(address), limit, status, comment)
            try:
                conn = sqlite3.connect('credit_sales.db')
                cursor = conn.cursor()
                cursor.execute("""
                select name from client where name == ?
                """, (name,))
                if len(cursor.fetchall()) == 0 and self.operation == 'insert':
                    cursor.execute("""
                        insert into client (name, cpf, rg, contact, address, c_limit, c_status, comment) 
                        values (?,?,?,?,?,?,?,?)
                    """, self.values)
                    cursor.execute("""
                        select cod from client where name == ? limit 1
                    """, (self.values[0],))
                    self.client_cod = cursor.fetchall()[0][0]
                elif len(cursor.fetchall()) != 0:
                    messagebox.showerror('ERRO', 'CLIENTE JÁ CADASTRADO!')
                if self.operation == 'update':
                    cursor.execute("""
                        update client 
                        set 
                        name = ?, 
                        cpf = ?, 
                        rg = ?, 
                        contact = ?, 
                        address = ?, 
                        c_limit = ?, 
                        c_status = ?, 
                        comment = ? 
                        where cod = ?
                    """, (self.values[0], self.values[1], self.values[2], self.values[3], self.values[4], self.values[5], self.values[6], self.values[7], self.client_cod))
                conn.commit()
                conn.close()
                
                cod = f'\n>> CÓDIGO DO CLIENTE:\t{self.format(tp="cod", string=str(self.client_cod))}'
                name = f'\n>> NOME:\t{self.values[0].upper()}'
                log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {self.operation_dict[self.operation].upper()} CLIENTE {cod}{name}\n"
                self.history_update(log_string)

            except:
                messagebox.showerror('ERRO', 'NÃO FOI POSSÍVEL CADASTRAR O CLIENTE!')
            self.destroy()
    
    def format(self, tp=None, string=None, *args):
        length = len(string)
        
        if string == '':
            return string

        if tp == 'cod':
            cod = string
            for i in range(0, 8-length):
                cod = '0' + cod
            return cod

        elif tp == 'cpf':
            if length != 11:
                return 'INVÁLIDO'
            else:
                return f'{string[:3]}.{string[3:6]}.{string[6:9]}-{string[9:]}'

        elif tp == 'rg':
            if length not in (8,9):
                return 'INVÁLIDO'
            elif length == 8:
                return f'{string[:2]}.{string[2:5]}.{string[5:]}'
            elif length == 9:
                return f'{string[:2]}.{string[2:5]}.{string[5:8]}-{string[8:]}'

        elif tp == 'phone':
            if length not in (8,9,10,11,12,13):
                return 'INVÁLIDO'
            else:
                phone = string[length-8:]
                ddd = '37'
                if length in (10,11):
                    ddd = string[:2]
                elif length in (12,13):
                    ddd = string[2:4]
                digit = '9 '
                if phone[0] == '3':
                    digit = ''
                return f'+55 {ddd} {digit}{phone[:4]}-{phone[4:]}'

    def cancel(self, *args): 
        self.destroy()
