import sqlite3
import tkinter as tk
from tkinter import ttk
from sheet import Sheet
import io
import collections
from tkinter import messagebox
import json
from datetime import date, datetime
from clientregister import ClientRegister
from infobox import InfoBox

class ClientWindow(tk.Toplevel):
    def __init__(self, container, cod=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.title('VENDAS A PRAZO')
        self.resizable(False, False)

        # grid
        # ClientWindow
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)

        self.client_cod = int(cod)
        self.container = container
        self.client = self.get_client_info()
        self.item_string = []
        self.history_update = history_update

        # client_frame
        self.client_frame = ttk.Frame(self)
        # cod
        self.client_cod_string = tk.StringVar()
        self.client_cod_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_cod_string 
        )
        # name
        self.client_name_string = tk.StringVar()
        self.client_name_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_name_string 
        )
        # address
        self.client_address_string = tk.StringVar()
        self.client_address_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_address_string 
        )
        # contact
        self.client_contact_string = tk.StringVar()
        self.client_contact_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_contact_string 
        )
        # cpf
        self.client_cpf_string = tk.StringVar()
        self.client_cpf_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_cpf_string 
        )
        # rg
        self.client_rg_string = tk.StringVar()
        self.client_rg_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_rg_string 
        )
        # limit
        self.client_limit_string = tk.StringVar()
        self.client_limit_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_limit_string 
        )
        # status
        self.client_status_string = tk.StringVar()
        self.client_status_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_status_string
        )
        # comments
        self.client_comment_string = tk.StringVar()
        self.client_comment_label = ttk.Label(
            self.client_frame,
            textvariable=self.client_comment_string
        )
        self.comment_box = tk.Text(
            self.client_frame,
            height=3,
            state='normal'
        )
        self.comment_scrollbar = ttk.Scrollbar(
            self.client_frame, 
            orient="vertical", 
            command=self.comment_box.yview, 
            cursor="hand2"
        )
        self.comment_box.configure(yscrollcommand=self.comment_scrollbar.set)

        self.set_client_labels()
        # grid
        # client_frame
        self.client_frame.grid(row=0, column=0, sticky='NSEW')
        self.client_frame.columnconfigure(1, weight=1)
        # children
        grid_setup = [
            [0,0,(20,20),(20,5),'W', 2], #cod
            [1,0,(20,10),(5, 5),'W', 1], #name
            [1,1,(10,20),(5, 5),'W', 2], #address
            [2,0,(20,20),(5, 5),'W', 2], #contact
            [3,0,(20,10),(5, 5),'W', 1], #cpf
            [3,1,(10,10),(5, 5),'W', 2], #rg
            [4,0,(20,10),(5, 5),'W', 1], #limit
            [4,1,(10,20),(5, 5),'W', 2], #status
            [5,0,(20,20),(5, 5),'EW',3], #comment
            [6,0,(20, 0),(0,20),'EW',2], #comment_box
            [6,2,(0, 20),(0,20),'NSW',2] #comment_scrollbar
        ]
        index = 0
        for child in self.client_frame.winfo_children():
            child.grid(
                row=grid_setup[index][0],
                column=grid_setup[index][1],
                padx=grid_setup[index][2],
                pady=grid_setup[index][3],
                sticky=grid_setup[index][4],
                columnspan=grid_setup[index][5]
            )
            index += 1

        # sales_frame
        self.sales_frame = ttk.Frame(self)
        # months_frame
        self.months_frame = ttk.Frame(self.sales_frame)
        # pending_months
        self.pending_months_string = tk.StringVar()
        self.verify_pendings()
        self.pending_months_label = ttk.Label(
            self.months_frame,
            textvariable=self.pending_months_string,
            foreground=self.choose_color(self.pending_months_string.get())
        )
        # months
        self.months_label = ttk.Label(
            self.months_frame,
            text='MÊS:',
        )
        self.months_dict = {
            'DEZEMBRO': '12',
            'NOVEMBRO': '11',
            'OUTUBRO': '10',
            'SETEMBRO': '09',
            'AGOSTO': '08',
            'JULHO': '07',
            'JUNHO': '06',
            'MAIO': '05',
            'ABRIL': '04',
            'MARÇO': '03',
            'FEVEREIRO': '02',
            'JANEIRO': '01'
        }
        current_month = len(self.months_dict) - int(date.today().strftime("%m"))
        self.months_cbox = ttk.Combobox(
            self.months_frame,
            values=list(self.months_dict)[current_month:],
            state='readonly'
        )
        self.months_cbox.current(0)
        # grid
        # sales_frame
        self.sales_frame.grid(row=1, column=0, sticky='NSEW')

        # months_frame
        self.months_frame.grid(row=0, column=0, sticky='NSEW')
        self.months_frame.columnconfigure(1, weight=1)
        # children
        grid_setup = [
            [0,0,(20,10),(0,0), 'W',1], # pending_months
            [0,1,(20,10),(0,0), 'E',1], # months_label
            [0,2,(10,20),(0,0),'W',1] # months_cbox
        ]
        index = 0
        for child in self.months_frame.winfo_children():
            child.grid(
                row=grid_setup[index][0],
                column=grid_setup[index][1],
                padx=grid_setup[index][2],
                pady=grid_setup[index][3],
                sticky=grid_setup[index][4],
                columnspan=grid_setup[index][5]
            )
            index += 1

        # table_frame
        self.table_frame = ttk.Frame(self.sales_frame)
        ## table
        self.table = ttk.Treeview(
            self.table_frame,
            selectmode='browse',
            column=('column1', 'column2', 'column3', 'column4', 'column5'),
            show='headings'
        )
        self.id_cod = self.table.column('column1', width=100, minwidth=100, stretch=tk.NO)
        self.table.heading('#1', text='CÓDIGO')
        self.id_det = self.table.column('column2', width=230, minwidth=230, stretch=tk.NO)
        self.table.heading('#2', text='DETALHES')
        self.id_value = self.table.column('column3', width=100, minwidth=100, stretch=tk.NO)
        self.table.heading('#3', text='VALOR')
        self.id_date = self.table.column('column4', width=100, minwidth=100, stretch=tk.NO)
        self.table.heading('#4', text='DATA')
        self.id_aut = self.table.column('column5', width=120, minwidth=120, stretch=tk.NO)
        self.table.heading('#5', text='AUTENTICAÇÃO')
        self.table_scrollbar = ttk.Scrollbar(
            self.table_frame, 
            orient="vertical", 
            command=self.table.yview, 
            cursor="hand2"
        )
        self.table.configure(yscrollcommand=self.table_scrollbar.set)

        # grid
        # table_frame
        self.table_frame.grid(row=1, column=0, sticky='NSEW')
        # children
        self.table.grid(row=0, column=0, sticky='NSEW', padx=(20,0), pady=(10,20))
        self.table_scrollbar.grid(row=0, column=1, padx=(0,20), pady=(10,20), sticky="WNS")
        
        # details_frame
        self.details_frame = ttk.Frame(self.sales_frame)
        # account
        self.account_status_string = tk.StringVar()
        self.account_status = ttk.Label(
            self.details_frame,
            textvariable=self.account_status_string,
        )
        # sales
        self.sales_string = tk.StringVar()
        self.sales_label = ttk.Label(
            self.details_frame,
            textvariable=self.sales_string,
        )
        # amount
        self.amount_string = tk.StringVar()
        self.amount_label = ttk.Label(
            self.details_frame,
            textvariable=self.amount_string,
        )
        # grid
        # details_frame
        self.details_frame.grid(row=2, column=0, sticky='NSEW')
        self.details_frame.columnconfigure(0, weight=1)
        # children
        self.account_status.grid(row=0, column=0, padx=20, pady=(0,20), sticky='W')
        self.sales_label.grid(row=0, column=1, padx=20, pady=(0,20), sticky='E')
        self.amount_label.grid(row=0, column=2, padx=20, pady=(0,20), sticky='E')

        # buttons_frame
        self.buttons_frame = ttk.Frame(self)
        self.buttons_frame.rowconfigure(0, weight=1)
        self.buttons_frame.columnconfigure((0,1,2), weight=1)
        self.buttons_frame.grid(row=3, column=0, sticky='NSEW')
        # buttons
        buttons_dict = {
            'PAGAR': self.pay,
            'ALTERAR': self.update_client,
            'FECHAR': self.destroy,
        }
        self.buttons = []
        buttons_padding = [20,5,5,20]
        index = 0
        for key in buttons_dict.keys():
            value = buttons_dict[key]
            tmp_button = ttk.Button(
                self.buttons_frame, 
                text=key, 
                command=value,
                cursor='hand2'
            )
            tmp_button.grid(row=0, column=index, padx=(buttons_padding[index], buttons_padding[index+1]), pady=20, sticky='NSEW')
            tmp_button.bind("<Return>", value)
            tmp_button.bind("<KP_Enter>", value)
            self.buttons.append(tmp_button)
            index += 1

        self.set_sales_table()
        self.months_cbox.bind('<<ComboboxSelected>>', self.set_sales_table)
        self.table.bind('<Double-1>', self.see_items)
    
    def update_client(self, *args):
        window = ClientRegister(
            self.container, 
            operation='update', 
            cod=self.client_cod, 
            history_update=self.history_update
        )
        window.focus()
        self.wait_window(window)
        self.client = self.get_client_info()
        self.set_client_labels()

    def get_client_info(self, *args):
        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
        cursor.execute("""
        select * from client where cod == ? limit 1
        """,(self.client_cod,))
        
        queue = cursor.fetchall()[0]
        contact = json.loads(queue[4])
        address = json.loads(queue[5])
        client = {
            'cod':      int(queue[0]),
            'name':     str(queue[1]),
            'cpf':      str(queue[2]),
            'rg':       str(queue[3]),
            'phone_1':  str(contact["phone_1"]),
            'phone_2':  str(contact["phone_2"]),
            'street':   str(address["street"]),
            'number':   str(address["number"]),
            'district': str(address["district"]),
            'city':     str(address["city"]),
            'limit':    float(queue[6]),
            'status':   str(queue[7]),
            'comment':  str(queue[8])
        }
        conn.commit()
        conn.close()

        return client
    
    def set_client_labels(self, *args):
        # cod
        self.client_cod_string.set(f'CÓDIGO: {self.format(tp="cod", string=str(self.client["cod"]))}')
        # name
        self.client_name_string.set(f'NOME: {self.client["name"]}')
        # cpf
        self.client_cpf_string.set(f'CPF: {self.format(tp="cpf", string=self.client["cpf"])}')
        # rg
        self.client_rg_string.set(f'RG: {self.format(tp="rg", string=self.client["rg"])}')
        # contact
        self.client_contact_string.set(f'CONTATO: {self.format(tp="phone", string=self.client["phone_1"])}\t{self.format(tp="phone", string=self.client["phone_2"])}')
        # address
        self.client_address_string.set(f'ENDEREÇO: {self.client["street"]}, {self.client["number"]}, {self.client["district"]} - {self.client["city"]}')
        # limit
        self.client_limit_string.set(f'LIMITE MENSAL: {self.to_br_currency(self.client["limit"])}')
        # status
        self.client_status_string.set(f'STATUS DA CONTA: {self.client["status"]}')
        self.client_status_label.configure(foreground=self.choose_color(self.client["status"]))
        # comment
        self.client_comment_string.set('INFORMAÇÕES ADICIONAIS:')
        self.comment_box.configure(state='normal')
        self.comment_box.insert(1.0, self.client["comment"])
        self.comment_box.configure(state='disabled')

    def see_items(self, *args):
        selection = self.table.selection()[0]
        index = int(str(selection).replace('I',''))-1
        window = InfoBox(self, title='ITENS', message=self.item_string[index])

    def set_sales_table(self, *args):
        for row in self.table.get_children():
            self.table.delete(row)
        
        self.item_string = []
        if(int(self.months_dict[self.months_cbox.get()]) == int(date.today().strftime("%m"))):
            try:
                conn = sqlite3.connect('credit_sales.db')
                cursor = conn.cursor()
                # getting the details of all sales
                cursor.execute("""
                select cod, items, value, strftime("%d/%m/%Y", s_date), authentication from sale
                where client_cod = ? and  strftime("%m", s_date) = ?
                order by s_date
                """, (self.client_cod, self.months_dict[self.months_cbox.get()]))
                
                for line in cursor.fetchall():
                    cod = self.format(tp='cod', string=str(line[0]))
                    items = json.loads(line[1]).values()
                    subtotal = self.to_br_currency(line[2])
                    authentication = line[3]
                    string = []
                    for item in items:
                        string.append(f'x{item[0]} {item[1]} : {item[2]}')
                    string = '\n'.join(string)
                    self.table.insert("", index="end", values=[cod, 'CLIQUE PARA VER', subtotal, authentication])
                    self.item_string.append(string)
                conn.commit() 
                # getting the sales total
                cursor.execute("""
                select sum(value) from sale
                where client_cod = ?
                """, (self.client_cod,))
                self.amount_string.set(f'TOTAL: {self.to_br_currency(cursor.fetchall()[0][0])}')
                conn.commit()
                # getting the sales quantity 
                cursor.execute("""
                select count(cod) from sale
                where client_cod = ?
                """, (self.client_cod,))
                self.sales_string.set(f'COMPRAS: {cursor.fetchall()[0][0]}')
                conn.commit()
                conn.close()  
            except:
                pass
        else:
            pass
            # try:
            #     conn = sqlite3.connect('credit_sales.db')
            #     cursor = conn.cursor()
            #     # getting the details of all sales
            #     cursor.execute("""
            #     select items, value from expired_sale
            #     where client_cod = ? and  strftime("%m", s_date) = ?
            #     order by s_date
            #     """, (self.client_cod, self.months_dict[self.months_cbox.get()]))
                
            #     qtt = 0
            #     for line in cursor.fetchall():
            #         sales = json.loads(line[0])
            #         for key in sales.keys():
            #             cod = self.format(tp='cod', string=str(key))
            #             items = json.loads(sales['items']).values()
            #             subtotal = self.to_br_currency(line[2])
            #             authentication = line[3]
            #             string = []
            #             for item in items:
            #                 string.append(f'x{item[0]} {item[1]} : {item[2]}')
            #             string = '\n'.join(string)
            #             self.table.insert("", index="end", values=[cod, 'CLIQUE PARA VER', subtotal, authentication])
            #             self.item_string.append(string)
            #     # getting the sales total
            #     self.amount_string.set(f'Total: {self.to_br_currency(cursor.fetchall()[0][1])}')
            #     # getting the sales quantity 
            #     self.sales_string.set(f'Compras: {qtt}')
            #     conn.close()  
            # except:
            #     pass
        
    def choose_color(self, string):
        try:
            for caractere in string:
                if int(caractere) == 0:
                    return 'green'
            return 'red'
        except:
            if string == 'ABERTO':
                return 'green'
            elif string == 'SUSPENSO':
                return 'orange'
            elif string == 'FECHADO':
                return 'red'

    def verify_pendings(self, *args):
        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
        cursor.execute("""
        select count(cod) from expired_sale
        where client_cod = ? and paid = 'false'
        """, (self.client_cod,))

        if(isinstance(cursor.fetchall()[0], tuple)):
            counter = 0
        else:
            counter = int(cursor.fetchall()[0]) 
        
        conn.commit()
        conn.close() 

        string = f'{counter} MESES PENDENTES'
        if counter == 1:
            string = f'{counter} MÊS PENDENTE'
        self.pending_months_string.set(string)    

    def format(self, tp=None, string=None, *args):
        length = len(string)
        
        if string == '':
            return string

        if tp == 'cod':
            cod = string
            for i in range(0, 8-length):
                cod = '0' + cod
            return cod

        elif tp == 'cpf':
            if length != 11:
                return 'INVÁLIDO'
            else:
                return f'{string[:3]}.{string[3:6]}.{string[6:9]}-{string[9:]}'

        elif tp == 'rg':
            if length not in (8,9):
                return 'INVÁLIDO'
            elif length == 8:
                return f'{string[:2]}.{string[2:5]}.{string[5:]}'
            elif length == 9:
                return f'{string[:2]}.{string[2:5]}.{string[5:8]}-{string[8:]}'

        elif tp == 'phone':
            if length not in (8,9,10,11,12,13):
                return 'INVÁLIDO'
            else:
                phone = string[length-8:]
                ddd = '37'
                if length in (10,11):
                    ddd = string[:2]
                elif length in (12,13):
                    ddd = string[2:4]
                digit = '9 '
                if phone[0] == '3':
                    digit = ''
                return f'+55 {ddd} {digit}{phone[:4]}-{phone[4:]}'

    def pay(self, *args):
        pass

    def to_br_currency(self, value: float):
        return 'R$ ' + ('%.2f' % value).replace('.',',')
    