import sqlite3
import tkinter as tk
from tkinter import ttk
from sheet import Sheet
import io
import collections
from tkinter import messagebox
import json
from datetime import date, datetime
from authwindow import AuthWindow

class SaleRegister(tk.Toplevel):
    def __init__(self, container, cod='', name='', history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.title('INSERIR VENDA A PRAZO')
        self.resizable(False, False)
        self.rowconfigure((1,2,3), weight=1)
        self.columnconfigure(0, weight=1)

        self.authenticate = True
        self.values = None
        self.cod = cod
        self.history_update = history_update
        self.reg_digit = self.register(self.callback_digit)
        self.reg_float = self.register(self.callback_float)

        self.error_string = tk.StringVar()
        self.error_label = ttk.Label(
            self,
            textvariable=self.error_string,
            foreground='red'
        )

        # client
        self.client_frame = ttk.Frame(self)
        self.code_label = ttk.Label(
            self.client_frame,
            text=f'CÓDIGO: {cod}',
        ) 
        self.client_label = ttk.Label(
            self.client_frame,
            text=f'CLIENTE: {name}',
        )

        # product
        self.product_frame = ttk.Frame(self)
        self.qtt_label = ttk.Label(
            self.product_frame,
            text='* QUANTIDADE',
        ) 
        self.qtt_string = tk.StringVar()
        self.qtt_string.set('1')
        self.qtt_entry = ttk.Entry(
            self.product_frame,
            textvariable=self.qtt_string,
            validate="key", 
            validatecommand=(self.reg_float, '%P'),
            justify=tk.CENTER,
            width=12
        )
        self.product_label = ttk.Label(
            self.product_frame,
            text='* DESCRIÇÃO',
        ) 
        self.product_string = tk.StringVar()
        self.product_entry = ttk.Entry(
            self.product_frame,
            textvariable=self.product_string,
            width=25,
        ) 
        self.value_label = ttk.Label(
            self.product_frame,
            text='* VALOR',
        ) 
        self.value_string = tk.StringVar()
        self.value_entry = ttk.Entry(
            self.product_frame,
            textvariable=self.value_string,
            validate="key", 
            validatecommand=(self.reg_float, '%P'),
            width=10
        )

        self.history_frame = ttk.Frame(self)
        ## table
        self.table = ttk.Treeview(
            self.history_frame,
            selectmode='extended',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.id_qtt = self.table.column('column1', width=100, minwidth=100, stretch=tk.NO)
        self.table.heading('#1', text='QUANTIDADE')
        self.id_det = self.table.column('column2', width=230, minwidth=230, stretch=tk.NO)
        self.table.heading('#2', text='DESCRIÇÃO')
        self.id_subtotal = self.table.column('column3', width=100, minwidth=100, stretch=tk.NO)
        self.table.heading('#3', text='SUBTOTAL')

        self.delete_button = ttk.Button(
            self.history_frame,
            text='APAGAR SELEÇÃO [DEL]',
            cursor='hand2',
            command=self.sale_delete,
            state='disabled'
        )

        self.items = 0
        self.amount = 0.0

        self.details_frame = ttk.Frame(self)
        self.item_string = tk.StringVar()
        self.item_string.set("items: 0")
        self.item_label = ttk.Label(
            self.details_frame,
            textvariable=self.item_string,
        ) 
        self.amount_string = tk.StringVar()
        self.amount_string.set("TOTAL: R$ 0,00")
        self.amount_label = ttk.Label(
            self.details_frame,
            textvariable=self.amount_string,
        )

        # buttons
        self.buttons_frame = ttk.Frame(self)
        self.confirm_button = ttk.Button(
            self.buttons_frame,
            text='CONFIRMAR [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.cancel_button = ttk.Button(
            self.buttons_frame,
            text='CANCELAR [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.qtt_entry, True],
            [self.product_entry, True],
            [self.value_entry, True],
            [self.table, False],
            [self.confirm_button, True],
            [self.cancel_button, True]
        ]
        self.widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.error_label.grid(row=0, column=0, padx=20, pady=10)

        self.client_frame.grid(row=1, column=0, sticky='NSEW')
        self.client_frame.rowconfigure((0,1), weight=1)
        self.client_frame.columnconfigure(0, weight=1)
        self.code_label.grid(row=0, column=0, sticky='W', padx=20, pady=(0,10))
        self.client_label.grid(row=1, column=0, sticky='W', padx=20, pady=(0,10))

        self.product_frame.grid(row=2, column=0, sticky='NSEW')
        self.product_frame.rowconfigure(1, weight=1)
        self.product_frame.columnconfigure(1, weight=1)
        self.qtt_label.grid(row=0, column=0, padx=(20, 10), pady=(10, 0), sticky='W')
        self.qtt_entry.grid(row=1, column=0, padx=(20, 10), pady=(0, 10), sticky='EW')
        self.product_label.grid(row=0, column=1, padx=(10,10), pady=(10,0), sticky='W')
        self.product_entry.grid(row=1, column=1, padx=(10,10), pady=(0,10), sticky='EW')
        self.value_label.grid(row=0, column=2, padx=(10, 20), pady=(10, 0), sticky='W')
        self.value_entry.grid(row=1, column=2, padx=(10, 20), pady=(0, 10), sticky='EW')

        self.history_frame.grid(row=3, column=0, sticky='NSEW')
        self.history_frame.rowconfigure(0, weight=1)
        self.history_frame.columnconfigure(0, weight=1)
        self.table.grid(row=0, column=0, padx=20, pady=(0, 10), sticky='EW')
        self.delete_button.grid(row=1, column=0, padx=20, pady=(0,20), sticky='E')

        self.details_frame.grid(row=4, column=0, sticky='NSEW')
        self.details_frame.rowconfigure(0, weight=1)
        self.details_frame.columnconfigure(1, weight=1)
        self.item_label.grid(row=0, column=0, sticky='W', padx=(20,10), pady=(0,10))
        self.amount_label.grid(row=0, column=1, sticky='E', padx=20, pady=(0,10))

        self.buttons_frame.grid(row=5, column=0, columnspan=2, sticky='NSEW')
        self.buttons_frame.rowconfigure(1, weight=1)
        self.buttons_frame.columnconfigure((0,1), weight=1)
        self.confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.qtt_entry.bind('<Return>', lambda arg: self.focus_next(self.qtt_entry))
        self.qtt_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.qtt_entry))

        self.product_entry.bind('<Return>', lambda arg: self.focus_next(self.product_entry))
        self.product_entry.bind('<KP_Enter>', lambda arg: self.focus_next(self.product_entry))

        self.value_entry.bind('<Return>', self.sale_insert)
        self.value_entry.bind('<KP_Enter>', self.sale_insert)

        self.table.bind('<<TreeviewSelect>>', lambda arg: self.delete_button.configure(state='normal'))
        
        self.confirm_button.bind('<Return>', self.confirm)
        self.confirm_button.bind('<KP_Enter>', self.confirm)

        self.cancel_button.bind('<Return>', self.cancel)
        self.cancel_button.bind('<KP_Enter>', self.cancel)

        self.focus_next(self.cancel_button)

    def callback_digit(self, input):
        if input.isdigit():
            return True
        elif input == '':
            return True
        else:
            return False

    def to_br_currency(self, value: float):
        return 'R$ ' + ('%.2f' % value).replace('.',',')
  
    def error_check(self, *args):
        for widget in (self.product_string, self.value_string):
            if widget.get() is None or widget.get() == '':
                self.error_string.set('PREENCHA TODOS OS CAMPOS OBRIGATÓRIOS (*)')
                self.focus_next(self.cancel_button)
                return True
        
        self.error_string.set('')
        return False
    
    def isnumber(self, value):
        try:
            float(value)
        except ValueError:
            return False
        return True
   
    def callback_float(self, input): # The string must be in BRL floating format
        input = input.replace('.','').replace(',','.') 
        if input == '.': input = '0.0'
        elif input == '-': input = '-0.0'
        elif input == '+': input = '+0.0'
        if self.isnumber(input):
            return True
        elif input == '':
            return True
        return False  
    
    def to_us_float(self, string: str): # The string must be in BRL floating format
        string = string.replace(',','.') 
        if self.isnumber(string):
            return float(string)
        return False  

    def focus_next(self, previous_widget):
        while self.widget_deque[0][0] != previous_widget:
            self.widget_deque.rotate(-1)
        while not self.widget_deque[1][1]:
            self.widget_deque.rotate(-1)
       
        self.widget_deque[1][0].focus()
        if isinstance(self.widget_deque[1][0], ttk.Entry):
            self.widget_deque[1][0].select_range(0, 'end')
    
    def sale_insert(self, *args):
        if not self.error_check():
            qtt = self.to_us_float(self.qtt_string.get())
            product = self.product_string.get()
            value = self.to_us_float(self.value_string.get())

            self.items += qtt
            self.amount += qtt*value

            self.product_entry.delete(0,'end')
            self.value_entry.delete(0, 'end')

            self.table.insert("", "end", values=[qtt, product.upper(), self.to_br_currency(qtt*value)])
            self.item_string.set(f'ITENS: {self.items}')
            self.amount_string.set(f'TOTAL: {self.to_br_currency(self.amount)}')
            self.qtt_string.set('1')

            self.focus_next(self.cancel_button)
    
    def sale_delete(self, *args):  
        selection = self.table.selection()
        
        for row in selection:
            values = self.table.item(row,"values")
            self.items -= float(values[0])
            subtotal = self.to_us_float(str(values[2]).replace('R$ ',''))
            self.amount -= subtotal
            self.table.delete(row)

        self.item_string.set(f'ITENS: {self.items}')
        self.amount_string.set(f'TOTAL: {self.to_br_currency(self.amount)}')

        self.delete_button.configure(state='disabled')

    def confirm(self, *args):
        if self.items > 0:
            if self.authenticate:
                window = AuthWindow(self)
                window.focus()
                self.wait_window(window)
                
            conn = sqlite3.connect('credit_sales.db')
            cursor = conn.cursor()
            
            indexes = self.table.get_children()
            values = []
            for index in indexes:
                values.append(self.table.item(index, "values"))
            cod = int(self.cod)
            items = {str(i): values[i] for i in range(0, len(values))}
            value = self.amount
            date = datetime.now()
            cursor.execute("""
            insert into sale(client_cod, items, value, s_date) values (?,?,?,?)
            """,(cod, json.dumps(items), value, date))
            conn.commit()
            cursor.execute("""
            select name from client where cod == ? limit 1
            """,(cod,))
            client = cursor.fetchall()[0][0]
            cursor.execute("""
            select cod from sale where client_cod == ? and items == ? and value == ? and s_date == ? 
            limit 1
            """,(cod, json.dumps(items), value, date))
            cod = cursor.fetchall()[0][0]
            conn.close()

            client =  f'\n>> CLIENTE:\t{client}'
            cod = f'\n>> CÓDIGO DA VENDA:\t{self.format(tp="cod", string=str(cod))}'
            value = f'\n>> VALOR:\t{self.to_br_currency(value)}'

            log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > INSERIR VENDA A PRAZO {client}{cod}{value}\n"
            self.history_update(log_string)
            
            self.destroy()
        else:
            self.error_string.set('NÃO É POSSÍVEL ADICIONAR UMA VENDA VAZIA')
    
    def format(self, tp=None, string=None, *args):
        length = len(string)
        
        if string == '':
            return string

        if tp == 'cod':
            cod = string
            for i in range(0, 8-length):
                cod = '0' + cod
            return cod

        elif tp == 'cpf':
            if length != 11:
                return 'INVÁLIDO'
            else:
                return f'{string[:3]}.{string[3:6]}.{string[6:9]}-{string[9:]}'

        elif tp == 'rg':
            if length not in (8,9):
                return 'INVÁLIDO'
            elif length == 8:
                return f'{string[:2]}.{string[2:5]}.{string[5:]}'
            elif length == 9:
                return f'{string[:2]}.{string[2:5]}.{string[5:8]}-{string[8:]}'

        elif tp == 'phone':
            if length not in (8,9,10,11,12,13):
                return 'INVÁLIDO'
            else:
                phone = string[length-8:]
                ddd = '37'
                if length in (10,11):
                    ddd = string[:2]
                elif length in (12,13):
                    ddd = string[2:4]
                digit = '9 '
                if phone[0] == '3':
                    digit = ''
                return f'+55 {ddd} {digit}{phone[:4]}-{phone[4:]}'

    def cancel(self, *args): 
        self.destroy()