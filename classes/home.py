import tkinter as tk
from tkinter import ttk
from datetime import date, datetime
from window import Window
from flowcontroller import FlowController
from functions import to_br_currency, callback_float, is_number
from search import Search

class Home(tk.Frame):
    def __init__(self, container, log_file_path=None, tmp_file_path=None, yesterday_file_path=None, clear_tables_selection=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.__container = container
        self.__tmp_file_path = tmp_file_path
        self.__log_file_path = log_file_path
        self.__reg_float = self.register(callback_float)

        try:
            tmp_file = open(self.__tmp_file_path, 'r')
            arr = tmp_file.read().split()
            self.__sales_counter = FlowController(initial_amount=float(arr[0]), initial_quantity=int(arr[1]))
            tmp_file.close()
        except:
            self.__sales_counter = FlowController()
        yesterday = []
        try:
            yesterday_tmp_file = open(yesterday_file_path, 'r')
            yesterday = yesterday_tmp_file.read().split()
            yesterday_tmp_file.close()
            yesterday = [to_br_currency(float(yesterday[0])), int(yesterday[1])]
        except:
            yesterday = ['NÃO ENCONTRADO','NÃO ENCONTRADO']
        log_string = ''
        try:
            log_file = open(self.__log_file_path, 'r')
            for line in log_file:
                log_string += str(line)
            log_file.close()
        except:
            log_string = f"BOM DIA!\nDIA {date.today().strftime('%d/%m/%Y')}\nRESUMO DO DIA ANTERIOR:\n>> VENDAS REALIZADAS: {yesterday[1]}\n>> VALOR TOTAL: {yesterday[0]}\n\n"
            log_file = open(self.__log_file_path, 'w')
            log_file.write(log_string)

        # command frame
        self.__command_frame = ttk.Frame(self)
        ## command label
        self.__command_label = ttk.Label(
            self.__command_frame, 
            justify=tk.CENTER, 
            text='Digite um valor:'
        )
        ## command entry
        self.__command_string = tk.StringVar()
        self.__command_entry = ttk.Entry(
            self.__command_frame, 
            textvariable=self.__command_string,
            validate="key", 
            validatecommand=(self.__reg_float, '%P'),
            justify=tk.CENTER
        )
        self.__command_entry.focus()

        # amount frame
        self.__amount_frame = ttk.Frame(self)
        ## amount text
        self.__amount_text = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            text='Montante atual:'
        )
        ## amount value
        self.__amount_string = tk.StringVar()
        self.__amount_string.set(self.__sales_counter.get()[1])
        self.__amount_value = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            textvariable=self.__amount_string
        )
        ## quantity text
        self.__quantity_text = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            text='Vendas realizadas:'
        )
        ## quantity value
        self.__quantity_string = tk.StringVar()
        self.__quantity_string.set(self.__sales_counter.get()[0])
        self.__quantity_value = ttk.Label(
            self.__amount_frame, 
            justify=tk.CENTER, 
            textvariable=self.__quantity_string
        )

        # buttons_frame
        self.__buttons_frame = ttk.Frame(self)
        ## buttons
        buttons_map = {
            'Entrada [F2]': lambda: self.__container.call_window('insert', 'cash_in'),
            'Saída [F3]': lambda: self.__container.call_window('insert', 'cash_out'),
            'Caixa II [F4]': lambda: self.__container.call_window('insert', 'cashier'),
            'Cartão [F5]': lambda: self.__container.call_window('insert', 'card_sales'),
            'A prazo [F6]': lambda: Search(self, operation='new_credit_sale', history_update=lambda arg: self.history_update(arg)),
        }
        self.__buttons = []
        buttons_padding = [20,5,5,5,5,20]
        index = 0
        for key in buttons_map.keys():
            value = buttons_map[key]
            tmp_button = ttk.Button(
                self.__buttons_frame, 
                text=key, 
                command=value,
                cursor='hand2'
            )
            tmp_button.grid(row=0, column=index, padx=(buttons_padding[index], buttons_padding[index+1]), pady=20, sticky='NSEW')
            tmp_button.bind("<Return>", value)
            tmp_button.bind("<KP_Enter>", value)
            self.__buttons.append(tmp_button)
            index += 1
        
        # history frame
        self.__history_frame = ttk.Frame(self)
        ## history label
        self.__history_label = ttk.Label(
            self.__history_frame, 
            justify=tk.CENTER, 
            text='Histórico:'
        )
        ## history text
        self.__history_string = tk.StringVar()
        self.__history_box = tk.Text(
            self.__history_frame,
            state='disabled',
            highlightthickness=0,
            wrap=tk.WORD,
            width=55,
            height=15
        )
        self.__history_box.tag_configure("right", justify='right')
        self.__history_box.tag_add("right", 1.0, "end")
        self.__history_scrollbar = ttk.Scrollbar(
            self.__history_frame, 
            orient="vertical", 
            command=self.__history_box.yview, 
            cursor="hand2"
        )
        self.__history_box.configure(yscrollcommand=self.__history_scrollbar.set)
        self.__history_box.yview_moveto(1)

        # close frame
        self.__close_frame = ttk.Frame(self)
        ## close button
        self.__close_button = ttk.Button(
            self.__close_frame, 
            text='Finalizar expediente', 
            command=self.__container.close,
            cursor='hand2'
        )
        self.__history_box.configure(state='normal')
        self.__history_box.insert('1.0', log_string)
        self.__history_box.configure(state='disabled')
        self.__history_box.yview('end')

        # Grid
        ## Self
        self.rowconfigure(3, weight=1)
        self.columnconfigure(0, weight=1)
        ## Command
        self.__command_frame.rowconfigure(0, weight=1)
        self.__command_frame.columnconfigure(1, weight=1)
        self.__command_frame.grid(row=0, column=0, sticky='NSEW')
        self.__command_label.grid(row=0, column=0, padx=(20,20), pady=(20, 5), sticky='NSW')
        self.__command_entry.grid(row=0, column=1, padx=(10,20), pady=(20, 5), sticky='EW')
        ## Amount
        self.__amount_frame.rowconfigure((0,1), weight=1)
        self.__amount_frame.columnconfigure(1, weight=1)
        self.__amount_frame.grid(row=1, column=0, sticky='NSEW')
        self.__amount_text.grid(row=0, column=0, padx=(20,0), pady=5, sticky='NSW')
        self.__amount_value.grid(row=0, column=1, padx=(11,20), pady=5, sticky='NSE')
        self.__quantity_text.grid(row=1, column=0, padx=(20,0), pady=5, sticky='NSW')
        self.__quantity_value.grid(row=1, column=1, padx=(10,20), pady=5, sticky='NSE')
        ## Buttons
        self.__buttons_frame.rowconfigure(0, weight=1)
        self.__buttons_frame.columnconfigure((0,1,2,3,4), weight=1)
        self.__buttons_frame.grid(row=2, column=0, sticky='NSEW')
        ## History
        self.__history_frame.rowconfigure(1, weight=1)
        self.__history_frame.columnconfigure(0, weight=1)
        self.__history_frame.grid(row=3, column=0, sticky='NSEW')
        self.__history_label.grid(row=0, column=0, padx=20, pady=5, sticky='W')
        self.__history_box.grid(row=1, column=0, padx=(20,0), pady=(0,5), ipadx=5, ipady=5, sticky='NSEW')
        self.__history_scrollbar.grid(row=1, column=1, sticky="WNS", padx=(0,20), pady=(0,5))
        ## Close
        self.__close_frame.rowconfigure(0, weight=1)
        self.__close_frame.columnconfigure(0, weight=1)
        self.__close_frame.grid(row=4, column=0, sticky='NSEW')
        self.__close_button.grid(row=0, column=0, padx=20, pady=20, sticky='EW')

        # Binds
        ## Self
        self.bind('<FocusIn>', clear_tables_selection)
        ## Command
        self.__command_entry.bind("<Return>", self.submit)
        self.__command_entry.bind("<KP_Enter>", self.submit)
        ## Close
        self.__close_button.bind("<Return>", self.__container.close)
        self.__close_button.bind("<KP_Enter>", self.__container.close)

    def submit(self, *args):
        string = self.__sales_counter.insert(self.__command_string.get())
        
        self.__command_entry.delete(0,'end')
        
        counter, amount = self.__sales_counter.get()
        
        self.__amount_string.set(to_br_currency(amount))
        self.__quantity_string.set(counter)

        log_string = None
        if is_number(string) and string != 0.0:
            log_string = f"{datetime.now().strftime('%H:%M:%S')} >\t{to_br_currency(float(string))}\n"
            
            self.history_update(log_string)

            tmp_file = open(self.__tmp_file_path, 'w')
            tmp_file.write(f'{amount} {counter}')    
            tmp_file.close()

        self.__command_entry.focus()

    def get_sales_var(self): return self.__sales_counter.get()

    def focus_on_command_entry(self): self.__command_entry.focus()
    
    def history_update(self, log_string, *args):
        self.__history_box.configure(state='normal')
        self.__history_box.insert('end', log_string)
        self.__history_box.configure(state='disabled')
        self.__history_box.yview('end')

        log_file = open(self.__log_file_path, 'a')
        log_file.write(log_string)
        log_file.close()