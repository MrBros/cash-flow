from functions import to_us_float, to_br_currency, is_number

# This class will be the core of the project, where the amount and sales quantity will be updated and stored
class FlowController():
    def __init__(self, initial_amount=0, initial_quantity=0):
        self.__amount = initial_amount
        self.__counter = initial_quantity

    # This function will update the amount and quantity variables
    def increase(self, new_amount: float):
        if is_number(new_amount):
            self.__amount += new_amount
            if self.__amount > 0:
                if new_amount > 0:
                    self.__counter += 1
                elif new_amount < 0:
                    self.__counter -= 1
            else: 
                self.__amount = 0
            return True
        return False

    # This function is just to simplify the previous one for amount variable 
    def get(self):
        return [self.__counter, self.__amount]  
    
    # This function will be a bridge between the graphic interface and the project core 
    def insert(self, command: str):
        try: 
            command = to_us_float(command)
            self.increase(command)
            return command
        except:
            pass
