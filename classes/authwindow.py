import sqlite3
import tkinter as tk
from tkinter import ttk
from sheet import Sheet
import io
import collections
from tkinter import messagebox
import json
from datetime import date, datetime
import time

class AuthWindow(tk.Toplevel):
    def __init__(self, container, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.title("AUTENTICAÇÃO")
        self.geometry("320x130")
        self.resizable(False,False)

        self.container = container

        self.auth_string = tk.StringVar()
        self.auth_string.set('CLIQUE')
        self.auth_label = ttk.Label(
            self,
            textvariable=self.auth_string,
        ) 
        self.img_label = ttk.Label(self)

        self.wait_img = tk.PhotoImage(file=r"assets/wait.png")
        self.check_img = tk.PhotoImage(file=r"assets/check.png")
        self.error_img = tk.PhotoImage(file=r"assets/cancel.png")
        
        self.flag = True
        
        # grid
        # AuthWindow
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        #children
        self.auth_label.grid(row=0, column=0, padx=20, pady=(20,5))
        self.img_label.grid(row=1, column=0, padx=20, pady=(5,20))

        self.auth_string.set('VERIFICANDO A IDENTIDADE...')
        self.auth_label.configure(foreground=self.choose_color(self.auth_string.get()))
        self.img_label.configure(image=self.wait_img)

        self.after(2000, self.authenticate)
    
    def authenticate(self, *args):  
        self.auth_string.set("SUCESSO")
        self.auth_label.configure(foreground=self.choose_color(self.auth_string.get()))
        self.img_label.configure(image=self.check_img)
        self.after(1000, self.destroy)
    
    def choose_color(self, string):
        if string == 'VERIFICANDO A IDENTIDADE...':
            return 'black'
        elif string == 'SUCESSO':
            return 'green'
        elif string == 'FALHA':
            return 'red'
