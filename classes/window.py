import collections
import tkinter as tk
from tkinter import ttk
from sheet import Sheet
from datetime import date, datetime
from functions import callback_float, focus_next, to_br_currency, to_us_float


class Window(tk.Toplevel):
    def __init__(self, container=None, operation=None, printable_operation=None, wintype=None, printable_wintype=None, description=True, payment_method=True, value=True, values=['','DINHEIRO',''], *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.resizable(False, False)
        self.title(f'{printable_operation} {printable_wintype}')
        self.rowconfigure(4, weight=1)
        self.columnconfigure(0, weight=1)

        self.values = values
        self.description = description
        self.payment = payment_method
        self.value = value
        self.reg_float = self.register(callback_float)
        self.__operation = operation
        self.__wintype = wintype

        # Error
        self.error_frame = ttk.Frame(self)
        self.error_string = tk.StringVar()
        self.error_label = ttk.Label(
            self.error_frame,
            textvar=self.error_string,
            foreground='red'
        )
        # Description
        self.description_frame = ttk.Frame(self)
        self.description_label = ttk.Label(
            self.description_frame, 
            text='* Descrição'
        )
        self.description_string = tk.StringVar()
        self.description_string.set(self.values[0])
        self.description_entry = ttk.Entry(
            self.description_frame,
            textvar=self.description_string,
            width=50
        )
        # Payment
        self.payment_frame = ttk.Frame(self)
        self.payment_label = ttk.Label(
            self.payment_frame, 
            text='* Forma de pagamento'
        )
        self.payment_array = [
            'DINHEIRO', 
            'CRÉDITO', 
            'DÉBITO', 
            'CHEQUE', 
            'VALE ALIMENTAÇÃO', 
            'SICOOB', 
            'IB CAIXA', 
            'OUTROS'
        ]
        if wintype == 'card_sales':
            values[1] = 'CRÉDITO'
            self.payment_array = [
                'CRÉDITO', 
                'DÉBITO'
            ]
        self.payment_cbox = ttk.Combobox(
            self.payment_frame,
            values=self.payment_array,
            state='readonly'
        )
        self.payment_cbox.set(values[1])
        # Value   
        self.value_frame = ttk.Frame(self)         
        self.value_label = ttk.Label(
            self.value_frame, 
            text='* Valor'
        )
        self.value_string = tk.StringVar()
        self.value_string.set(self.values[2])
        self.value_entry = ttk.Entry(
            self.value_frame,
            textvariable=self.value_string,
            validate="key", 
            validatecommand=(self.reg_float, '%P'),
            width=50
        )
        # Buttons
        self.buttons_frame = ttk.Frame(self)
        self.confirm_button = ttk.Button(
            self.buttons_frame,
            text='OK',
            cursor='hand2',
            command=self.confirm
        )
        self.cancel_button = ttk.Button(
            self.buttons_frame,
            text='Cancelar',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.description_entry, self.description],
            [self.payment_cbox, self.payment],
            [self.value_entry, self.value],
            [self.confirm_button, True],
            [self.cancel_button, True]
        ]
        self.widget_deque = collections.deque(widget_sequence)

        # Grid 
        if description:
            self.description_frame.grid(row=1, column=0, sticky='NSEW')
            self.description_frame.rowconfigure((0,1), weight=1)
            self.description_frame.columnconfigure(0, weight=1)
            self.description_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,0))
            self.description_entry.grid(row=1, column=0, sticky='EW', padx=20, pady=(0,5))
        if payment_method:
            self.payment_frame.grid(row=2, column=0, sticky='NSEW')
            self.payment_frame.rowconfigure(0, weight=1)
            self.payment_frame.columnconfigure(1, weight=1)
            self.payment_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,0), ipady=5)
            self.payment_cbox.grid(row=0, column=1, sticky='EW', padx=(0,20), pady=(20,0))
        if value:
            self.value_frame.grid(row=3, column=0, sticky='NSEW')
            self.value_frame.rowconfigure(0, weight=1)
            self.value_frame.columnconfigure(1, weight=1)
            self.value_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,0))
            self.value_entry.grid(row=1, column=0, sticky='EW', padx=20, pady=(0,5))
        self.buttons_frame.grid(row=4, column=0, sticky='NSEW')
        self.buttons_frame.rowconfigure(0, weight=1)
        self.buttons_frame.columnconfigure((0,1), weight=1)
        self.confirm_button.grid(row=0, column=0, sticky='NSEW', padx=(20,10), pady=20)
        self.cancel_button.grid(row=0, column=1, sticky='NSEW', padx=(10,20), pady=20)

        focus_next(previous_widget=self.cancel_button, widget_deque=self.widget_deque)
        
        # Binds
        self.bind('<Escape>', self.cancel)
        self.description_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.description_entry, widget_deque=self.widget_deque))
        self.description_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.description_entry, widget_deque=self.widget_deque))
        self.payment_cbox.bind('<Return>', lambda arg: focus_next(previous_widget=self.payment_cbox, widget_deque=self.widget_deque))
        self.payment_cbox.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.payment_cbox, widget_deque=self.widget_deque))
        self.value_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.value_entry, widget_deque=self.widget_deque))
        self.value_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.value_entry, widget_deque=self.widget_deque))
        self.confirm_button.bind('<Return>', self.confirm)
        self.confirm_button.bind('<KP_Enter>', self.confirm)
        self.cancel_button.bind('<Return>', self.cancel)
        self.cancel_button.bind('<KP_Enter>', self.cancel)

        self.focus()
        container.wait_window(self)

    def error_check(self, *args):
        widget_dict = {
            'description': [self.description_entry, self.description_string, self.description],
            'payment': [self.payment_cbox, self.payment_cbox, self.payment],
            'value': [self.value_entry, self.value_string, self.value]
        }

        for key in widget_dict.keys():
            if widget_dict[key][1].get() in (None, '') and widget_dict[key][2]:
                self.error_string.set('Preencha todos os campos obrigatórios (*)')
                self.error_frame.grid(row=0, column=0, sticky='NSEW')
                self.error_frame.rowconfigure(0, weight=1)
                self.error_frame.columnconfigure(0, weight=1)
                self.error_label.grid(row=0, column=0, sticky='NS', padx=20, pady=(20,0))

                widget_dict[key][0].focus()
                return True
        
        self.error_string.set('')
        self.error_frame.grid_remove()
        return False    
        
    def confirm(self, *args):
        if not self.error_check():
            self.values = [self.description_entry.get().upper(), self.payment_cbox.get().upper(), to_us_float(self.value_entry.get())]
            self.destroy()

    def cancel(self, *args):
        self.values = [None, None, None]
        self.destroy()
