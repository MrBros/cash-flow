# OK
class Search(tk.Toplevel):
    def __init__(self, container, operation=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.title('Buscar Cliente')
        self.resizable(False, False)
        self.rowconfigure((0,1), weight=1)
        self.columnconfigure(0, weight=1)
        self.grab_set()
        self.focus()

        self.__container = container
        self.__history_update = history_update
        self.__operation = operation
        self.__costumer_id = int()
        self.__db = Database()

        # Search
        self.__search_frame = ttk.Frame(self)
        self.__search_label = ttk.Label(
            self.__search_frame,
            text='Digite o nome ou o código do cliente',
        ) 
        self.__search_string = tk.StringVar()
        self.__search_entry = ttk.Entry(
            self.__search_frame,
            textvariable=self.__search_string
        )
        self.__search_lbox = tk.Listbox(
            self.__search_frame,
            selectmode='browse',
            width=50,
        )
        self.__costumer_counter_string = tk.StringVar()
        self.__costumer_counter_label = ttk.Label(
            self.__search_frame,
            textvariable=self.__costumer_counter_string
        )

        # buttons
        self.__buttons_frame = ttk.Frame(self)
        self.__confirm_button = ttk.Button(
            self.__buttons_frame,
            text='OK [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.__cancel_button = ttk.Button(
            self.__buttons_frame,
            text='Cancelar [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.__search_entry, True],
            [self.__search_lbox, True],
            [self.__confirm_button, True],
            [self.__cancel_button, True]
        ]
        self.__widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.__search_frame.grid(row=0, column=0, sticky='NSEW')
        self.__search_frame.rowconfigure(2, weight=1)
        self.__search_label.grid(row=0, column=0, padx=20, pady=(20,0), sticky='W')
        self.__search_entry.grid(row=1, column=0, padx=20, pady=5, sticky='NSEW')
        self.__search_lbox.grid(row=2, column=0, padx=20, pady=(0,0), sticky='EW')
        self.__costumer_counter_label.grid(row=3, column=0, padx=20, pady=(5,20), sticky='W')

        self.__buttons_frame.grid(row=1, column=0, columnspan=2, sticky='NSEW')
        self.__buttons_frame.rowconfigure(1, weight=1)
        self.__buttons_frame.columnconfigure((0,1), weight=1)
        self.__confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.__cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.__search_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.__search_entry, widget_deque=self.__widget_deque))
        self.__search_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__search_entry, widget_deque=self.__widget_deque))
        self.__search_entry.bind('<KeyRelease>', self.search)

        self.__search_lbox.bind('<Return>', lambda arg: focus_next(previous_widget=self.__search_lbox, widget_deque=self.__widget_deque))
        self.__search_lbox.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__search_lbox, widget_deque=self.__widget_deque))

        self.__confirm_button.bind('<Return>', self.confirm)
        self.__confirm_button.bind('<KP_Enter>', self.confirm)

        self.__cancel_button.bind('<Return>', self.cancel)
        self.__cancel_button.bind('<KP_Enter>', self.cancel)

        focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
        self.search()
    
    def search(self, *args):
        costumers = None
        
        if self.__search_string.get().upper() == '': 
            costumers = self.__db.search_costumer(all=True)
        
        else:
            try:
                word = int(self.__search_string.get().upper())
                costumers = self.__db.search_costumer(id=word)
            except:
                word = self.__search_string.get().upper()+'%'
                costumers = self.__db.search_costumer(name=word)
        
        self.__search_lbox.delete(0,'end')
        if costumers:
            for costumer in costumers:
                formated_id = format(tp='id', string=str(costumer[0]))
                name = costumer[1]
                self.__search_lbox.insert("end", f'{formated_id} {name}')
        
        lenght = len(costumers)
        string = ""
        if lenght == 0: string = "nenhum cliente encontrado"
        elif lenght == 1: string = "1 cliente encontrado"
        else: string = f"{lenght} clientes encontrados"
        self.__costumer_counter_string.set(string)
        
    def confirm(self, *args):
        selection = self.__search_lbox.curselection()
        self.__costumer_id = int(self.__search_lbox.get(selection)[:8])
        name = self.__search_lbox.get(selection)[9:]
        
        if self.__operation == 'update':
            ClientRegister(
                self.__container, 
                operation=self.__operation, 
                costumer_id=self.__costumer_id, 
                history_update=self.__history_update
            )

        elif self.__operation == 'new_credit_sale':
            SaleRegister(
                self.__container,
                costumer_id=self.__costumer_id,
                name=name,
                history_update=self.__history_update
            )

        elif self.__operation == 'show_client':
            ClientWindow(
                self.__container,
                costumer_id=self.__costumer_id,
                history_update=self.__history_update
            )

        elif self.__operation == 'remove':
            self.__db.delete_costumer(id=self.__costumer_id)

            formated_id = format(tp="id", string=str(self.__costumer_id))
            now = datetime.now().strftime('%H:%M:%S')
            log_string = f"\n{now} > REMOVER CLIENTE\
                           \n>> CÓDIGO DO CLIENTE:\t{formated_id}\
                           \n>> NOME:\t{name.upper()}\n"
            self.__history_update(log_string)
         
        self.__db.close_connection()
        self.destroy()

    def cancel(self, *args): 
        self.__db.close_connection()
        self.destroy()
# OK
class SaleRegister(tk.Toplevel):
    def __init__(self, container, costumer_id=None, name=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.title('Inserir Venda a Prazo')
        self.resizable(False, False)
        self.rowconfigure((1,2,3), weight=1)
        self.columnconfigure(0, weight=1)
        self.grab_set()
        self.focus()

        self.__authenticate = True
        self.__order = None
        self.__costumer_id = int(costumer_id)
        self.__costumer_name = name
        self.__history_update = history_update
        self.__reg_digit = self.register(callback_digit)
        self.__reg_float = self.register(callback_float)
        self.__db = Database()

        self.__error_string = tk.StringVar()
        self.__error_label = ttk.Label(
            self,
            textvariable=self.__error_string,
            foreground='red'
        )

        # costumer
        self.__costumer_frame = ttk.Frame(self)
        self.__id_label = ttk.Label(
            self.__costumer_frame,
            text=f'Código: {format(tp=id, string=str(self.__costumer_id))}',
        ) 
        self.__name_label = ttk.Label(
            self.__costumer_frame,
            text=f'Cliente: {self.__costumer_name}',
        )

        # order
        self.__order_frame = ttk.Frame(self)
        self.__qtt_label = ttk.Label(
            self.__order_frame,
            text='* Quantidade',
        ) 
        self.__qtt_string = tk.StringVar()
        self.__qtt_string.set('1')
        self.__qtt_entry = ttk.Entry(
            self.__order_frame,
            textvariable=self.__qtt_string,
            validate="key", 
            validatecommand=(self.__reg_float, '%P'),
            justify=tk.CENTER,
            width=12
        )
        self.__description_label = ttk.Label(
            self.__order_frame,
            text='* Descrição',
        ) 
        self.__description_string = tk.StringVar()
        self.__description_entry = ttk.Entry(
            self.__order_frame,
            textvariable=self.__description_string,
            width=25,
        ) 
        self.__value_label = ttk.Label(
            self.__order_frame,
            text='* Valor',
        ) 
        self.__value_string = tk.StringVar()
        self.__value_entry = ttk.Entry(
            self.__order_frame,
            textvariable=self.__value_string,
            validate="key", 
            validatecommand=(self.__reg_float, '%P'),
            width=10
        )

        # table
        self.__table_frame = ttk.Frame(self)
        self.__table = ttk.Treeview(
            self.__table_frame,
            selectmode='extended',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.__delete_button = ttk.Button(
            self.__table_frame,
            text='Remover [DEL]',
            cursor='hand2',
            command=self.delete,
            state='disabled'
        )

        # details
        self.__details_frame = ttk.Frame(self)
        self.__item_string = tk.StringVar()
        self.__item_string.set("Itens: 0,0")
        self.__item_label = ttk.Label(
            self.__details_frame,
            textvariable=self.__item_string,
        ) 
        self.__amount_string = tk.StringVar()
        self.__amount_string.set("Total: R$ 0,00")
        self.__amount_label = ttk.Label(
            self.__details_frame,
            textvariable=self.__amount_string,
        )
        self.__items = 0
        self.__amount = 0.0

        # buttons
        self.__buttons_frame = ttk.Frame(self)
        self.__confirm_button = ttk.Button(
            self.__buttons_frame,
            text='OK [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.__cancel_button = ttk.Button(
            self.__buttons_frame,
            text='Cancelar [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.__qtt_entry, True],
            [self.__description_entry, True],
            [self.__value_entry, True],
            [self.__table, False],
            [self.__confirm_button, True],
            [self.__cancel_button, True]
        ]
        self.__widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.__costumer_frame.grid(row=1, column=0, sticky='NSEW')
        self.__costumer_frame.rowconfigure((0,1), weight=1)
        self.__costumer_frame.columnconfigure(0, weight=1)
        self.__id_label.grid(row=0, column=0, sticky='W', padx=20, pady=(0,10))
        self.__name_label.grid(row=1, column=0, sticky='W', padx=20, pady=(0,10))

        self.__order_frame.grid(row=2, column=0, sticky='NSEW')
        self.__order_frame.rowconfigure(1, weight=1)
        self.__order_frame.columnconfigure(1, weight=1)
        self.__qtt_label.grid(row=0, column=0, padx=(20, 10), pady=(10, 0), sticky='W')
        self.__qtt_entry.grid(row=1, column=0, padx=(20, 10), pady=(0, 10), sticky='EW')
        self.__description_label.grid(row=0, column=1, padx=(10,10), pady=(10,0), sticky='W')
        self.__description_entry.grid(row=1, column=1, padx=(10,10), pady=(0,10), sticky='EW')
        self.__value_label.grid(row=0, column=2, padx=(10, 20), pady=(10, 0), sticky='W')
        self.__value_entry.grid(row=1, column=2, padx=(10, 20), pady=(0, 10), sticky='EW')

        self.__table_frame.grid(row=3, column=0, sticky='NSEW')
        self.__table_frame.rowconfigure(0, weight=1)
        self.__table_frame.columnconfigure(0, weight=1)
        self.__table.grid(row=0, column=0, padx=20, pady=(0, 10), sticky='EW')
        self.__delete_button.grid(row=1, column=0, padx=20, pady=(0,20), sticky='E')

        self.__details_frame.grid(row=4, column=0, sticky='NSEW')
        self.__details_frame.rowconfigure(0, weight=1)
        self.__details_frame.columnconfigure(1, weight=1)
        self.__item_label.grid(row=0, column=0, sticky='W', padx=(20,10), pady=(0,10))
        self.__amount_label.grid(row=0, column=1, sticky='E', padx=20, pady=(0,10))

        self.__buttons_frame.grid(row=5, column=0, columnspan=2, sticky='NSEW')
        self.__buttons_frame.rowconfigure(1, weight=1)
        self.__buttons_frame.columnconfigure((0,1), weight=1)
        self.__confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.__cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.__qtt_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.__qtt_entry, widget_deque=self.__widget_deque))
        self.__qtt_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__qtt_entry, widget_deque=self.__widget_deque))

        self.__description_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.__description_entry, widget_deque=self.__widget_deque))
        self.__description_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__description_entry, widget_deque=self.__widget_deque))

        self.__value_entry.bind('<Return>', self.insert)
        self.__value_entry.bind('<KP_Enter>', self.insert)

        self.__table.bind('<<TreeviewSelect>>', lambda arg: update_button_state(table=self.__table, button=self.__delete_button))
        
        self.__delete_button.bind('<Return>', self.delete)
        self.__delete_button.bind('<KP_Enter>', self.delete)
        
        self.__confirm_button.bind('<Return>', self.confirm)
        self.__confirm_button.bind('<KP_Enter>', self.confirm)

        self.__cancel_button.bind('<Return>', self.cancel)
        self.__cancel_button.bind('<KP_Enter>', self.cancel)

        focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
        
        update_table_dimensions(
            master=self,
            table=self.__table,
            colname=["Quantidade", "Descrição", "Subtotal"],
            mincolwidth=[0.25,0.5,0.25],
            colwidth=[0.25,0.5,0.25]
        )
  
    def error_check(self, *args):
        for widget in (self.__description_string, self.__value_string):
            if widget.get() in (None,''):
                self.__error_string.set('Preencha todos os campos obrigatórios (*)')
                self.__error_label.grid(row=0, column=0, padx=20, pady=10)
                focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
                return True
        
        self.__error_string.set('')
        self.__error_label.grid_remove()
        return False
    
    def insert(self, *args):
        if not self.error_check():
            qtt = to_us_float(self.__qtt_string.get())
            description = self.__description_string.get()
            value = to_us_float(self.__value_string.get())

            self.__items += qtt
            self.__amount += qtt * value

            self.__description_entry.delete(0,'end')
            self.__value_entry.delete(0, 'end')

            self.__table.insert("", "end", values=[qtt, description.upper(), to_br_currency(qtt*value)])
            self.__item_string.set(f'Itens: {to_br_float(self.__items)}')
            self.__amount_string.set(f'Total: {to_br_currency(self.__amount)}')
            self.__qtt_string.set('1')

            focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
    
    def delete(self, *args):  
        selection = self.__table.selection()
        
        for row in selection:
            values = self.__table.item(row,"values")
            self.__items -= float(values[0])
            subtotal = to_us_float(str(values[2]).replace('R$ ',''))
            self.__amount -= subtotal
            self.__table.delete(row)

        self.__item_string.set(f'Itens: {to_br_float(self.__items)}')
        self.__amount_string.set(f'Total: {to_br_currency(self.__amount)}')

        self.__delete_button.configure(state='disabled')
        focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)

    def confirm(self, *args):
        if self.__items > 0:
            auth_id = 1
            # if messagebox.askyesno(title='Autenticação', message='Deseja autenticar a venda?'):
            #     window = AuthWindow(self)
            #     self.wait_window(window)
            #     # auth_key = window.get_auth_key()

            indexes = self.__table.get_children()
            values = []
            for index in indexes:
                values.append(self.__table.item(index, "values"))
            items = {str(i): values[i] for i in range(0, len(values))}
            
            total = self.__amount
            order_date = datetime.now()
            
            order_id = self.__db.insert_order(value=(auth_id, json.dumps(items), total, order_date))
            
            costumer =  self.__costumer_name
            order_id = format(tp="id", string=str(order_id))
            total = to_br_currency(total)

            log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > INSERIR VENDA A PRAZO\
                           \n>> CLIENTE:\t{costumer}\
                           \n>> CÓDIGO DA VENDA:\t{order_id}\
                           \n>> VALOR:\t{total}\n"
            self.__history_update(log_string)
            
            self.__db.close_connection()
            self.destroy()
        else:
            self.__error_string.set('Não é possível inserir uma venda vazia')

    def cancel(self, *args):
        self.__db.close_connection() 
        self.destroy()
# OK
class InfoBox(tk.Toplevel):
    def __init__(self, container, title=None, message=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.title(title)
        self.rowconfigure(0, weight=1)
        self.focus()
        
        self.__info_label = ttk.Label(
            self,
            text=message,
        )
        self.__ok_button = ttk.Button(
            self,
            cursor='hand2',
            text='OK',
            command=self.destroy
        )
        self.__ok_button.focus()

        self.__info_label.grid(row=0, column=0, padx=20, pady=20, sticky='NS')
        self.__ok_button.grid(row=1, column=0, padx=20, pady=(0,20), sticky='NS')

        self.bind('<Escape>', lambda arg: self.destroy())
        self.__ok_button.bind('<KP_Enter>', lambda arg: self.destroy())
        self.__ok_button.bind('<Return>', lambda arg: self.destroy())
# OK
class Database():
    def __init__(self, *args, **kwargs):
        self.__connection = None
        self.__cursor = None

        self.init_database()

    def __backup(self):
        with io.open(r'database/sicflow_dump.sql', 'w') as f:
            for linha in self.__connection.iterdump():
                f.write('%s\n' % linha)

        self.__connection.commit()

    def init_database(self):
        print("conectanto ao banco de dados 'sicflow_creditsales.db'...")
        try:
            # f = open(r'database/sicflow_creditsales.db', 'r')
            # f.close()
            self.__connection = sqlite3.connect(r'database/sicflow_creditsales.db')
            self.__cursor = self.__connection.cursor()
            print('sucesso!')
            return True       
        except FileNotFoundError:
            print("falha: arquivo não encontrado!")
        except IOError:
            print("falha: arquivo corrompido!")

        print("resturando o backup de 'sicflow_creditsales.db'...")
        try:
            self.__restore_backup()   
            print('sucesso!')
            return True
        except IOError as e:
            print("falha: arquivo não encontrado ou corrompido!")

        print("criando o banco de dados 'sicflow_creditsales.db'...")
        try:
            self.__create_database()
            print('sucesso!')
            return True       
        except:
            print("falha: não foi possível criar o banco de dados!")

        return False

    def finalize_database(self): 
        try:
            self.__connection.commit()
            self.__backup()
            self.__connection.close()
            return True
        except:
            print('falha: não foi possível fechar o banco de dados!')
        return False
    
    def fill(self):
        f = io.open(r'database/fill_db.sql', 'r')
        sql = f.read()
        
        self.__cursor.executescript(sql)
        self.__connection.commit()

    def close_connection(self):
        self.__connection.commit()
        self.__connection.close()

    def get_costumer_info(self, costumer_id=None, *args):
        self.__cursor.execute("""
        select * from costumer where id == ?
        """,(costumer_id,))
        
        queue = self.__cursor.fetchall()
        costumer = None
        
        if queue:
            queue = queue[0]
            contact = json.loads(queue[3])
            address = json.loads(queue[4])
            costumer = {
                'id'      : int(queue[0]),
                'name'    : str(queue[1]),
                'cpf'     : str(queue[2]),
                'phone_1' : str(contact["phone_1"]),
                'phone_2' : str(contact["phone_2"]),
                'street'  : str(address["street"]),
                'number'  : str(address["number"]),
                'district': str(address["district"]),
                'city'    : str(address["city"]),
                'limit'   : float(queue[5]),
                'status'  : str(queue[6]),
            }
        else:
            costumer = {
            'id'      : costumer_id,
            'name'    : '',
            'cpf'     : '',
            'phone_1' : '',
            'phone_2' : '',
            'street'  : '',
            'number'  : '',
            'district': 'AEROPORTO',
            'city'    : 'BOM DESPACHO',
            'limit'   : '100,00',
            'status'  : 'ABERTO'
        }

        self.__connection.commit()

        return costumer
        
    def get_auth_info(self, costumer_id=None, *args):
        self.__cursor.execute("""
        select * from authentication where costumer_id == ?
        """,(costumer_id,))
        
        queue = self.__cursor.fetchall()
        authentication = None
        
        if queue:
            authentication = dict()
            for q in queue:
                authentication[int(q[0])] = {
                    'costumer_id': int(q[1]),
                    'name'       : str(q[2]),
                    'auth_key'   : str(q[3])
                }
        self.__connection.commit()

        return authentication
    
    def get_order_info(self, auth_name=None, costumer_id=None, month=None, *args):
        if auth_name is not None:
            self.__cursor.execute("""
            select * from order_, authentication 
            where 
                authentication.id = order.auth_id 
                and authentication.name = ? 
                and strftime('%m', order_date) = ?
            order by order_date desc
            """,(auth_name, month))

        elif costumer_id is not None:
            self.__cursor.execute("""
            select * from order_, authentication 
            where 
                authentication.id = order.auth_id 
                and authentication.costumer_id = ? 
                and strftime('%m', order_date) = ?
            order by order_date desc
            """,(costumer_id, month))

        queue = self.__cursor.fetchall()
        order = None
        
        if queue:
            order = dict()
            for q in queue:
                items = json.loads(q[2])
                order[int(q[0])] = {
                    'auth_id': int(q[1]),
                    'items'  : items,
                    'total'  : float(q[3]),
                    'date'   : date(q[4]).strftime("%d/%m/%Y")
                }
        self.__connection.commit()

        return order
    
    def __restore_backup(self):
        self.__connection = sqlite3.connect(r'database/sicflow_creditsales.db')
        self.__cursor = self.__connection.cursor()
        f = io.open(r'database/sicflow_dump.sql', 'r')
        sql = f.read()
        
        self.__cursor.executescript(sql)
        self.__connection.commit()

    def __create_database(self):
        self.__connection = sqlite3.connect(r'database/sicflow_creditsales.db')
        self.__cursor = self.__connection.cursor()
        f = io.open(r'database/create_database.sql', 'r')
        sql = f.read()
        
        self.__cursor.executescript(sql)
        self.__connection.commit()
    
    def insert_costumer(self, value=None):
        try:
            self.__cursor.execute("""
            insert into costumer(name, cpf, contact, address, account_limit, account_status)
            values (?,?,?,?,?,?)
            """, value)
            self.__connection.commit()
        except: return False
        
        return True
    
    def search_costumer(self, id=0, name="0", all=False):
        if all:
            self.__cursor.execute("""
            select id, name from costumer 
            order by name 
            """)
            return self.__cursor.fetchall()

        self.__cursor.execute("""
        select id, name from costumer 
        where id = ? or name like ? 
        order by name 
        """,(id,name))
        return self.__cursor.fetchall()

    def delete_costumer(self, id=None):
        self.__cursor.execute("""
        delete from costumer, authentication, order_, payment
        where 
            payment.order_id  = order_.id and
            order_.auth_id    = authentication.id and
            authentication.costumer_id = costumer.id and
            costumer.id = ?
        """,(id,))
    
    def insert_order(self, value=None):
        try:
            self.__cursor.execute("""
            insert into order_(auth_id, items, total, order_date)
            values (?,?,?,?)
            """, value)
            self.__cursor.execute("""
            select id from order_
            where
                auth_id = ? and
                items = ? and
                total = ? and
                order_date = ? 
            limit 1
            """, value)
            auth_id = self.__cursor.fetchall()[0][0]
            self.__connection.commit()
        except: 
            return None
        
        return auth_id

    def close_client_sales(self, client_cod):
        self.connection = sqlite3.connect('credit_sales.db')
        self.cursor = self.connection.cursor()
        
        items = dict()
        value = 0.0
        expiration_date = datetime.now()
        paid = False

        self.cursor.execute("""
        select * from sale
        where client_cod = ?
        order by client_cod
        """,(client_cod,))
        fetched = self.cursor.fetchall()

        if len(fetched) > 0:
            for line in self.cursor.fetchall():
                items.update({
                    'cod': line[0],
                    'client_cod': line[1],
                    'items': line[2],
                    'value': line[3],
                    's_date': line[4],
                    'authentication': line[5]
                })
                print(line[3])
                value += line[3]
            
            self.cursor.execute("""
            insert into expired_sale(client_cod, items, value, expiration_date, paid) 
            values (?,?,?,?,?)
            """,(client_cod, json.dumps(items), value, expiration_date, paid))
        
        self.cursor.execute("""
        delete from sale where client_cod == ?
        """,(client_cod,))
        
        self.connection.commit()
        self.connection.close()

    def close_month_sales(self):
        if int(datetime.now().strftime('%d')) == 1:
            self.connection = sqlite3.connect('credit_sales.db')
            self.cursor = self.connection.cursor()
            
            self.cursor.execute("""
            select client_cod from sale
            group by client_cod
            """)
            clients = self.cursor.fetchall()

            self.connection.commit()
            self.connection.close()

            for client in clients:
                self.close_client_sales(client[0])

class ClientWindow(tk.Toplevel):
    def __init__(self, container, costumer_id=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.title('Ver Cliente')
        self.resizable(False, False)
        self.grab_set()
        self.focus()
        
        # grid
        # ClientWindow
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)

        self.__db = Database()
        self.__costumer_id = int(costumer_id)
        self.__container = container
        self.__costumer = self.__db.get_costumer_info(self.__costumer_id)
        self.__items = []
        self.__history_update = history_update

        # costumer_frame
        self.__costumer_frame = ttk.Frame(self)
        # cod
        self.__costumer_id_string = tk.StringVar()
        self.__costumer_id_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_id_string 
        )
        # name
        self.__costumer_name_string = tk.StringVar()
        self.__costumer_name_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_name_string 
        )
        # address
        self.__costumer_address_string = tk.StringVar()
        self.__costumer_address_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_address_string 
        )
        # contact
        self.__costumer_contact_string = tk.StringVar()
        self.__costumer_contact_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_contact_string 
        )
        # cpf
        self.__costumer_cpf_string = tk.StringVar()
        self.__costumer_cpf_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_cpf_string 
        )
        # limit
        self.__costumer_limit_string = tk.StringVar()
        self.__costumer_limit_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_limit_string 
        )
        # status
        self.__costumer_status_string = tk.StringVar()
        self.__costumer_status_label = ttk.Label(
            self.__costumer_frame,
            textvariable=self.__costumer_status_string
        )
        # authorized people
        self.__auth_label = ttk.Label(
            self.__costumer_frame,
            text='Pessoas autorizadas',
        ) 
        self.__auth_table = ttk.Treeview(
            self.__costumer_frame,
            selectmode='extended',
            column=('column1', 'column2'),
            show='headings',
            height=3
        )
        self.__auth_table_scrollbar = ttk.Scrollbar(
            self.__costumer_frame, 
            orient="vertical", 
            command=self.__auth_table.yview, 
            cursor="hand2"
        )
        self.__auth_table.configure(yscrollcommand=self.__auth_table_scrollbar.set)
        
        # grid
        # costumer_frame
        self.__costumer_frame.grid(row=0, column=0, sticky='NSEW')
        self.__costumer_frame.columnconfigure(1, weight=1)
        # children
        grid_setup = [
            [0,0,(20,20),(20,5),'W', 2], #cod
            [1,0,(20,10),(5, 5),'W', 1], #name
            [1,1,(10,20),(5, 5),'W', 2], #address
            [2,0,(20,20),(5, 5),'W', 2], #contact
            [3,0,(20,10),(5, 5),'W', 1], #cpf
            [3,1,(10,10),(5, 5),'W', 2], #rg
            [4,0,(20,10),(5, 5),'W', 1], #limit
            [4,1,(10,20),(5, 5),'W', 2], #status
            [5,0,(20,20),(5, 5),'EW',3], #comment
            [6,0,(20, 0),(0,20),'EW',2], #comment_box
            [6,2,(0, 20),(0,20),'NSW',2] #comment_scrollbar
        ]
        index = 0
        for child in self.__costumer_frame.winfo_children():
            child.grid(
                row=grid_setup[index][0],
                column=grid_setup[index][1],
                padx=grid_setup[index][2],
                pady=grid_setup[index][3],
                sticky=grid_setup[index][4],
                columnspan=grid_setup[index][5]
            )
            index += 1

        # sales_frame
        self.__sales_frame = ttk.Frame(self)
        # months_frame
        self.__months_frame = ttk.Frame(self.__sales_frame)
        # pending_months
        self.__pending_months_string = tk.StringVar()
        self.__pending_months_label = ttk.Label(
            self.__months_frame,
            textvariable=self.__pending_months_string,
            foreground=self.choose_color(self.__pending_months_string.get())
        )
        self.verify_pendings()
        # months
        self.__months_label = ttk.Label(
            self.__months_frame,
            text='Mês:',
        )
        self.__months_dict = {
            'DEZEMBRO': '12',
            'NOVEMBRO': '11',
            'OUTUBRO': '10',
            'SETEMBRO': '09',
            'AGOSTO': '08',
            'JULHO': '07',
            'JUNHO': '06',
            'MAIO': '05',
            'ABRIL': '04',
            'MARÇO': '03',
            'FEVEREIRO': '02',
            'JANEIRO': '01'
        }
        current_month = len(self.__months_dict) - int(date.today().strftime("%m"))
        self.__months_cbox = ttk.Combobox(
            self.__months_frame,
            values=list(self.__months_dict)[current_month:],
            state='readonly'
        )
        self.__months_cbox.current(0)
        # grid
        # sales_frame
        self.__sales_frame.grid(row=1, column=0, sticky='NSEW')

        # months_frame
        self.__months_frame.grid(row=0, column=0, sticky='NSEW')
        self.__months_frame.columnconfigure(1, weight=1)
        # children
        grid_setup = [
            [0,0,(20,10),(0,0), 'W',1], # pending_months
            [0,1,(20,10),(0,0), 'E',1], # months_label
            [0,2,(10,20),(0,0),'W',1] # months_cbox
        ]
        index = 0
        for child in self.__months_frame.winfo_children():
            child.grid(
                row=grid_setup[index][0],
                column=grid_setup[index][1],
                padx=grid_setup[index][2],
                pady=grid_setup[index][3],
                sticky=grid_setup[index][4],
                columnspan=grid_setup[index][5]
            )
            index += 1

        # table_frame
        self.__table_frame = ttk.Frame(self.__sales_frame)
        ## table
        self.__table = ttk.Treeview(
            self.__table_frame,
            selectmode='browse',
            column=('column1', 'column2', 'column3', 'column4', 'column5'),
            show='headings'
        )
        self.__table_scrollbar = ttk.Scrollbar(
            self.__table_frame, 
            orient="vertical", 
            command=self.__table.yview, 
            cursor="hand2"
        )
        self.__table.configure(yscrollcommand=self.__table_scrollbar.set)

        # grid
        # table_frame
        self.__table_frame.grid(row=1, column=0, sticky='NSEW')
        # children
        self.__table.grid(row=0, column=0, sticky='NSEW', padx=(20,0), pady=(10,20))
        self.__table_scrollbar.grid(row=0, column=1, padx=(0,20), pady=(10,20), sticky="WNS")
        update_table_dimensions(
            master=self,
            table=self.__table,
            colname=["Código", "Detalhes", "Valor", "Data", "Autenticação"],
            colwidth=[0.2,0.2,0.2,0.2,0.2]
        )
        # details_frame
        self.__details_frame = ttk.Frame(self.__sales_frame)
        # account
        self.__account_status_string = tk.StringVar()
        self.__account_status = ttk.Label(
            self.__details_frame,
            textvariable=self.__account_status_string,
        )
        # sales
        self.__sales_string = tk.StringVar()
        self.__sales_label = ttk.Label(
            self.__details_frame,
            textvariable=self.__sales_string,
        )
        # amount
        self.__amount_string = tk.StringVar()
        self.__amount_label = ttk.Label(
            self.__details_frame,
            textvariable=self.__amount_string,
        )
        # grid
        # details_frame
        self.__details_frame.grid(row=2, column=0, sticky='NSEW')
        self.__details_frame.columnconfigure(0, weight=1)
        # children
        self.__account_status.grid(row=0, column=0, padx=20, pady=(0,20), sticky='W')
        self.__sales_label.grid(row=0, column=1, padx=20, pady=(0,20), sticky='E')
        self.__amount_label.grid(row=0, column=2, padx=20, pady=(0,20), sticky='E')

        # buttons_frame
        self.__buttons_frame = ttk.Frame(self)
        self.__buttons_frame.rowconfigure(0, weight=1)
        self.__buttons_frame.columnconfigure((0,1,2), weight=1)
        self.__buttons_frame.grid(row=3, column=0, sticky='NSEW')
        # buttons
        buttons_dict = {
            'Pagar': self.pay,
            'Alterar': self.update_costumer,
            'Fechar': self.destroy,
        }
        self.__buttons = []
        buttons_padding = [20,5,5,20]
        index = 0
        for key in buttons_dict.keys():
            value = buttons_dict[key]
            tmp_button = ttk.Button(
                self.__buttons_frame, 
                text=key, 
                command=value,
                cursor='hand2'
            )
            tmp_button.grid(row=0, column=index, padx=(buttons_padding[index], buttons_padding[index+1]), pady=20, sticky='NSEW')
            tmp_button.bind("<Return>", value)
            tmp_button.bind("<KP_Enter>", value)
            self.__buttons.append(tmp_button)
            index += 1

        # self.set_sales_table()
        self.__months_cbox.bind('<<ComboboxSelected>>', self.set_sales_table)
        self.__table.bind('<Double-1>', self.see_items)
    
    def update_costumer(self, *args):
        window = ClientRegister(
            self.__container, 
            operation='update', 
            id=self.__costumer_id, 
            history_update=self.__history_update
        )
        self.wait_window(window)
        self.__costumer = self.__db.get_costumer_info()
        self.set_costumer_labels()

    def set_costumer_labels(self, *args):
        # cod
        self.__costumer_id_string.set(f'Código: {format(tp="id", string=str(self.__costumer["id"]))}')
        # name
        self.__costumer_name_string.set(f'Nome: {self.__costumer["name"]}')
        # cpf
        self.__costumer_cpf_string.set(f'CPF: {format(tp="cpf", string=self.__costumer["cpf"])}')
        # contact
        self.__costumer_contact_string.set(f'Contato: {format(tp="phone", string=self.__costumer["phone_1"])}\t{format(tp="phone", string=self.__costumer["phone_2"])}')
        # address
        self.__costumer_address_string.set(f'Endereço: {self.__costumer["street"]}, {self.__costumer["number"]}, {self.__costumer["district"]} - {self.__costumer["city"]}')
        # limit
        self.__costumer_limit_string.set(f'Limite mensal: {to_br_currency(self.__costumer["limit"])}')
        # status
        self.__costumer_status_string.set(f'Status da conta: {self.__costumer["status"]}')
        self.__costumer_status_label.configure(foreground=self.choose_color(self.__costumer["status"]))
        # authorized people

    def see_items(self, *args):
        selection = self.__table.selection()[0]
        index = hex_to_int(str(selection).replace('I',''))-1
        window = InfoBox(self, title='Itens', message=self.__item_string[index])

    def set_sales_table(self, *args):
        for row in self.__table.get_children():
            self.__table.delete(row)
        
        self.item_string = []
        
        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
            
        if(int(self.months_dict[self.months_cbox.get()]) == int(date.today().strftime("%m"))):
            try:
                # getting the details of all sales
                cursor.execute("""
                select cod, items, value, strftime("%d/%m/%Y", s_date), authentication from sale
                where client_cod = ? and  strftime("%m", s_date) = ?
                order by s_date
                """, (self.client_cod, self.months_dict[self.months_cbox.get()]))
                
                for line in cursor.fetchall():
                    cod = format(tp='id', string=str(line[0]))
                    items = json.loads(line[1]).values()
                    subtotal = to_br_currency(line[2])
                    authentication = line[3]
                    string = []
                    for item in items:
                        string.append(f'x{item[0]} {item[1]} : {item[2]}')
                    string = '\n'.join(string)
                    self.table.insert("", index="end", values=[cod, 'CLIQUE PARA VER', subtotal, authentication])
                    self.item_string.append(string)
                conn.commit() 
                # getting the sales total
                cursor.execute("""
                select sum(value) from sale
                where client_cod = ?
                """, (self.client_cod,))
                if cursor.fetchall(): self.amount_string.set(f'Total: {to_br_currency(cursor.fetchall()[0][0])}')
                conn.commit()
                # getting the sales quantity 
                cursor.execute("""
                select count(cod) from sale
                where client_cod = ?
                """, (self.client_cod,))
                if cursor.fetchall(): self.sales_string.set(f'Compras: {cursor.fetchall()[0][0]}')
                conn.commit()
                conn.close()  
            except:
                pass
        else:
            # getting the details of all sales
            cursor.execute("""
            select json_tree(items, '$.ALL')
            from expired_sale
            where client_cod = ? and  strftime("%m", json_extract(items, '$.s_date')) = ?
            order by json_extract(items, '$.s_date')
            """, (self.client_cod, self.months_dict[self.months_cbox.get()]))
            print(cursor.fetchall())
            for line in cursor.fetchall():
                cod = format(tp='id', string=str(line[0]))
                items = json.loads(line[1]).values()
                subtotal = to_br_currency(line[2])
                authentication = line[3]
                string = []
                for item in items:
                    string.append(f'x{item[0]} {item[1]} : {item[2]}')
                string = '\n'.join(string)
                self.table.insert("", index="end", values=[cod, 'CLIQUE PARA VER', subtotal, authentication])
                self.item_string.append(string)
            conn.commit() 
            # getting the sales total
            cursor.execute("""
            select value from expired_sale
            where client_cod = ?
            """, (self.client_cod,))
            if cursor.fetchall(): self.amount_string.set(f'Total: {to_br_currency(cursor.fetchall()[0][0])}')
            conn.commit()
            # getting the sales quantity 
            cursor.execute("""
            select count(cod) from expired_sale
            where client_cod = ?
            """, (self.client_cod,))
            if cursor.fetchall(): self.sales_string.set(f'Compras: {cursor.fetchall()[0][0]}')
            conn.commit()
            conn.close() 
            
            self.verify_pendings(this_month=self.months_cbox.get())
        
    def choose_color(self, string):
        try:
            for caractere in string:
                if int(caractere) == 0:
                    return 'green'
            return 'red'
        except:
            if string in ('ABERTO', 'ESTE MÊS FOI FINALIZADO'):
                return 'green'
            elif string == 'SUSPENSO':
                return 'orange'
            elif string in ('FECHADO', 'ESTE MÊS ESTÁ PENDENTE'):
                return 'red'

    def verify_pendings(self, this_month=None, *args):
        
        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
        
        if this_month is not None:
            cursor.execute("""
            select expiration_date from expired_sale
            where client_cod = ? and paid = 'false'
            """, (self.client_cod,))
            
            if cursor.fetchall():
                for line in cursor.fetchall():
                    exp_month = date.fromordinal(date(line[0]).toordinal() - 1).strftime("%m")
                    if int(exp_month) == int(self.months_dict[this_month]):
                        self.pending_months_string.set('ESTE MÊS ESTÁ PENDENTE')
                        break
            self.pending_months_string.set('ESTE MÊS FOI FINALIZADO')

        else:
            cursor.execute("""
            select count(cod) from expired_sale
            where client_cod = ? and paid = 'false'
            """, (self.client_cod,))

            counter = 0
            if isinstance(cursor.fetchall(), tuple):
                counter = int(cursor.fetchall()) 
            
            string = f'{counter} MESES PENDENTES'
            if counter == 1:
                string = f'{counter} MÊS PENDENTE'
            self.pending_months_string.set(string)    
       
        self.pending_months_label['foreground'] = self.choose_color(self.pending_months_string.get())
        conn.commit()
        conn.close() 

    def pay(self, *args):
        window = PayWindow(
            self,
            cod=self.client['cod'],
            name=self.client['name']
        )

class ClientRegister(tk.Toplevel):
    def __init__(self, container=None, operation=None, costumer_id=None, history_update=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        
        self.operation_dict = {
            'insert': 'Inserir',
            'delete': 'Remover',
            'update': 'Atualizar'
        }
        
        self.title(f'{self.operation_dict[operation]} Cliente')
        self.resizable(False, False)
        self.rowconfigure((1,2,3,4,5,6), weight=1)
        self.columnconfigure(0, weight=1)
        self.grab_set()
        self.focus()
        
        self.__values = None
        self.__operation = operation
        self.__costumer_id = costumer_id
        self.__history_update = history_update
        self.__reg_digit = self.register(callback_digit)
        self.__reg_float = self.register(callback_float)
        self.__db = Database()
        
        self.__costumer = self.__db.get_costumer_info(costumer_id=self.__costumer_id) 
            
        self.error_string = tk.StringVar()
        self.error_label = ttk.Label(
            self,
            textvariable=self.error_string,
            foreground='red'
        )

        # name 
        self.name_frame = ttk.Frame(self)
        self.name_label = ttk.Label(
            self.name_frame,
            text='* Nome',
        ) 
        self.name_string = tk.StringVar()
        self.name_string.set(self.client["name"])
        self.name_entry = ttk.Entry(
            self.name_frame,
            textvariable=self.name_string,
            width=50,
        ) 

        # documents
        self.inf_frame = ttk.Frame(self)
        self.cpf_label = ttk.Label(
            self.inf_frame,
            text='CPF'
        ) 
        self.cpf_string = tk.StringVar()
        self.cpf_string.set(self.client["cpf"])
        self.cpf_entry = ttk.Entry(
            self.inf_frame,
            textvariable=self.cpf_string,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        ) 
        self.rg_label = ttk.Label(
            self.inf_frame,
            text='RG'
        ) 
        self.rg_string = tk.StringVar()
        self.rg_string.set(self.client["rg"])
        self.rg_entry = ttk.Entry(
            self.inf_frame,
            textvariable=self.rg_string,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        ) 
        
        # Contact 
        self.contact_frame = ttk.Frame(self)
        self.phone_1_label = ttk.Label(
            self.contact_frame,
            text='Telefone 1'
        ) 
        self.phone_1_string = tk.StringVar()
        self.phone_1_string.set(self.client["phone_1"])
        self.phone_1_entry = ttk.Entry(
            self.contact_frame,
            textvariable=self.phone_1_string,
            width=20,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        )  
        self.phone_2_label = ttk.Label(
            self.contact_frame,
            text='Telefone 2',
        ) 
        self.phone_2_string = tk.StringVar()
        self.phone_2_string.set(self.client["phone_2"])
        self.phone_2_entry = ttk.Entry(
            self.contact_frame,
            textvariable=self.phone_2_string,
            width=20,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        )

        # address
        self.address_frame = ttk.Frame(self)
        self.street_label = ttk.Label(
            self.address_frame,
            text='Rua/Avenida'
        ) 
        self.street_string = tk.StringVar()
        self.street_string.set(self.client["street"])
        self.street_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.street_string,
            width=50
        )  
        self.number_label = ttk.Label(
            self.address_frame,
            text='Número'
        ) 
        self.number_string = tk.StringVar()
        self.number_string.set(self.client["number"])
        self.number_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.number_string,
            width=9,
            validate="key", 
            validatecommand=(self.reg_digit, '%P')
        )  
        self.district_label = ttk.Label(
            self.address_frame,
            text='Bairro',
        ) 
        self.district_string = tk.StringVar()
        self.district_string.set(self.client["district"])
        self.district_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.district_string,
            width=25
        )
        self.city_label = ttk.Label(
            self.address_frame,
            text='Cidade',
        ) 
        self.city_string = tk.StringVar()
        self.city_string.set(self.client["city"])
        self.city_entry = ttk.Entry(
            self.address_frame,
            textvariable=self.city_string,
            width=25
        )

        # limit
        self.limit_frame = ttk.Frame(self)
        self.limit_label = ttk.Label(
            self.limit_frame,
            text='Limite mensal',
        ) 
        self.limit_string = tk.StringVar()
        self.limit_string.set(self.client["limit"])
        self.limit_entry = ttk.Entry(
            self.limit_frame,
            textvariable=self.limit_string,
            validate="key", 
            validatecommand=(self.reg_float, '%P')
        )
        self.status_label = ttk.Label(
            self.limit_frame,
            text='Status da conta',
        ) 
        self.status_cbox = ttk.Combobox(
            self.limit_frame,
            values=['ABERTO', 'SUSPENSO', 'FECHADO'],
            state='readonly'
        )
        self.status_cbox.set(self.client["status"])
        
        self.auth_frame = ttk.Frame(self)
        self.auth_label = ttk.Label(
            self.auth_frame,
            text='Pessoas autorizadas',
        ) 
        self.auth_string = tk.StringVar()
        self.auth_entry = ttk.Entry(
            self.auth_frame,
            textvariable=self.auth_string
        )
        self.insert_button = ttk.Button(
            self.auth_frame,
            text='Inserir',
            cursor='hand2',
            command=self.auth_insert
        )
        self.auth_table = ttk.Treeview(
            self.auth_frame,
            selectmode='extended',
            column=('column1', 'column2'),
            show='headings',
            height=3
        )
        self.auth_table_scrollbar = ttk.Scrollbar(
            self.auth_frame, 
            orient="vertical", 
            command=self.auth_table.yview, 
            cursor="hand2"
        )
        self.auth_table.configure(yscrollcommand=self.auth_table_scrollbar.set)

        self.delete_button = ttk.Button(
            self.auth_frame,
            text='Remover [DEL]',
            cursor='hand2',
            command=self.auth_delete,
            state='disabled'
        )
        
        # buttons
        self.buttons_frame = ttk.Frame(self)
        self.confirm_button = ttk.Button(
            self.buttons_frame,
            text='OK [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.cancel_button = ttk.Button(
            self.buttons_frame,
            text='Cancelar [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.name_entry, True],
            [self.cpf_entry, True],
            [self.rg_entry, True],
            [self.phone_1_entry, True],
            [self.phone_2_entry, True],
            [self.street_entry, True],
            [self.number_entry, True],
            [self.district_entry, True],
            [self.city_entry, True],
            [self.limit_entry, True],
            [self.status_cbox, True],
            [self.auth_entry, True],
            [self.insert_button, True],
            [self.auth_table, False],
            [self.delete_button, False],
            [self.confirm_button, True],
            [self.cancel_button, True]
        ]
        self.widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.name_frame.grid(row=1, column=0, sticky='NSEW')
        self.name_frame.rowconfigure(2, weight=1)
        self.name_frame.columnconfigure(0, weight=1)
        self.name_label.grid(row=0, column=0, padx=20, pady=0, sticky='W')
        self.name_entry.grid(row=1, column=0, padx=20, pady=(0,10), sticky='EW')

        self.inf_frame.grid(row=2, column=0, sticky='NSEW')
        self.inf_frame.rowconfigure(1, weight=1)
        self.inf_frame.columnconfigure((0,1), weight=1)
        self.cpf_label.grid(row=0, column=0, padx=(20, 10), pady=0, sticky='W')
        self.cpf_entry.grid(row=1, column=0, padx=(20, 10), pady=(0,10), sticky='EW')
        self.rg_label.grid(row=0, column=1, padx=(10, 20), pady=0, sticky='W')
        self.rg_entry.grid(row=1, column=1, padx=(10, 20), pady=(0,10), sticky='EW')

        self.contact_frame.grid(row=3, column=0, sticky='NSEW')
        self.contact_frame.rowconfigure(1, weight=1)
        self.contact_frame.columnconfigure((0,1), weight=1)
        self.phone_1_label.grid(row=0, column=0, padx=(20, 10), pady=(10,0), sticky='W')
        self.phone_1_entry.grid(row=1, column=0, padx=(20, 10), pady=(0,10), sticky='EW')
        self.phone_2_label.grid(row=0, column=1, padx=(10, 20), pady=(10,0), sticky='W')
        self.phone_2_entry.grid(row=1, column=1, padx=(10, 20), pady=(0,10), sticky='EW')

        self.address_frame.grid(row=4, column=0, sticky='NSEW')
        self.address_frame.rowconfigure((1,3), weight=1)
        self.address_frame.columnconfigure((0,1), weight=1)
        self.street_label.grid(row=0, column=0, padx=(20, 10), pady=(10,0), sticky='W')
        self.street_entry.grid(row=1, column=0, padx=(20, 10), pady=(0,10), sticky='W')
        self.number_label.grid(row=0, column=1, padx=(10, 20), pady=(10,0), sticky='W')
        self.number_entry.grid(row=1, column=1, padx=(10, 20), pady=(0,10), sticky='W')
        self.district_label.grid(row=2, column=0, padx=(20, 10), pady=0, sticky='W')
        self.district_entry.grid(row=3, column=0, padx=(20, 10), pady=(0,10) ,sticky='EW')
        self.city_label.grid(row=2, column=1, padx=(10, 20), pady=0, sticky='W')
        self.city_entry.grid(row=3, column=1, padx=(10, 20), pady=(0,10), sticky='EW')

        self.limit_frame.grid(row=5, column=0, sticky='NSEW')
        self.limit_frame.rowconfigure(1, weight=1)
        self.limit_frame.columnconfigure((0, 1), weight=1)
        self.limit_label.grid(row=0, column=0, padx=(20, 10), pady=(10, 0), sticky='W')
        self.limit_entry.grid(row=1, column=0, padx=(20, 10), pady=(0, 10), sticky='EW')
        self.status_label.grid(row=0, column=1, padx=(10, 20), pady=(10, 0), sticky='W')
        self.status_cbox.grid(row=1, column=1, padx=(10, 20), pady=(0, 10), sticky='W')

        self.auth_frame.grid(row=6, column=0, sticky='NSEW')
        self.auth_frame.rowconfigure(2, weight=1)
        self.auth_frame.columnconfigure(0, weight=1)
        self.auth_label.grid(row=0, column=0, padx=20, pady=(10,0), sticky='W')
        self.auth_entry.grid(row=1, column=0, padx=(20,0), pady=5, sticky='EW')
        self.insert_button.grid(row=1, column=1, padx=20, pady=5, sticky='E')
        self.auth_table.grid(row=2, column=0, padx=(20,35), pady=(0,10), sticky='EW', columnspan=2)
        self.auth_table_scrollbar.grid(row=2, column=1, padx=(0,20), pady=(0,10), sticky='NSE')
        self.delete_button.grid(row=3, column=1, padx=20, pady=0, sticky='E')

        self.buttons_frame.grid(row=7, column=0, columnspan=2, sticky='NSEW')
        self.buttons_frame.rowconfigure(1, weight=1)
        self.buttons_frame.columnconfigure((0,1), weight=1)
        self.confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.name_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.name_entry, widget_deque=self.widget_deque))
        self.name_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.name_entry, widget_deque=self.widget_deque))

        self.cpf_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.cpf_entry, widget_deque=self.widget_deque))
        self.cpf_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.cpf_entry, widget_deque=self.widget_deque))

        self.rg_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.rg_entry, widget_deque=self.widget_deque))
        self.rg_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.rg_entry, widget_deque=self.widget_deque))

        self.phone_1_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.phone_1_entry, widget_deque=self.widget_deque))
        self.phone_1_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.phone_1_entry, widget_deque=self.widget_deque))

        self.phone_2_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.phone_2_entry, widget_deque=self.widget_deque))
        self.phone_2_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.phone_2_entry, widget_deque=self.widget_deque))

        self.street_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.street_entry, widget_deque=self.widget_deque))
        self.street_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.street_entry, widget_deque=self.widget_deque))

        self.number_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.number_entry, widget_deque=self.widget_deque))
        self.number_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.number_entry, widget_deque=self.widget_deque))

        self.district_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.district_entry, widget_deque=self.widget_deque))
        self.district_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.district_entry, widget_deque=self.widget_deque))

        self.city_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.city_entry, widget_deque=self.widget_deque))
        self.city_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.city_entry, widget_deque=self.widget_deque))

        self.limit_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.limit_entry, widget_deque=self.widget_deque))
        self.limit_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.limit_entry, widget_deque=self.widget_deque))

        self.status_cbox.bind('<Return>', lambda arg: focus_next(previous_widget=self.status_cbox, widget_deque=self.widget_deque))
        self.status_cbox.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.status_cbox, widget_deque=self.widget_deque))

        self.auth_table.bind('<<TreeviewSelect>>', lambda arg: update_button_state(table=self.auth_table, button=self.delete_button))
        
        self.auth_entry.bind('<Return>', self.auth_insert)
        self.auth_entry.bind('<KP_Enter>', self.auth_insert)

        self.insert_button.bind('<Return>', self.auth_insert)
        self.insert_button.bind('<KP_Enter>', self.auth_insert)

        self.delete_button.bind('<Return>', self.auth_delete)
        self.delete_button.bind('<KP_Enter>', self.auth_delete)

        self.confirm_button.bind('<Return>', self.confirm)
        self.confirm_button.bind('<KP_Enter>', self.confirm)

        self.cancel_button.bind('<Return>', self.cancel)
        self.cancel_button.bind('<KP_Enter>', self.cancel)

        focus_next(previous_widget=self.cancel_button, widget_deque=self.widget_deque)

        update_table_dimensions(
            master=self,
            table=self.auth_table,
            colname=['Nome', 'Chave'],
            mincolwidth=[0.5,0.2],
            colwidth=[0.5,0.5]
        )

    def auth_insert(self, *args):
        name = self.auth_string.get()
        
        if name not in (None, ''):
            self.auth_table.insert("", "end", values=[name,"auth_key"])

        self.auth_string.set('')
        self.auth_table.yview_moveto(1)

    def auth_delete(self, *args):
        self.auth_entry.focus()
        self.delete_button.configure(state='disabled')
        
        for item in self.auth_table.selection():
            self.auth_table.delete(item)

    def error_check(self, *args):
        if self.name_string.get() in (None, ''):
            self.error_string.set('Preencha todos os campos obrigatórios (*)')
            focus_next(previous_widget=self.cancel_button, widget_deque=self.widget_deque)
            self.error_label.grid(row=0, column=0, padx=20, pady=10)
            return True
        
        self.error_string.set('')
        self.error_label.grid_remove()
        return False
        
    def get_client_info(self, *args):
        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
        cursor.execute("""
        select * from client where cod == ? limit 1
        """,(self.client_cod,))
        
        queue = cursor.fetchall()[0]
        contact = json.loads(queue[4])
        address = json.loads(queue[5])
        client = {
            'cod':      int(queue[0]),
            'name':     str(queue[1]),
            'cpf':      str(queue[2]),
            'rg':       str(queue[3]),
            'phone_1':  str(contact["phone_1"]),
            'phone_2':  str(contact["phone_2"]),
            'street':   str(address["street"]),
            'number':   str(address["number"]),
            'district': str(address["district"]),
            'city':     str(address["city"]),
            'limit':    str(queue[6]).replace('.',','),
            'status':   str(queue[7]),
            'comment':  str(queue[8])
        }
        conn.commit()
        conn.close()

        return client

    def confirm(self, *args):
        if not self.error_check():
            name = self.name_string.get().upper()
            cpf = self.cpf_string.get()
            rg = self.rg_string.get()
            contact = { 
                "phone_1": self.phone_1_string.get(), 
                "phone_2": self.phone_2_string.get() 
            }
            address = { 
                "street": self.street_string.get().upper(), 
                "number": int(self.number_string.get()), 
                "district": self.district_string.get().upper(), 
                "city": self.city_string.get().upper()
            }
            limit = to_us_float(self.limit_string.get())
            status = self.status_cbox.get().upper()
            self.values = (name, cpf, rg, json.dumps(contact), json.dumps(address), limit, status, comment)
            try:
                conn = sqlite3.connect('credit_sales.db')
                cursor = conn.cursor()
                cursor.execute("""
                select name from client where name == ?
                """, (name,))
                if len(cursor.fetchall()) == 0 and self.operation == 'insert':
                    cursor.execute("""
                        insert into client (name, cpf, rg, contact, address, c_limit, c_status, comment) 
                        values (?,?,?,?,?,?,?,?)
                    """, self.values)
                    cursor.execute("""
                        select cod from client where name == ? limit 1
                    """, (self.values[0],))
                    self.client_cod = cursor.fetchall()[0][0]
                elif len(cursor.fetchall()) != 0:
                    messagebox.showerror('ERRO', 'CLIENTE JÁ CADASTRADO!')
                if self.operation == 'update':
                    cursor.execute("""
                        update client 
                        set 
                        name = ?, 
                        cpf = ?, 
                        rg = ?, 
                        contact = ?, 
                        address = ?, 
                        c_limit = ?, 
                        c_status = ?, 
                        comment = ? 
                        where cod = ?
                    """, (self.values[0], self.values[1], self.values[2], self.values[3], self.values[4], self.values[5], self.values[6], self.values[7], self.client_cod))
                conn.commit()
                conn.close()
                
                cod = f'\n>> CÓDIGO DO CLIENTE:\t{format(tp="id", string=str(self.client_cod))}'
                name = f'\n>> NOME:\t{self.values[0].upper()}'
                log_string = f"\n{datetime.now().strftime('%H:%M:%S')} > {self.operation_dict[self.operation].upper()} CLIENTE {cod}{name}\n"
                self.history_update(log_string)

            except:
                messagebox.showerror('Erro', 'Não foi possível cadastrar o cliente!')
            self.destroy()
    
    def cancel(self, *args): 
        self.destroy()

    def close_month_sales(self, month=None,*args):
        credit_sales_object = CreditSales()
        credit_sales_object.close_month_sales()

class AuthWindow(tk.Toplevel):
    def __init__(self, container, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.title("Autenticação")
        self.geometry("320x130")
        self.resizable(False,False)
        self.grab_set()
        self.focus()
        
        self.__container = container

        self.__auth_string = tk.StringVar()
        self.__auth_label = ttk.Label(
            self,
            textvariable=self.__auth_string,
        ) 
        self.__img_label = ttk.Label(self)

        self.__wait_img = tk.PhotoImage(file=r"assets/wait.png")
        self.__check_img = tk.PhotoImage(file=r"assets/check.png")
        self.__error_img = tk.PhotoImage(file=r"assets/cancel.png")
        
        self.__flag = True
        
        # grid
        # AuthWindow
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        # children
        self.__auth_label.grid(row=0, column=0, padx=20, pady=(20,5))
        self.__img_label.grid(row=1, column=0, padx=20, pady=(5,20))

        self.__auth_string.set('Verificando a identidade...')
        self.__auth_label.configure(foreground=self.choose_color(self.__auth_string.get()))
        self.__img_label.configure(image=self.__wait_img)

        self.after(2000, self.authenticate)
    
    def authenticate(self, *args): 
        self.__auth_string.set("Sucesso")
        self.__auth_label.configure(foreground=self.choose_color(self.__auth_string.get()))
        self.__img_label.configure(image=self.__check_img)
        self.after(1000, self.destroy)
    
    def choose_color(self, string):
        if string == 'Verificandoa a identidade...':
            return 'black'
        elif string == 'Sucesso':
            return 'green'
        elif string == 'Falha':
            return 'red'

class PayWindow(tk.Toplevel):
    def __init__(self, container, cod=None, name=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)

        self.title('Notinhas Pendentes')
        self.resizable(False, False)
        self.rowconfigure((1,2,3,4), weight=1)
        self.columnconfigure(0, weight=1)
        self.grab_set()
        self.focus()
        
        self.__cod = cod
        self.__name = name
        self.__reg_float = self.register(callback_float)

        self.__error_string = tk.StringVar()
        self.__error_label = ttk.Label(
            self,
            textvariable=self.__error_string,
            foreground='red'
        )

        # client
        self.__client_frame = ttk.Frame(self)
        self.__code_label = ttk.Label(
            self.__client_frame,
            text=f'Código: {format(tp="id", string=str(self.__cod))}',
        ) 
        self.__client_label = ttk.Label(
            self.__client_frame,
            text=f'Cliente: {self.__name}',
        )

        self.__sales_frame = ttk.Frame(self)
        ## table
        self.__table = ttk.Treeview(
            self.__sales_frame,
            selectmode='extended',
            column=('column1', 'column2', 'column3'),
            show='headings'
        )
        self.__table.column('column1', width=100, minwidth=100, stretch=tk.NO)
        self.__table.heading('#1', text='Código')
        self.__table.column('column2', width=100, minwidth=100, stretch=tk.NO)
        self.__table.heading('#2', text='Valor')
        self.__table.column('column3', width=100, minwidth=100, stretch=tk.NO)
        self.__table.heading('#3', text='Vencimento')
        
        # Details and Payment
        self.__details_frame = ttk.Frame(self)
        ## Amount
        self.__amount = 0.0
        self.__amount_string = tk.StringVar()
        self.__amount_string.set("Total: R$ 0,00")
        self.__amount_label = ttk.Label(
            self.__details_frame,
            textvariable=self.__amount_string,
        )
        ## Paid Value
        self.__paid_value_label = ttk.Label(
            self.__details_frame,
            text='Valor a ser pago: ',
        ) 
        self.__paid_value_string = tk.StringVar()
        self.__paid_value_string.set('0,00')
        self.__paid_value_entry = ttk.Entry(
            self.__details_frame,
            textvariable=self.__paid_value_string,
            validate="key", 
            validatecommand=(self.__reg_float, '%P'),
            justify=tk.RIGHT
        )
        ## Payment method
        self.__payment_label = ttk.Label(
            self.__details_frame, 
            text='Forma de pagamento'
        )
        self.__payment_array = [
            'DINHEIRO', 
            'CRÉDITO', 
            'DÉBITO', 
            'CHEQUE', 
            'VALE ALIMENTAÇÃO', 
            'SICOOB', 
            'IB CAIXA', 
            'OUTROS'
        ]
        self.__payment_cbox = ttk.Combobox(
            self.__details_frame,
            values=self.__payment_array,
            state='readonly'
        )
        self.__payment_cbox.current(0)

        # buttons
        self.__buttons_frame = ttk.Frame(self)
        self.__confirm_button = ttk.Button(
            self.__buttons_frame,
            text='OK [F1]',
            cursor='hand2',
            command=self.confirm
        )
        self.__cancel_button = ttk.Button(
            self.__buttons_frame,
            text='Cancelar [ESC]',
            cursor='hand2',
            command=self.cancel
        )

        widget_sequence = [
            [self.__table, False],
            [self.__paid_value_entry, True],
            [self.__payment_cbox, True],
            [self.__confirm_button, True],
            [self.__cancel_button, True]
        ]
        self.__widget_deque = collections.deque(widget_sequence)
        
        # grid
        self.__client_frame.grid(row=1, column=0, sticky='NSEW')
        self.__client_frame.rowconfigure((0,1), weight=1)
        self.__client_frame.columnconfigure(0, weight=1)
        self.__code_label.grid(row=0, column=0, sticky='W', padx=20, pady=(20,10))
        self.__client_label.grid(row=1, column=0, sticky='W', padx=20, pady=(0,20))

        self.__sales_frame.grid(row=2, column=0, sticky='NSEW')
        self.__sales_frame.rowconfigure(0, weight=1)
        self.__sales_frame.columnconfigure(0, weight=1)
        self.__table.grid(row=0, column=0, padx=20, pady=(0, 10), sticky='EW')

        self.__details_frame.grid(row=3, column=0, sticky='NSEW')
        self.__details_frame.rowconfigure(0, weight=1)
        self.__details_frame.columnconfigure(1, weight=1)
        self.__amount_label.grid(row=0, column=1, padx=20, pady=(0,10), sticky='E')
        self.__paid_value_label.grid(row=1, column=0, padx=20, pady=(0,10), ipady=5, sticky='E', columnspan=2)
        self.__paid_value_entry.grid(row=1, column=1, padx=20, pady=(0,10), sticky='E')
        self.__payment_cbox.grid(row=2, column=1, padx=20, pady=(0,10), sticky='E')

        self.__buttons_frame.grid(row=4, column=0, columnspan=2, sticky='NSEW')
        self.__buttons_frame.rowconfigure(1, weight=1)
        self.__buttons_frame.columnconfigure((0,1), weight=1)
        self.__confirm_button.grid(row=0, column=0, padx=(20, 10), pady=20, sticky='EW')
        self.__cancel_button.grid(row=0, column=1, padx=(10, 20), pady=20, sticky='EW')

        # binds
        self.bind('<Escape>', self.cancel)
        self.bind('<F1>', self.confirm)

        self.__table.bind('<<TreeviewSelect>>', self.update_details)

        self.__paid_value_entry.bind('<Return>', lambda arg: focus_next(previous_widget=self.__paid_value_entry, widget_deque=self.__widget_deque))
        self.__paid_value_entry.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__paid_value_entry, widget_deque=self.__widget_deque))

        self.__payment_cbox.bind('<Return>', lambda arg: focus_next(previous_widget=self.__payment_cbox, widget_deque=self.__widget_deque))
        self.__payment_cbox.bind('<KP_Enter>', lambda arg: focus_next(previous_widget=self.__payment_cbox, widget_deque=self.__widget_deque))

        self.__confirm_button.bind('<Return>', self.confirm)
        self.__confirm_button.bind('<KP_Enter>', self.confirm)

        self.__cancel_button.bind('<Return>', self.cancel)
        self.__cancel_button.bind('<KP_Enter>', self.cancel)

        focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
        self.set_table()
  
    def error_check(self, *args):
        if self.__paid_value_string.get() in ('', None) or to_us_float(self.__paid_value_string.get()) == 0:
            self.__error_string.set('Preencha todos os campos obrigatórios (*)')
            focus_next(previous_widget=self.__cancel_button, widget_deque=self.__widget_deque)
            self.__error_label.grid(row=0, column=0, padx=20, pady=20)
            return True
        
        self.__error_string.set('')
        self.__error_label.grid_remove()
        return False
    
    def update_details(self, *args):
        selection = self.__table.selection()
        self.__amount = 0.0
        for row in selection:
            value = to_us_float(row[2].replace('R$ ', ''))
            self.__amount += value

        self.__amount_string.set(to_br_currency(self.__amount))
        self.__paid_value_string.set(to_br_float(self.__amount))

    def set_table(self, *args):
        for row in self.__table.get_children():
            self.__table.delete(row)

        conn = sqlite3.connect('credit_sales.db')
        cursor = conn.cursor()
            
        # getting the details of all sales
        cursor.execute("""
        select cod, value, strftime("%d/%m/%Y", expiration_date)from expired_sale
        where client_cod = ?
        order by expiration_date desc
        """, (self.__cod,))
        
        for line in cursor.fetchall():
            self.__table.insert("", "end", values=line)
        conn.commit()
        conn.close()  

    def confirm(self, *args):
        pass

    def cancel(self, *args): 
        self.destroy()
