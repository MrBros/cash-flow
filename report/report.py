import os
from datetime import datetime
from tkinter import messagebox
from openpyxl import load_workbook
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4


class GenerateReport():
    def __init__(self, report_dir_path=None, sheet_file_path=None, *args, **kwargs):
        sheet_file_path = fr"{os.path.dirname(__file__)}/12.xlsx"
        if report_dir_path is None:
            report_dir_path = os.path.dirname(__file__)
        self.__report = canvas.Canvas(
            filename=report_dir_path+fr"/report_{datetime.now().strftime('%Y%m%d')}.pdf",
            pagesize=A4
        )
        # Trying to open the workbook
        workbook = load_workbook(
            filename=sheet_file_path,
            read_only=True
        )
        sheet = workbook["RESUMO"]
        # Reading the sheet data
        self.__base_data_heading = [
            "DIA",
            "ENTRADA",
            "SAÍDA",
            "VENDAS",
            "CARTÃO DE CRÉDITO",
            "CARTÃO DE DÉBITO",
            "CAIXA II"
        ]
        self.__base_data = []
        for row in sheet.iter_rows(min_row=3, max_row=33, min_col=1, max_col=7):
            self.__base_data.append(row)
            print(row)
            

        

            

        self.__report.save()
        
    def map(self, pt): 
        mm = pt/0.352777
        return mm

rep = GenerateReport()